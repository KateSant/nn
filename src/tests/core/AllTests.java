/*
 * Created on 03-Aug-2005 by Katie P
 */
package core;

import junit.framework.Test;
import junit.framework.TestSuite;


public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for core");
		
	      suite.addTest(core.structure.AllTests.suite());
	      suite.addTest(core.structure.layers.AllTests.suite());
	      suite.addTest(core.structure.networks.AllTests.suite());
	      suite.addTest(core.structure.neurons.AllTests.suite());
		//$JUnit-BEGIN$

		//$JUnit-END$
		return suite;
	}
}
