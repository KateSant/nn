/*
 * Created on 18-Nov-2005 by Katie P
 */
package core.utils;

import junit.framework.TestCase;


public class UtilTest extends TestCase {

	
	public void testConcat() throws Exception{
		
		float[] a = new float[]{1,2};
		float[] b = new float[]{3,4};
		float[] c = Util.concat(a,b);
		System.out.println(Util.join(" ",c));
		assertTrue(c[0]==1);
		assertTrue(c[3]==4);
	}
	
	
	public void testConcatFirstEmpty() throws Exception{
		
		float[] a = new float[0];
		float[] b = new float[]{3,4};
		float[] c = Util.concat(a,b);
		System.out.println(Util.join(" ",c));
		assertTrue(c[0]==3);
		assertTrue(c[1]==4);
	}
	
	
	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public UtilTest(String name) {
		super(name);
	}

}
