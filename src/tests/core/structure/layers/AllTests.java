/*
 * Created on 03-Aug-2005 by Katie P
 */
package core.structure.layers;

import junit.framework.Test;
import junit.framework.TestSuite;


public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for core.structure.layers");
		//$JUnit-BEGIN$
		suite.addTestSuite(LayerTest.class);
		//$JUnit-END$
		return suite;
	}
}
