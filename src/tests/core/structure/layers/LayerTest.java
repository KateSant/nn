/*
 * Created on 03-Aug-2005 by Katie P
 */
package core.structure.layers;

import junit.framework.TestCase;
import core.structure.neurons.*;

public class LayerTest extends TestCase {

	public void testFactoryMethod_InputLayer()throws Exception{
		InputLayer i = new InputLayer(2,"test input layer");
		Neuron n = i.getNeuron(0);
		assertTrue(n instanceof InputNeuron);		
	}
	
	public void testFactoryMethod_HiddenLayer()throws Exception{
		HiddenLayer i = new HiddenLayer(2,"test hidden layer");
		Neuron n = i.getNeuron(0);
		assertTrue(n instanceof HiddenNeuron);		
	}
	
	public void testFactoryMethod_OutputLayer()throws Exception{
		OutputLayer i = new OutputLayer(2,"test output layer");
		Neuron n = i.getNeuron(0);
		assertTrue(n instanceof OutputNeuron);		
	}
	
	public void testSetupOffsetNeuron() throws Exception{
		HiddenLayer i = new HiddenLayer(2,"test hidden layer",true);
		assertNotNull(i.getOffsetNeuron());		
	}

}
