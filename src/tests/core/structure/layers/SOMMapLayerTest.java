package core.structure.layers;

import junit.framework.TestCase;
import core.structure.neurons.*;
import core.structure.*;
import core.utils.plot.*;
import core.structure.networks.*;
/**
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public class SOMMapLayerTest extends TestCase {

	
	
	public void testGridSetUpOK() throws Exception{
		SOMMapLayer mapLayer = new SOMMapLayer(5, 5);
		Neuron[][] grid = mapLayer.getGrid();
		int rows = grid[0].length;
		assertEquals(rows,5);
		assertNotNull(grid[4][4]);
	}
	
	
	public void testcalculateEuclidianDifferenceBetween() throws Exception{
		float[]a = new float[]{0f,0f};
		float[]b = new float[]{3f,-4f};  // 3sq + 4sq sqrt = 5?	 
		
		
		SOMMapLayer dl = new SOMMapLayer(1, 1);
		
		float ed = dl.calculateEuclidianDifferenceBetween(a, b);
		assertTrue(ed==5);
	}
	
	public void testChooseNeuronNearestTo() throws Exception{
		SOMNetwork net = new SOMNetwork(2, 2);
		SOMMapLayer dl = net.getMapLayer();
		Neuron x = dl.getNeuron(3);
		Synapse xa = (Synapse)x.getSynapsesInputs().get(0);
		xa.setWeight(0.5f);
		Synapse xb = (Synapse)x.getSynapsesInputs().get(1);
		xb.setWeight(0.5f);
		
		Point p = new Point(0.5f,0.5f);		
		
		Neuron chosen = dl.chooseNeuronNearestTo(p);
		assertTrue(chosen==x);	
		
		Neuron not = dl.getNeuron(2);
		assertFalse(chosen==not);					
	}
	
	
	public void testNeighbourhoodTopLeft() throws Exception{
		SOMMapLayer layer = new SOMMapLayer(3,3);
		
		SOMNeuron topLeft = layer.getGrid()[0][0];
		
		// only 3 neighbours because in corner
		assertTrue(topLeft.getNeighbours().size()==3);
		
		// has rhs neighbour
		SOMNeuron rhs = layer.getGrid()[1][0];
		assertTrue(topLeft.getNeighbours().contains(rhs));
		
		// too far away
		SOMNeuron toofar = layer.getGrid()[0][2];
		assertFalse(topLeft.getNeighbours().contains(toofar));
		
	}
	
	public void testNeighbourhoodMiddle() throws Exception{
		SOMMapLayer layer = new SOMMapLayer(3,3);
		SOMNeuron middle = layer.getGrid()[1][1];
		// full 8 neighbours
		assertTrue(middle.getNeighbours().size()==8);
	}
	
	protected void setUp() throws Exception {
		super.setUp();
		
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}


	public SOMMapLayerTest(String arg0) {
		super(arg0);
	}

}
