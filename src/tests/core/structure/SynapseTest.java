/*
 * Created on 03-Aug-2005 by Katie P
 */
package core.structure;

import junit.framework.TestCase;
import core.structure.Synapse;
import core.structure.networks.*;
import core.structure.neurons.*;

public class SynapseTest extends TestCase {

	private Synapse synapse;
	Neuron in;
	Neuron out;
	
	protected void setUp() throws Exception {
		super.setUp();
		// create one
		in = new InputNeuron(null);
		out = new HiddenNeuron(null);
		synapse= new Synapse(in,out);
	}

	
	/** should be activation of lhs * error on rhs * learning rate **/
	public void testCalcDelta() throws Exception{
		in.setActivation(-1);
		((ActivatableNeuron)out).setTempError(1);
		Network.learningRate=0.3f;
		synapse.calcDelta();
		System.out.println("delta="+synapse.getDelta());
		assertTrue(synapse.getDelta()==-0.3f);
	}
	
	
	public void testStartingWeight() throws Exception{
		assertTrue(synapse.getWeight()>=-0.5);
		assertTrue(synapse.getWeight()<=+0.5);
	}	
	
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public SynapseTest(String name) {
		super(name);
	}

}
