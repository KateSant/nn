/*
 * Created on 03-Aug-2005 by Katie P
 */
package core.structure;

import junit.framework.Test;
import junit.framework.TestSuite;


public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for core.structure");
		//$JUnit-BEGIN$
		suite.addTestSuite(SynapseTest.class);
		//$JUnit-END$
		return suite;
	}
}
