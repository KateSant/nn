/*
 * Created on 03-Aug-2005 by Katie P
 */
package core.structure.neurons;

import junit.framework.Test;
import junit.framework.TestSuite;


public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for core.structure.neurons");
		//$JUnit-BEGIN$
		suite.addTestSuite(ActivatableNeuronTest.class);
		suite.addTestSuite(HiddenNeuronTest.class);
		//$JUnit-END$
		return suite;
	}
}
