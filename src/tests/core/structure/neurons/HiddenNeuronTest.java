/*
 * Created on 03-Aug-2005 by Katie P
 */
package core.structure.neurons;

import junit.framework.TestCase;
import core.structure.*;
import core.utils.*;

public class HiddenNeuronTest extends TestCase {

	HiddenNeuron active;

	OutputNeuron o1;
	Synapse sy_active_o1;

	
	
	protected void setUp() throws Exception {
		super.setUp();
		
		active = new HiddenNeuron();
		
		/** it has 1 output **/
		o1 = new OutputNeuron();		
		sy_active_o1 = new Synapse(active,o1);
		active.addOutputSynapse(sy_active_o1);
	}
	
	//TODO check this
	public void testCalcError() throws Exception{

		sy_active_o1.setWeight(1);
		active.setActivation(0.5f);	
		o1.setTempError(1);
		
		active.calcError();
		System.out.println();

		assertTrue(active.getTempError()==0.25);
	}
}




