/*
 * Created on 03-Aug-2005 by Katie P
 */
package core.structure.neurons;

import junit.framework.TestCase;
import core.structure.*;
import core.utils.*;

public class ActivatableNeuronTest extends TestCase {

	ActivatableNeuron active;

	InputNeuron i1;
	Synapse sy_i1_active;
	//InputNeuron i2;	
	//Synapse sy_i2_active;	
	
	
	protected void setUp() throws Exception {
		super.setUp();
		
		active = new HiddenNeuron();
		
		/** it has 1 input **/
		i1 = new InputNeuron();
		sy_i1_active = new Synapse(i1,active);
		active.addInputSynapse(sy_i1_active);
		
		/*i2 = new InputNeuron();
		sy_i2_active = new Synapse(i2,active);	
		active.addInputSynapse(sy_i2_active);*/
		
	}
	

	public void testBeActivated() throws Exception{
		i1.setActivation(1);
		sy_i1_active.setWeight(1);
		
		active.beActivated();
		
		float funcOfOne = new ActivationFunctionZeroToOne().getact(1,1);
		
		assertTrue(active.getActivation()==funcOfOne);
	}
}




