/*
 * Created on 03-Aug-2005 by Katie P
 */
package core.structure.networks;

import junit.framework.Test;
import junit.framework.TestSuite;


public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for core.structure.networks");
		//$JUnit-BEGIN$
		suite.addTestSuite(BackPropagationNetworkTest.class);
		suite.addTestSuite(ElmanNetworkTest.class);
		//$JUnit-END$
		return suite;
	}
}
