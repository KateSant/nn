/*
 * Created on 03-Aug-2005 by Katie P
 */
package core.structure.networks;

import junit.framework.TestCase;
import core.structure.layers.*;
import core.structure.Synapse;
import core.structure.networks.*;
import core.datapatterns.*;

public class ElmanNetworkTest extends TestCase {

	ElmanNetwork net;
	InputLayer input;
	HiddenLayer hidden;
	OutputLayer output;
	
	protected void setUp() throws Exception {
		super.setUp();
		input = new InputLayer(2,"input");// we want an offsetneuron		
		hidden = new HiddenLayer(3,"hidden");		
		output = new OutputLayer(2,"output");
		net = new ElmanNetwork(input, new HiddenLayer[]{hidden},output);
	
	}

	public void testCopyActivation() throws Exception{
		Pattern pat = new Pattern(new float[]{1,0}, new float[]{1,2});
		net.trainWithPatternSet(new Pattern[]{pat},false);
		float h1_act = hidden.getNeuron(1).getActivation();
		float h2_act =net.getContextLayer().getNeuron(1).getActivation();
		assertTrue(h1_act==h2_act);
	}
	

}












