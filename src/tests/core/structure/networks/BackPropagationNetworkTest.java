/*
 * Created on 03-Aug-2005 by Katie P
 */
package core.structure.networks;

import junit.framework.TestCase;
import core.structure.layers.*;
import core.structure.Synapse;
import core.structure.networks.*;
import core.datapatterns.*;

public class BackPropagationNetworkTest extends TestCase {

	BackPropagationNetwork bp;
	InputLayer input;
	HiddenLayer hidden;
	HiddenLayer hidden2;
	OutputLayer output;
	
	protected void setUp() throws Exception {
		super.setUp();
		input = new InputLayer(2,"input");// we want an offsetneuron		
		hidden = new HiddenLayer(3,"hidden");		
		hidden2 = new HiddenLayer(4,"hidden2");	
		output = new OutputLayer(5,"output");
		bp = new BackPropagationNetwork(input, new HiddenLayer[]{hidden,hidden2},output);
	
	}

	public void testNumLayers() throws Exception{
		assertEquals(bp.getLayers().size(),4);
	}
	
	
	public void testFullyConnected() throws Exception {
		int numInputs_hneuron = hidden.getNeuron(0).getSynapsesInputs().size();
		assertEquals(numInputs_hneuron,2);
		
		int numInputs_h2neuron = hidden2.getNeuron(0).getSynapsesInputs().size();
		assertEquals(numInputs_h2neuron,3);
	}
	
	public void testMatches()throws Exception{
		float[] f1 = new float[]{1,1,1};
		float[] f2 = new float[]{1,1,1};
		float[] f3 = new float[]{1,1,5};
		assertTrue(bp.matches(f1,f2));
		assertFalse(bp.matches(f1,f3));	
	}
	
	public void testFeedForward() throws Exception{

		bp.feedForward(new float[]{1,2});
		float activationFarEndOfNet = output.getNeuron(0).getActivation();
		assertTrue(activationFarEndOfNet!=0);
	}
	
	public void testFeedBack()throws Exception{			
		bp.feedForward(new float[]{1,2});
		bp.feedBack(new float[]{1,2,3,4,5});
		float deltaStartOfNet = ((Synapse)input.getNeuron(0).getSynapsesOutputs().get(0)).getDelta();
		assertTrue(deltaStartOfNet!=0);
	}
	
	public void testDataArrayWrongSizeException()throws Exception{			
		
		try{
			bp.feedForward(new float[]{1});
			fail("Should not allow silly size for data array");
		}catch(DataArrayWrongSizeForLayerException da){
			// OK
		}
		
		try{
			bp.feedForward(new float[]{1,2,3,4});
			fail("Should not allow silly size for data array");
		}catch(DataArrayWrongSizeForLayerException da){
			// OK
		}
		
	}
}












