/*
 * Created on 03-Aug-2005 by Katie P
 */
package referenceimplementations;

import applications.xor.Xor;
import junit.framework.TestCase;


public class TestXor extends TestCase {
	Xor x;


	protected void setUp() throws Exception {
		super.setUp();		
		x = new Xor();
		
	}
	
	/** maybe ambitious!**/
	public void testXorLearnedPerfectly(){
		//for(int n=0; n<10; n++){
			
		assertTrue(x.getTr().getPercCorrect()==100); // could put this down!
	}

}
