/*
 * Created on 13-Aug-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package applications.password.dao;

import java.io.BufferedReader;
import java.io.FileReader;

import junit.framework.TestCase;

import beans.SetOfKeystrokes;
import beans.WrongStringException;
import junit.framework.TestCase;

public class SetOfKeystrokesTest extends TestCase {

	public void testChecksCorrectStringSent() throws Exception{
		
		
	String f = "data/jonathan/20050610153713.xml";
        BufferedReader in = new BufferedReader(new FileReader(f));
        StringBuffer sb = new StringBuffer();
        String str;
        while ((str = in.readLine()) != null) {
        	
            sb.append(str);
        }
        in.close();
	    // now parse it into a SetOfKeystrokes
	    try{
	    	SetOfKeystrokes sok = new SetOfKeystrokes(sb.toString());
	    
	    	fail("wrong string - should have thrown a WrongStringException");

	    } catch (WrongStringException ws) {
	    	System.err.println("Wrong String- good");
	
	    }	
	}
}
