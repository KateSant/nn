package beans;

import java.awt.event.KeyEvent;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Basic Bean for storing keyUp, keyDown and keyCode data.<BR> 
 * A list of these gets constructed by the {@link applet.listener.KeyStrokeListener KeyStrokeListener}<BR>
 * Also has a {@link #serialise(Document) #serialise(Document)} method, 
 * which is used when {@link applet.listener.SetOfKeystrokes SetOfKeystrokes} is creating an XML version of itself.  This method creates an XML node for this particular keystroke.<BR>
 * @author Katie Portwin
* @version 1.0  August 2005
 **/
public class KeyStroke {

	
	/** set**/
	private long pressedAt;
	private long releasedAt;
	int keyCode;
	char keyChar;

	KeyStroke previousKeystroke;// need this to calc flightTo


	/** called when created in applet**/
	public KeyStroke(int keyCode, long pressedAt, char keyChar){
	
		this.keyChar=keyChar;
		this.pressedAt=pressedAt;
		this.keyCode=keyCode;
	}
	
	/** called when key is released **/
	public void released(long releasedAt){
		this.releasedAt=releasedAt;	
	}
	
	
	/** a constructor for when you've parsed it out of xml.. **/
	public KeyStroke(char keyChar, int keyCode, long pressedAt, long releasedAt, KeyStroke previousKeystroke) {
		super();
		this.pressedAt = pressedAt;
		this.releasedAt = releasedAt;
		this.keyCode = keyCode;	
		this.keyChar = keyChar;
		this.previousKeystroke=previousKeystroke;
	}
	

	
	public String toString(){
		return keyChar+"";//"Key:"+key+"\t flightTo:"+flightTo+"\t dwell:"+dwell+"\n";// we don't know times until the end
	}



	public char getKeychar() {
		return keyChar;
	}
	
	public int getKeycode() {
		return keyCode;
	}

	public long getPressedAt() {
		return pressedAt;
	}

	public long getReleasedAt() {
		return releasedAt;
	}

	public Element serialise(Document doc) throws Exception{
		Element hEl = doc.createElement("KeyStroke");
		hEl.setAttribute("keyCode",keyCode+"");
		
		// only put in keyChar if it is printable.. it's only there for eyeballing anyway..

		 char c = keyChar;
		 if (Character.isISOControl(keyChar)) {
		 	System.out.println("is control character");
		 	c='?';// that will do
		 }else{
		 	if(this.keyCode==KeyEvent.VK_SHIFT){// why isn't shift a ctrl char???
		 		System.out.println("is shift");
			 	c='?';// that will do
		 	}else{
		 		System.out.println("is not control charater.. or shift");
		 	}
		 }

		
		hEl.setAttribute("keyChar",c+"");	
		hEl.setAttribute("pressedAt",pressedAt+"");	
		hEl.setAttribute("releasedAt",releasedAt+"");
		return hEl;
	}
	
	public long getDwell() {
		return releasedAt-pressedAt;
	}
	
	public long getFlightTo() throws BadTimingDataException {
		if(previousKeystroke!=null){
			if(previousKeystroke.getReleasedAt()>0){
				return pressedAt-previousKeystroke.getReleasedAt();
			}else{
				throw new BadTimingDataException("could not calculate flightTo for key:"+keyCode +" "+this.keyChar+ " because previous is 0");
			}
		}else{
			throw new BadTimingDataException("First key..? - can't calculate flightTo because previous is null");
		}
	}
	
}
