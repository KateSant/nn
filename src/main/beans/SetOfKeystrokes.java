package beans;

import java.util.*;
import java.text.*;
import org.w3c.dom.*;
import java.io.*;
import java.awt.event.KeyEvent;
import javax.xml.parsers.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/** Bean to represent a whole keystroke sample - an array of all the keystrokes, along with the name of the user, date etc.<BR>
 * Also has method for serialising itself to XML so it can be stored / sent over HTTP .<BR>
 * And methods for un-serialising at the other end, so it can be reused when the servlet at the other end is reading the XML and checking one for errors, and also when the ANN at the other end is reading one from file, and making use of the data.
*@author Katie Portwin
* @version 1.0  August 2005 
 */
public class SetOfKeystrokes {

		String userName;
		String intendedString; 
		/** a method to check this is valid?**/
		LinkedList allKeystrokes= new LinkedList();
		Date sampleCollectedOnDate;
		String sampleCollectedOn;
		String actuallySent;

		
		/** create bean via applet params **/
		public SetOfKeystrokes(String userName, String intendedString, LinkedList allKeystrokes) {
			super();
			this.userName = userName;
			this.intendedString = intendedString;
			this.allKeystrokes = allKeystrokes;
			this.sampleCollectedOnDate = new Date();
			Format formatter = new SimpleDateFormat("yyyyMMddHHmmss");
			sampleCollectedOn = formatter.format(sampleCollectedOnDate);
		}
		
		
		
		
		/** recreate bean via parsing xml document .. throws exception if no good for any reason.. eg, can't parse, don't know name.. etc**/
		public SetOfKeystrokes(String xmls) throws Exception{
			
			Document in;
			try{
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
				DocumentBuilder builder = factory.newDocumentBuilder();
				in = builder.parse(new ByteArrayInputStream(xmls.getBytes()));
							
			}catch (Exception e){
				throw new Exception("Could not parse the XML you sent.");
			}
			
			try{
				NodeList nodes = in.getElementsByTagName("userName");
				Element element = (Element) nodes.item(0);
				this.userName=element.getFirstChild().getNodeValue();
			}catch (Exception e){
				throw new Exception("No username.");
			}
			try{
				NodeList nodes = in.getElementsByTagName("sampleCollectedOn");
				Element element = (Element) nodes.item(0);
				this.sampleCollectedOn=element.getFirstChild().getNodeValue();
				
				this.sampleCollectedOnDate = new Date();
				//Format formatter = new SimpleDateFormat("yyyyMMddHHmmss");
				//sampleCollectedOn = formatter.format(sampleCollectedOnDate);
				
				
			}catch (Exception e){
				throw new Exception("No date.");
			}		
			
			try{
				NodeList nodes = in.getElementsByTagName("intendedString");
				Element element = (Element) nodes.item(0);
				this.intendedString=element.getFirstChild().getNodeValue();
			}catch (Exception e){
				throw new Exception("No intended string.");
			}
			
			
			NodeList nodes = in.getElementsByTagName("KeyStroke"); 
			for (int i = 0; i < nodes.getLength(); i++) {
			       Element element = (Element) nodes.item(i);
			       
			       char keyChar =' ';
			       if(element.getAttribute("keyChar")!=null&&element.getAttribute("keyChar").toString().length()>0){
			       		keyChar = element.getAttribute("keyChar").charAt(0);
			       }
			       int keyCode = Integer.parseInt(element.getAttribute("keyCode"));
			       long pressedAt = Long.parseLong(element.getAttribute("pressedAt"));
			       long releasedAt = Long.parseLong(element.getAttribute("releasedAt"));
			       
			       KeyStroke prev = null;
			       if(i>0){//ie, not first one
			       	prev = (KeyStroke)allKeystrokes.get(i-1);
			       }
			       KeyStroke ks = new KeyStroke(keyChar, keyCode, pressedAt, releasedAt, prev);
			       allKeystrokes.add(ks);
			      
			}  
			//System.out.println("got all keyStrokes..");
			
			// now check they sent the right thing!
			workOutActuallySent();
			
			
			//checkStringSent();
			//System.out.println("intendedString:"+intendedString+"  actuallySent:"+actuallySent);

			
			
		}
		
		public void checkStringSent() throws Exception{
			
			if(!intendedString.equals(actuallySent)){
				throw new WrongStringException("I think you typed: '"+actuallySent+"', but you were meant to send: '"+intendedString+"'<BR>Either something technical went wrong, or you typed the wrong thing!  <BR>Please check.");
			}
		}
		
		private void workOutActuallySent(){
			StringBuffer s=new StringBuffer("");
			for(int n=0; n<allKeystrokes.size(); n++){
				KeyStroke k = (KeyStroke)allKeystrokes.get(n);
				
				//s.append(k.getKeychar());
				// if it is shift, don't append it here..
				if(k.getKeycode()==KeyEvent.VK_SHIFT){
					//skip it
				}else{
					s.append(k.getKeychar());
				}
			}
			actuallySent = s.toString();
		}
		
		
		
		public String serialiseToString(){
			
			
			try{
			
				Document doc = this.serialise();
		
				String s =stringify(doc);
			
				return s;
			
			}catch(Exception e){
				
				System.out.println("serious error - couldn't make xml: "+e.getMessage());
				return null;
			}
		}
		
		
	   /** xml representation of this bean.  **/
	   public Document serialise() throws Exception{
		
		
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			//Document doc = builder.parse( new File(argv[0]) );
			Document doc = builder.newDocument();
			
			Element rootEl = doc.createElement("SetOfKeystrokes");
			doc.appendChild(rootEl);
			
			// header info
			rootEl.appendChild(makeHeaderXML(doc));

			// the keystrokes
			Element data = doc.createElement("data");
			rootEl.appendChild(data);
			for(int n=0; n<allKeystrokes.size(); n++){
				KeyStroke ks = (KeyStroke)allKeystrokes.get(n);
				data.appendChild(ks.serialise(doc));
				
			}
	
			return doc;

		
	}
		
	
	private Element makeHeaderXML(Document doc) throws Exception{
		Element hEl = doc.createElement("header");
		
		if(userName==null){
			throw new Exception("no username");
		}else{
			Element unEl = doc.createElement("userName");
			unEl.appendChild(doc.createTextNode(userName));
			hEl.appendChild(unEl);				
		}
		
		if(sampleCollectedOnDate==null){
			throw new Exception("no sampleCollectedOnDate date");
		}else{
			
			Element dEl = doc.createElement("sampleCollectedOn");

			dEl.appendChild(doc.createTextNode(this.sampleCollectedOn));
			hEl.appendChild(dEl);				
		}
		
		if(intendedString==null){
			throw new Exception("no intendedString date");
		}else{
			Element dEl = doc.createElement("intendedString");
			dEl.appendChild(doc.createTextNode(intendedString));
			hEl.appendChild(dEl);				
		}
		return hEl;
	}


	private String stringify(Document doc) throws Exception{
		
		
		StringWriter outst=new StringWriter();		
		Result out = new StreamResult(outst);	 	
		Source source = new DOMSource(doc);		
		Transformer xformer = TransformerFactory.newInstance().newTransformer();
		xformer.setOutputProperty(OutputKeys.INDENT,"yes");
		//org.apache.xalan.serialize.CharInfo.XML_ENTITIES_RESOURCE = "http://foo.com/yourappletcodebase/XMLEntities.res";
		xformer.transform(source, out);
		return outst.toString();
	}

		/**
		 * @return Returns the intendedString.
		 * 
		 * @uml.property name="intendedString"
		 */
		public String getIntendedString() {
			return intendedString;
		}

		/**
		 * @param intendedString The intendedString to set.
		 * 
		 * @uml.property name="intendedString"
		 */
		public void setIntendedString(String intendedString) {
			this.intendedString = intendedString;
		}

		/**
		 * @return Returns the sampleCollectedOnDate.
		 * 
		 * @uml.property name="sampleCollectedOnDate"
		 */
		public Date getSampleCollectedOnDate() {
			return sampleCollectedOnDate;
		}

		/**
		 * @param sampleCollectedOn The sampleCollectedOn to set.
		 * 
		 * @uml.property name="sampleCollectedOnDate"
		 */
		public void setSampleCollectedOnDate(Date sampleCollectedOnDate) {
			this.sampleCollectedOnDate = sampleCollectedOnDate;
		}

		/**
		 * @return Returns the userName.
		 * 
		 * @uml.property name="userName"
		 */
		public String getUserName() {
			return userName;
		}

		/**
		 * @param userName The userName to set.
		 * 
		 * @uml.property name="userName"
		 */
		public void setUserName(String userName) {
			this.userName = userName;
		}

		/**
		 * @return Returns the allKeystrokes.
		 * 
		 * @uml.property name="allKeystrokes"
		 */
		public LinkedList getAllKeystrokes() {
			return allKeystrokes;
		}

		/**
		 * @return Returns the sampleCollectedOn.
		 * 
		 * @uml.property name="sampleCollectedOn"
		 */
		public String getSampleCollectedOn() {
			return sampleCollectedOn;
		}

		/**
		 * @return Returns the actuallySent.
		 * 
		 * @uml.property name="actuallySent"
		 */
		public String getActuallySent() {
			// set it in case keystrokes have changed
			workOutActuallySent();
			return actuallySent;
		}

		/**
		 * @param actuallySent The actuallySent to set.
		 * 
		 * @uml.property name="actuallySent"
		 */
		public void setActuallySent(String actuallySent) {
			this.actuallySent = actuallySent;
		}

		/**
		 * @param allKeystrokes The allKeystrokes to set.
		 * 
		 * @uml.property name="allKeystrokes"
		 */
		public void setAllKeystrokes(LinkedList allKeystrokes) {
			this.allKeystrokes = allKeystrokes;
		}

		/**
		 * @param sampleCollectedOn The sampleCollectedOn to set.
		 * 
		 * @uml.property name="sampleCollectedOn"
		 */
		public void setSampleCollectedOn(String sampleCollectedOn) {
			this.sampleCollectedOn = sampleCollectedOn;
		}

}
