/*
 * Created on 03-Aug-2005 by Katie P
 */
package beans;


public class BadTimingDataException extends Exception {

	public BadTimingDataException() {
		super();
		
	}

	public BadTimingDataException(String message) {
		super(message);
	
	}

}
