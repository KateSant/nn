
package beans;


public class WrongStringException extends Exception {

	public WrongStringException() {
		super();

	}

	public WrongStringException(String arg0) {
		super(arg0);

	}

}
