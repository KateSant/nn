/*
 * Created on 03-Jul-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package applications.freeformpassword.experiments;

import applications.freeformpassword.FreeformPassword;
import core.utils.plot.Line;
import core.utils.plot.Graph;
import core.utils.plot.Point;


public class ExperimentWithLengthOfTestStrings extends FreeformPasswordMultiRunner {
	
	int numUsers = 3;
	int trainLength = 450;
	// do testLengths from 1 to 400
	int[] testLengths = new int[]{5, 10, 20, 50, 100, 200, 300, 400};

	

	protected void test() throws Exception{
		// 1st param is length of sample, 2nd is number of samples to use in training, 3rd is number of users	
		tests = new FreeformPassword[testLengths.length];
		points = new Point[testLengths.length];
		falsePositivesPoints=new Point[testLengths.length];
		falseNegativesPoints=new Point[testLengths.length];
		
		for(int n=0; n<testLengths.length; n++){
			System.out.println("setup");
			// set it up
			tests[n]= new FreeformPassword(trainLength,testLengths[n],numUsers);//2 users
			multiRun(tests[n],n,testLengths[n]);
			
		}

	}
	
	protected  void plot(){	
		super.plot();

		p = new Graph(new Line[]{correctLine,falsePositivesLine,falseNegativesLine}, 400, 100, 50f, 10f, "Effect of length of test string", "length","accuracy(%)");
		p.plot();
	}
	
	public static void main(String args[]){
		try{
			ExperimentWithLengthOfTestStrings me = new ExperimentWithLengthOfTestStrings();
			me.test();
			me.plot();
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}

}
