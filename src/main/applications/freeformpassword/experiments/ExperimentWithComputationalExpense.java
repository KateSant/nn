
package applications.freeformpassword.experiments;
import java.util.*;
import applications.freeformpassword.FreeformPassword;
import core.utils.plot.Graph;
import core.utils.plot.Line;
import core.utils.plot.Point;

public class ExperimentWithComputationalExpense extends FreeformPasswordMultiRunner{


	int trainLength = 450;
	int testLength=300;

	int[] numUsers = new int[]{2,3,4};/** need to up this! **/
	
	
	protected void test() throws Exception{
		
		
		tests = new FreeformPassword[numUsers.length];
		points = new Point[numUsers.length];
		falsePositivesPoints=new Point[numUsers.length];
		falseNegativesPoints=new Point[numUsers.length];

		for(int n=0; n<numUsers.length; n++){
					
			tests[n]= new FreeformPassword(trainLength,testLength,numUsers[n]);
		
			// time
			Calendar before = new GregorianCalendar();
			// run
			multiRun(tests[n],n,numUsers[n]);
			//time again
			Calendar after = new GregorianCalendar();
			  // Get difference in milliseconds
		    long diffMillis = after.getTimeInMillis()-before.getTimeInMillis();		    
		    // divide by number of times we ran it
		    float averageTime = diffMillis / this.numTimesPerPoint;	    
		    // in seconds
		    float avSecs = averageTime/(1000);          
			//int res = multiRun(tests[n]); 
			points[n]=new Point(numUsers[n],avSecs);
			
		}
		

	
	}
	
	
	protected void plot(){		
		Line thisline = new Line(points,null,null);
		//xmax ymax, xsteps, ysteps
		Graph g = new Graph(new Line[]{thisline}, 5, 60, 1f, 10f, "Computational Expense of retraining network (Password/Backprop)", "num users","seconds");
		
		g.plot();
	}
	
	
	
	public static void main(String args[]){
		try{
			ExperimentWithComputationalExpense me = new ExperimentWithComputationalExpense();
			System.out.println("plotting..");
			me.test();
			me.plot();
			
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
	
}
