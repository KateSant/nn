
package applications.freeformpassword.experiments;

import applications.freeformpassword.*;
import core.utils.plot.Line;
import core.utils.plot.Graph;
import core.utils.plot.Point;


public class ExperimentWithInterleafSize extends FreeformPasswordMultiRunner{
	
	
	int numUsers = 2;
	int trainLength = 400;
	int testLength=200;
	
	int[] interLeafSizes = new int[]{1,2,3,5,8,10,12,15,20,30,40,60};
	
	
	protected void test() throws Exception{
		// 1st param is length of sample, 2nd is number of samples to use in training, 3rd is number of users	
		tests = new FreeformPassword[interLeafSizes.length];
		points = new Point[interLeafSizes.length];
		falsePositivesPoints=new Point[interLeafSizes.length];
		falseNegativesPoints=new Point[interLeafSizes.length];
		
		for(int n=0; n<interLeafSizes.length; n++){

			// interleafsize
			int leaf=interLeafSizes[n];
	
//			 set it up
			tests[n]= new FreeformPassword(trainLength,testLength,numUsers);
			tests[n].setInterLeafSize(leaf);
			//TestResults res = multiRun(tests[n]);
			multiRun(tests[n],n,leaf);
		}		
	}

	
	protected  void plot(){		
		super.plot();
		//xmax ymax, xsteps, ysteps
		p = new Graph(new Line[]{correctLine,falsePositivesLine,falseNegativesLine}, 65, 100, 5f, 10f, 
				"Effect of inter-leaf size (3 users)", "interleaf size","accuracy(%)");
		p.plot();
	}
	
	
	
	
	public static void main(String args[]){
		try{
			ExperimentWithInterleafSize me = new ExperimentWithInterleafSize();
			me.test();
			me.plot();
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}

}
