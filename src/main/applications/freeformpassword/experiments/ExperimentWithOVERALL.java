
package applications.freeformpassword.experiments;

import core.utils.plot.Line;
import core.utils.plot.Graph;
import core.utils.plot.Point;
import applications.freeformpassword.FreeformPassword;
import applications.password.Password;


public class ExperimentWithOVERALL extends FreeformPasswordMultiRunner{



	int numUsers = 4;
	int trainLength = 450;
	int testLength=300;
	
	
	protected void test() throws Exception{			
		// 1st param is length of sample, 2nd is number of samples to use in training, 3rd is number of users	
		tests = new FreeformPassword[1];
		points = new Point[1];
		falsePositivesPoints=new Point[1];
		falseNegativesPoints=new Point[1];
		
	
		// set it up
		tests[0]= new FreeformPassword(trainLength,testLength,numUsers);
		//tests[0].setDesiredError(0.001f);	
		// run it.. 10 times		
		multiRun(tests[0],1,1);

		
	}
	

	
	
	protected  void plot(){		
		super.plot();
		//xmax ymax, xsteps, ysteps
		p = new Graph(new Line[]{correctLine,falsePositivesLine,falseNegativesLine}, 0.011f, 100, 0.001f, 10f, "OVERALL", "net error","accuracy(%)");
		p.plot();
	}
	
	public static void main(String args[]){
		try{
			ExperimentWithOVERALL me = new ExperimentWithOVERALL();
			me.test();
			me.plot();
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}

}
