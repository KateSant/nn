
package applications.freeformpassword.experiments;

import applications.freeformpassword.*;
import core.utils.plot.Line;
import core.utils.plot.Graph;
import core.utils.plot.Point;


public class ExperimentWithLengthOfTrainingData extends FreeformPasswordMultiRunner{
	
	int numUsers = 4; // try both..
	int testLength=200;	
	int[] trainingLengths = new int[]{5, 10, 50, 100, 300, 450, 600, 800};
	//int[] trainingLengths = new int[]{10,400};

	
	protected void test() throws Exception{
		// 1st param is length of sample, 2nd is number of samples to use in training, 3rd is number of users	
		tests = new FreeformPassword[trainingLengths.length];
		points = new Point[trainingLengths.length];
		falsePositivesPoints=new Point[trainingLengths.length];
		falseNegativesPoints=new Point[trainingLengths.length];
		
		for(int n=0; n<trainingLengths.length; n++){

			// amount training data
			int tl=trainingLengths[n];
	
//			 set it up
			tests[n]= new FreeformPassword(tl,testLength,numUsers);

			//TestResults res = multiRun(tests[n]);
			multiRun(tests[n],n,tl);
		}		
	}

	protected  void plot(){		
		super.plot();
		//xmax ymax, xsteps, ysteps
		p = new Graph(new Line[]{correctLine,falsePositivesLine,falseNegativesLine}, 900, 100, 50f, 10f, "Effect of length of training data", "length","accuracy(%)");
		p.plot();
	}
	
	
	
	public static void main(String args[]){
		try{
			ExperimentWithLengthOfTrainingData me = new ExperimentWithLengthOfTrainingData();
			me.test();
			me.plot();
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
	
	
}
