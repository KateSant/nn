
package applications.freeformpassword.experiments;

import applications.freeformpassword.*;
import core.utils.plot.Line;
import core.utils.plot.Graph;
import core.utils.plot.Point;


public class ExperimentWithNumHiddenUnits extends FreeformPasswordMultiRunner{
	
	
	int numUsers = 4;
	int trainLength = 800;
	int testLength=400;
	
	int[] numHiddenUnits = new int[]{10,50,100,200,300};
	

	
	protected void test() throws Exception{
		// 1st param is length of sample, 2nd is number of samples to use in training, 3rd is number of users	
		tests = new FreeformPassword[numHiddenUnits.length];
		points = new Point[numHiddenUnits.length];
		falsePositivesPoints=new Point[numHiddenUnits.length];
		falseNegativesPoints=new Point[numHiddenUnits.length];
		
		for(int n=0; n<numHiddenUnits.length; n++){

			int hu=numHiddenUnits[n];
	
//			 set it up
			tests[n]= new FreeformPassword(trainLength,testLength,numUsers);
			tests[n].setNumHiddenUnits(hu);

			multiRun(tests[n],n,hu);
		}		
	}

	
	protected  void plot(){		
		super.plot();
		//xmax ymax, xsteps, ysteps
		p = new Graph(new Line[]{correctLine,falsePositivesLine,falseNegativesLine}, 400, 100, 50f, 10f, 
				"Effect of number of hidden units (3 users)", "num units","accuracy(%)");
		p.plot();
	}
	
	
	
	
	public static void main(String args[]){
		try{
			ExperimentWithNumHiddenUnits me = new ExperimentWithNumHiddenUnits();
			me.test();
			me.plot();
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}

}
