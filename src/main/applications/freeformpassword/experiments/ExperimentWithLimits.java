
package applications.freeformpassword.experiments;

import core.structure.networks.Network;
import core.utils.plot.Line;
import core.utils.plot.Graph;
import core.utils.plot.Point;
import core.utils.TestResults;
import applications.freeformpassword.FreeformPassword;
import java.awt.Color;



public class ExperimentWithLimits extends FreeformPasswordMultiRunner{


	int numUsers = 3;
	int trainLength = 400;
	int testLength = 200;

	private float[] limits= new float[]{0.005f,0.01f,0.05f,0.1f,0.2f,0.3f,0.4f,0.5f};
	
	protected void test() throws Exception{
		// 1st param is length of sample, 2nd is number of samples to use in training, 3rd is number of users	
		tests = new FreeformPassword[limits.length];
		points = new Point[limits.length];
		falsePositivesPoints=new Point[limits.length];
		falseNegativesPoints=new Point[limits.length];
		
		for(int n=0; n<limits.length; n++){

			// limits
			float limitPlusMinus=limits[n];
			float lowLimit = 0+limitPlusMinus;
			
			System.err.println("doing limits n:"+n+" limitPlusMinus:"+limitPlusMinus+" lowlimit:"+lowLimit);			
			
//			 set it up
			tests[n]= new FreeformPassword(trainLength,testLength,numUsers,limitPlusMinus);

			//TestResults res = multiRun(tests[n]);
			multiRun(tests[n],n,limitPlusMinus);
		}		
	}
	

	
	protected void plot(){	
		super.plot();
		//xmax ymax, xsteps, ysteps
		p = new Graph(new Line[]{correctLine,falsePositivesLine,falseNegativesLine}, 0.5f, 100, 0.1f, 10f, "Effect of nearToZero/nearToOne cutoff", "cutoff limit","accuracy(%)");
		p.plot();
	}
	
	public static void main(String args[]){
		try{
			ExperimentWithLimits me = new ExperimentWithLimits();
			me.test();
			me.plot();
			
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}

}
