
package applications.freeformpassword.experiments;

import core.utils.plot.Line;
import core.utils.plot.Graph;
import core.utils.plot.Point;
import applications.freeformpassword.FreeformPassword;
import applications.password.Password;


public class ExperimentWithOvertraining extends FreeformPasswordMultiRunner{


	int numUsers = 3;
	int trainLength = 400;
	int testLength = 200;
	
	private float[] desiredErrors= new float[]{/*0.00001f,0.00005f,*/0.0001f,0.0005f,0.001f,0.005f,0.01f,0.05f,0.1f};

	
	protected void test() throws Exception{			
		// 1st param is length of sample, 2nd is number of samples to use in training, 3rd is number of users	
		tests = new FreeformPassword[desiredErrors.length];
		points = new Point[desiredErrors.length];
		falsePositivesPoints=new Point[desiredErrors.length];
		falseNegativesPoints=new Point[desiredErrors.length];
		
		for(int n=0; n<desiredErrors.length; n++){	
//			 set it up
			tests[n]= new FreeformPassword(trainLength,testLength,numUsers);
			tests[n].setDesiredError(desiredErrors[n]);
			
			// run it.. 10 times		
			multiRun(tests[n],n,desiredErrors[n]);

		}
	}
	

	
	protected  void plot(){		
		super.plot();
		//xmax ymax, xsteps, ysteps
		p = new Graph(new Line[]{correctLine,falsePositivesLine,falseNegativesLine}, 0.11f, 100, 0.01f, 10f, "Effect of changing net error (overtraining/underraining) (Freeform, 4 users)", "net error","accuracy(%)");
		p.plot();
	}
	
	public static void main(String args[]){
		try{
			ExperimentWithOvertraining me = new ExperimentWithOvertraining();
			me.test();
			me.plot();
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}

}
