
package applications.freeformpassword.experiments;

import applications.freeformpassword.*;
import core.utils.plot.Line;
import core.utils.plot.Graph;
import core.utils.plot.Point;


public class ExperimentWithNumberOfUsers extends FreeformPasswordMultiRunner{
	
	
	int testLength=300;	
	int trainingLength = 450;


	int[] numUsers = new int[]{2,3,4}; 
	
	protected void test() throws Exception{
		// 1st param is length of sample, 2nd is number of samples to use in training, 3rd is number of users	
		tests = new FreeformPassword[numUsers.length];
		points = new Point[numUsers.length];
		falsePositivesPoints=new Point[numUsers.length];
		falseNegativesPoints=new Point[numUsers.length];
		
		for(int n=0; n<numUsers.length; n++){

			// amount training data
			int nu=numUsers[n];	
//			 set it up
			tests[n]= new FreeformPassword(trainingLength,testLength,nu);
			//TestResults res = multiRun(tests[n]);
			multiRun(tests[n],n,nu);
		}		
	}

	protected  void plot(){		
		super.plot();
		//xmax ymax, xsteps, ysteps
		p = new Graph(new Line[]{correctLine,falsePositivesLine,falseNegativesLine}, 5, 100, 1f, 10f, "Effect of NUMBER OF USERS", "users","accuracy(%)");
		p.plot();
	}
	
	
	
	public static void main(String args[]){
		try{
			ExperimentWithNumberOfUsers me = new ExperimentWithNumberOfUsers();
			me.test();
			me.plot();
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
	
	
}
