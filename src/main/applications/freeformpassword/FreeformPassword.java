
package applications.freeformpassword;


import applications.freeformpassword.dao.*;
import core.structure.layers.*;
import core.structure.networks.ElmanNetwork;
import core.structure.networks.BackPropagationNetwork;
import core.utils.TestResults;
import core.utils.plot.*;
import core.datapatterns.*;


public class FreeformPassword {

	public static final String dataDir = "data/";
	float desiredError = 0.01f;
	
	int numHiddenUnits=50;// OOH, this really matters after all..
	
	int maxTrials = 30;
	ElmanNetwork me;
	
	// in charge of reading and transforming input data
	FreeformSampleDataDAO fsd;
	
	// the response
	TestResults testResults;
	
	boolean drawProgressGraphs;
	
	/** ok, it works with 2 if we have:
	 * 100 hidden
	 * train to error of 0.01.
	 * chunkLength=10
	 * 
	 * put up chunk length.. no difference.  
	 * put down desired error... ends up overtraining to katie..
	 * put hidden layer up to 150..**/
	
	// lengthTrainingsample can change ->change in fsd by chop before interleave
	// lengthtestsample can change -> as above
	// numusers can change -> also fsd
	
	/**public FreeformPassword(int lengthTrainingSample, int lengthTestSample, int numUsers, int leafSize) throws Exception{
	// come back to these 				
	}*/
	
	int numUsers;
	int lengthTrainingSample;
	int lengthTestSample;
	int interLeafSize = 15;// default
	
	float limitPlusMinus = 0.1f;
	
	private byte betweenChunk = ElmanNetwork.BC_REVERT_TO_SAVED;// default
	
	
	/**
	 * @param betweenChunk The betweenChunk to set.
	 */
	public void setBetweenChunk(byte betweenChunk) {
		this.betweenChunk = betweenChunk;
	}
	/**
	 * @param interLeafSize The interLeafSize to set.
	 */
	public void setInterLeafSize(int interLeafSize) {
		this.interLeafSize = interLeafSize;
	}
	public FreeformPassword(int lengthTrainingSample, int lengthTestSample, int numUsers){
		this.numUsers=numUsers;
		this.lengthTrainingSample=lengthTrainingSample;
		this.lengthTestSample=lengthTestSample;
	}
	public FreeformPassword(int lengthTrainingSample, int lengthTestSample, int numUsers, float limitPlusMinus){
		this(lengthTrainingSample,lengthTestSample,numUsers);
		this.limitPlusMinus=limitPlusMinus;
	}
	
	public void go() throws Exception{	
		System.out.println("\n-----------------------------\n STARTING FreeformPassword with\n desiredError:"+this.desiredError+"\n interLeafSize:"+this.interLeafSize+" \n limitPlusMinus:"+this.limitPlusMinus+"\n numHiddenUnits:"+this.numHiddenUnits+"\n lengthTrainingSample:"+this.lengthTrainingSample+"\n lengthTestSample:"+this.lengthTestSample+"\n numUsers:"+numUsers+"\n-----------------------------");
		setUpPatterns();	
		createNetwork();
		train();
		test();
		System.out.println("Test results:"+getTestResults().toString());
	}
	
	private void setUpPatterns() throws Exception{
		fsd = new FreeformSampleDataDAO(lengthTrainingSample,lengthTestSample,numUsers, dataDir);
		// pass this one one
		fsd.setInterLeafSize(this.interLeafSize);
		fsd.setAllDifferentTrainingData(this.allDifferentTrainingData);
		fsd.read();
	}
	
	private void createNetwork() throws Exception{
		//1. set up structure - first 100 are for keycode, 101 is flight and 102 is dwell
		
		InputLayer input = new InputLayer(103,"input",true);// we want an offsetneuron		
		HiddenLayer hidden = new HiddenLayer(numHiddenUnits,"hidden",true);	
		// output is numusers..
		OutputLayer output = new OutputLayer(numUsers,"output");
		me = new ElmanNetwork(input,new HiddenLayer[]{hidden},output);					

		//me.setNearToOne(0.6f);
		//me.setNearToZero(0.4f);
		//me.setLogevery(10);			
		//me.setBetweenChunk(ElmanNetwork.BC_REINITIALISE); // we need this to reinitialise every chunk
		// maybe change the way this works to get dave's thing where save last one 
		// and keep it rather than 0.5's... send array of users and chunk it inside net?
		//me.setBetweenChunk(ElmanNetwork.BC_REVERT_TO_SAVED);
		me.setBetweenChunk(betweenChunk);
		me.setNearToZero(0+limitPlusMinus);
		me.setNearToOne(1-limitPlusMinus);	
		//me.setLearningRate(0.8f);
	}
	
	
	private void train(){
		System.out.println("TRAINING...");
		int numTrials= me.train(fsd.getTrainingPatterns(),desiredError, maxTrials);		
		System.out.println("... Took "+numTrials+" to reach error of "+desiredError);
	}
	
	
	private void test() throws Exception{
		// TODO .. maybe move this inside elman net.. is fairly generic..maybe review all the various testing methods that have turned up..?
		
		
		/** works in v different way to Password.  in that case, test by throwing each of a set of test patterns at it, and seeing whether output is = to desired, do this for severa goes, for each user.
		 * in this case, throw one at it, see if same, then another, see if same, then another... on for 100 or so.  then at END see if works.  also do graph of how got there.. actualy, is same..
		 *	
		 *.. so, make test patterns for users eg: length = 10, and want to do 3 tests for each user..-> need 30 keys.  luckily we have about 500 keys in our test. 
		 */
		/** NB maybe could move this INTO elman network class .. is fairly generic.. **/
		
		System.out.println("TESTING");
		
		testResults = new TestResults(fsd.getTestStrings().length);
		// for each test set..
		for(int n=0; n<fsd.getTestStrings().length; n++){
			SetOfPatterns testpat = fsd.getTestStrings()[n];		
			byte res = me.guess(testpat.getPatterns(),fsd.getAllDesiredOutputs(),testpat.getLabel(),this.drawProgressGraphs);
			
			switch (res){
				case BackPropagationNetwork.MATCH : 		testResults.addCorrect();
											//System.out.println(" MATCH");
											break;
				case BackPropagationNetwork.FALSEPOSITIVE : testResults.addFalsePostitive();
											//System.out.println(" FALSE +ive *****");
											break;
				case BackPropagationNetwork.FALSENEGATIVE : testResults.addFalseNegative();
											//System.out.println(" FALSE -ive");
			 			 					break;
			}
		}
	}
	
	
	private boolean allDifferentTrainingData; // train different people with different data.. hmm
	public void setAllDifferentTrainingData(boolean allDifferentTrainingData){
		this.allDifferentTrainingData=allDifferentTrainingData;
	}
	
	
	
	/**
	 * @return Returns the testResults.
	 */
	public TestResults getTestResults() {
		return testResults;
	}
	
	
	
	/**
	 * @return Returns the lengthTestSample.
	 */
	public int getLengthTestSample() {
		return lengthTestSample;
	}
	/**
	 * @return Returns the lengthTrainingSample.
	 */
	public int getLengthTrainingSample() {
		return lengthTrainingSample;
	}
	
	/**
	 * @param drawProgressGraphs The drawProgressGraphs to set.
	 */
	public void setDrawProgressGraphs(boolean drawProgressGraphs) {
		this.drawProgressGraphs = drawProgressGraphs;
	}
	
	
	/**
	 * @return Returns the interLeafSize.
	 */
	public int getInterLeafSize() {
		return interLeafSize;
	}
	

	public int getNumUsers() {
		return numUsers;
	}
	
	
	/**
	 * @param desiredError The desiredError to set.
	 */
	public void setDesiredError(float desiredError) {
		this.desiredError = desiredError;
	}
	
	
	public void setNumHiddenUnits(int numHiddenUnits) {
		this.numHiddenUnits = numHiddenUnits;
	}
		
	public static void main(String[] args){
		try{	
			int trainLength = 450; 
			int testLength = 200;
			int numUsers = 3;
			FreeformPassword fp = new FreeformPassword(trainLength,testLength,numUsers);//2 users
			fp.setInterLeafSize(8);
			//fp.setAllDifferentTrainingData(true);/** maybe it doesn't matter?**/
			//fp.setBetweenChunk(ElmanNetwork.BC_REINITIALISE);// this means don't do storedcontext thing// result - takes for ever to train!!
			fp.setDrawProgressGraphs(true);
			fp.setNumHiddenUnits(50);
			fp.setDesiredError(0.01f);
			fp.go();
				
			//System.out.println("test results:"+fp.getTestResults().toString());
			
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
	

}

