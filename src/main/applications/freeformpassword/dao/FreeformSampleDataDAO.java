
package applications.freeformpassword.dao;

import java.io.File;
import java.util.*;


import core.datapatterns.*;
import applications.password.dao.*;
import beans.*;
import core.utils.Util;


public class FreeformSampleDataDAO extends PasswordSampleDataDAO{
	
	//float scale = 0.01f;
	int chunkLength=10;// 10 in a string of characters
	

	// inherits:....
	//protected Pattern[] trainingPatterns; // interleaved set of patterns
	//protected Pattern[] testPatterns; ........ but we don't want this..
	// instead:
	private SetOfPatterns[] testStrings;
	
	
	// 1 read test and training sok's for all users
	// 2 allocate desired outputs
	// 3 turn each keystroke into a flight-dwell-next-this pattern
	// 4 interleave the patterns in groups	
	// do 3 for test data, and chop into teststring bits
		
	/** needs to read all data, transform it into patterns, ready for input.. **/
	

	protected int lengthTrainingSample;
	protected int lengthTestSample;
	protected int interLeafSize;
	
	
	
	/**
	 * @param interLeafSize The interLeafSize to set.
	 */
	public void setInterLeafSize(int interLeafSize) {
		this.interLeafSize = interLeafSize;
	}
	public FreeformSampleDataDAO(int lengthTrainingSample, int lengthTestSample, int numUsers, String dataArea) throws Exception{
		
		super(dataArea);
		
		this.numUsers=numUsers;
		this.lengthTrainingSample=lengthTrainingSample;
		this.lengthTestSample=lengthTestSample;		
	}
	
	private boolean allDifferentTrainingData; // train different people with different data.. hmm
	public void setAllDifferentTrainingData(boolean allDifferentTrainingData){
		this.allDifferentTrainingData=allDifferentTrainingData;
	}
	
	
	public void read() throws Exception{
		// read from files
		//System.out.println("READING FILE DATA");
		//HashMap usersSamplesFromFile = getDataFromFiles();		
	

		System.out.println("READING FILE DATA AND GETTING ELIGIBLE USERS");
		//int numTrainingSamples =1;
		//int numTestingSamples=1;
		
		//getTestAndTraining(usersSamplesFromFile,numUsers);
		
		getTestAndTraining();
		
		System.out.print("ADDING DESIRED OUTPUTS");
		addDesiredOutputs();
		
		System.out.print("  MAKING TRAINING PATTERNS OF LENGTH "+lengthTrainingSample);
		ArrayList listOfPatterns = makeListOfTrainingPatterns(lengthTrainingSample);
		
		chunkLength=interLeafSize;
		
		
		System.out.print("  INTERLEAVING PATTERNS");
		makeInterleavedTrainingPatterns(listOfPatterns);
		

		System.out.println("  MAKING TEST SETS");
		makeTestSets(lengthTestSample);
		// not interleaved. just a set of 10 or so keystrokes from each user.. with a final outcome.. looks different ie patterns don't have desired outputs, but the WHOLE thing does??
		// also, several of these from each user??? 	
	}
	

	
	public void getTestAndTraining() throws Exception{
		
		eligibleUserSamples = new UserSample[numUsers];
		
		File origdata = new File(dataArea);	
		if(origdata==null){	
			throw new NoDataException("No such directory "+dataArea);
		}
		// for each  user dir
		File[] datadir = origdata.listFiles();		
		if(datadir==null||datadir.length==0){
			throw new NoDataException("No user data in "+dataArea);
		}
		
		/** for each user.. **/
		int count=0;
		// first, we need to know how many have enough samples
		for(int n=0; n<datadir.length; n++){
			
			if(count>=numUsers){
				return;// that's enough
			}
			String userName=datadir[n].getName();
			
			File trainFile = new File(dataArea+userName+"/train.xml");
			SetOfKeystrokes trainBean = readFileIntoSOK(trainFile,userName);
			//System.out.println(userName+" train: length:"+trainBean.getActuallySent().length()+" text:"+trainBean.getActuallySent());
			
				
			File testFile = new File(dataArea+userName+"/test.xml");
			SetOfKeystrokes testBean = readFileIntoSOK(testFile,userName);
			//System.out.println(userName+" test: length:"+testBean.getActuallySent().length()+" text:"+testBean.getActuallySent());
		
			 //so create em
			eligibleUserSamples[count]= new UserSample(new SetOfKeystrokes[]{trainBean}, new SetOfKeystrokes[]{testBean}, userName);	

			count++;
		}		
		

	}
	
	// depricated
	/*public void getTestAndTraining(HashMap usersSamplesFromFile,int numUsers) throws Exception{
		
		eligibleUserSamples = new UserSample[numUsers];
		
		Iterator it = usersSamplesFromFile.keySet().iterator();
		int count=0;
		// first, we need to know how many have enough samples.. so we know how many users in this exp		
		while(it.hasNext()){
			
			String user = (String)it.next();
			ArrayList set = (ArrayList)usersSamplesFromFile.get(user);
			
			if(set.size()<2){
				throw new Exception("user "+user+" has insfficient data.");
			}
		
			
			
			// training one should be first in list
			SetOfKeystrokes training = (SetOfKeystrokes)set.get(0);
			// just CHECK that this is correct!
			System.out.println(user+" train: length:"+training.getActuallySent().length()+" text:"+training.getActuallySent());
			if(training.getActuallySent().indexOf("country")<0){
				// "country" appears in the training passage and not the test passage. it appears several times and is a simple word -> hopefully everyone has managed to type it!
				throw new Exception("SAMPLES WRONG WAY ROUND");
			}
			
			// test is 2nd
			SetOfKeystrokes test = (SetOfKeystrokes)set.get(1);
	
			// just CHECK that this is correct!
			System.out.println(user+" test: length:"+test.getActuallySent().length()+" text:"+test.getActuallySent());
			
			// THIS IS TO DEMONSTRATE THE EFFECT OF TRAINING WITH DIFF USERS WITH DIFF DATA
			if(this.allDifferentTrainingData){
				// swap em for 2nd user
				if(count==1){
					training=(SetOfKeystrokes)set.get(1);
					test=(SetOfKeystrokes)set.get(0);
				}
			}
//			 so create em
			eligibleUserSamples[count++]= new UserSample(new SetOfKeystrokes[]{training}, new SetOfKeystrokes[]{test}, user);	
		}
	}*/
		

	

	
	
	private void makeTestSets(int lengthTestSample) throws Exception{
		
		

		/**!! chop them down to **/
		/** how many can we get out given that we have length x??**/
		int numTestKeystrokes = eligibleUserSamples[0].getTestSamples()[0].getAllKeystrokes().size();
		int numTestsCanFit = numTestKeystrokes/lengthTestSample;
		if(numTestsCanFit>10){
			numTestsCanFit=10;// 10 is enough - otherwise takes too long to run.
		}
		int size = numTestsCanFit*eligibleUserSamples.length;
		
		testStrings = new SetOfPatterns[size];
		//System.out.println("tss size="+size);
		//ArrayList tss = new ArrayList(size);
		//tss.ensureCapacity(size);
		//System.out.println("size of tss="+tss.size());
				
		// for each user.. get patterns
		for (int n=0; n<eligibleUserSamples.length; n++){		
			UserSample us = eligibleUserSamples[n];
			SetOfKeystrokes trainingsok = us.getTestSamples()[0];
			// 1. make list of key-next-flight-dwells with output PATTERNS out of each sok
			ArrayList patterns = ksToPatterns(trainingsok.getAllKeystrokes(),us.getDesiredOutput());
				
			/**!! chop them down **/

			for(int m=0; m<numTestsCanFit; m++){
				//Pattern[] thispat = new Pattern[lengthTestSample];
				int start = lengthTestSample*m;
				int end = start+lengthTestSample;
				ArrayList thispat=Util.getSliceFromArrayList(patterns,start,end);
				//Pattern[] thispatar = (Pattern[])thispat.toArray(new Pattern[0]);
				// make a teststring
				// we want them shuffled so insert it at correct place..
				int insertat = (eligibleUserSamples.length*m)+n;
				//System.out.println("N="+n+" "+us.getUsername()+" m="+m+" insertat="+insertat);
				testStrings[insertat]=new SetOfPatterns(thispat,us.getUsername());	
			}								
		}
		// finalise it into an array
		//testStrings = (TestString[])tss.toArray(new TestString[0]);
	}
	
	
	private ArrayList makeListOfTrainingPatterns(int lengthTrainingSample) throws Exception{

		ArrayList userToPattern = new ArrayList();
				
		for (int n=0; n<eligibleUserSamples.length; n++){
			UserSample us = eligibleUserSamples[n];
			SetOfKeystrokes trainingsok = us.getTrainingSamples()[0];
			//System.out.println("doing flightdwell for "+us.getUsername());
			// 1. make list of key-next-flight-dwells with output PATTERNS out of each sok
			ArrayList patterns = ksToPatterns(trainingsok.getAllKeystrokes(),us.getDesiredOutput());
			
			/** chop it down **/
			ArrayList desiredSize = Util.cutArrayList(patterns,lengthTrainingSample);
			
			userToPattern.add(desiredSize);
		}
		return userToPattern; 	
	}
	
	
	private void makeInterleavedTrainingPatterns(ArrayList userToPattern){ 
		
		//System.out.println("chunklength:"+chunkLength);
		ArrayList pat = new ArrayList();
		// now shuffle them
		boolean someleft=true;
		while(someleft){
			for(int m=0; m< userToPattern.size(); m++){		
				ArrayList a =  (ArrayList)userToPattern.get(m);
				//System.out.println("this user pattern list is length: "+a.size());
				// take 6 off
				if(a.size()<chunkLength){
					someleft=false;
					break; // give up on whole thing soon as hit the first short one
				}
				for(int i=0; i<chunkLength; i++){
					//System.out.println("removing item "+i + " from lenth "+a.size());
					pat.add(a.remove(0));// does this work ??????
				}
			}
		}
		this.trainingPatterns=(Pattern[])pat.toArray(new Pattern[0]);
		
	}
	
	
	private ArrayList ksToPatterns(LinkedList keystrokes, float[] outputAlways) throws Exception{
    	
		ArrayList thisUserPat = new ArrayList(); 
		
		/** start at 1 not 0 as no flightto time.. **/
		for(int p=1; p<keystrokes.size(); p++){
	
			KeyStroke ks = (KeyStroke)keystrokes.get(p);

	    	// first 100 for encoding the keycode, then 101 for flight and 102 for dwell..
	    	// so it goes 0 0 0 0 0 0 0 1 0 0 0 0 0 0 -> 0 0 0   -2.6 4.3
	    	float[] inputBits=new float[103];
	    	
	    		    	
	    	int code = ks.getKeycode();
	    	if(code>100){
	    		//System.out.println("warning, code="+code+" > 100 so data ignored");
	    		continue;
	    	}
	    	inputBits[code]=1; // the rest default to 0	   
	    	inputBits[101]=transform(ks.getFlightTo());
	    	inputBits[102]=transform(ks.getDwell()); 		    	
	    	
	    	//System.out.println(p+ "/"+keystrokes.size()+" "+/*code+" "+*/"f:"+ks.getFlightTo()+" d:"+ks.getDwell()+" "+Util.join("",inputBits));
			Pattern pa = new Pattern(inputBits, outputAlways);		    		
			//pat.add(pa);
			thisUserPat.add(pa);
		}
		return thisUserPat;
	}
	

	
	/**
	 * @return Returns the testStrings.
	 */
	public SetOfPatterns[] getTestStrings() {
		return testStrings;
	}
	
	/** override the version in SampleFileReaderDAO **/
	protected void doChecks(SetOfKeystrokes sok) throws Exception{
    	//sok.checkStringSent();
		// do nothing
	}
	
}
