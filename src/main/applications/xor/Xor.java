
package applications.xor;


import core.datapatterns.SetOfPatterns;
import core.structure.layers.*;
import core.structure.networks.BackPropagationNetwork;
import core.utils.*;

/** uses a BackpropgationNetwork with 2,2,1 layers, and SetOfPatterns to read data**/
public class Xor {


	float desiredError =0.001f;
	int maxTrials = 10000;
	private TestResults tr;
	
	public Xor(){
		
		try{
			
		
			//1. set up structure
			//InputLayer input = new InputLayer(2,"input");
			InputLayer input = new InputLayer(2,"input",true);
		
			//HiddenLayer hidden = new HiddenLayer(2,"hidden");
			HiddenLayer hidden = new HiddenLayer(2,"hidden",true);
		
			OutputLayer output = new OutputLayer(1,"output");
			BackPropagationNetwork me = new BackPropagationNetwork(input,new HiddenLayer[]{hidden},output);					
			me.setLearningRate(0.3f);
				
			//2. read data patterns			
			String[] files = {"src/main/applications/xor/xor.csv"};		
			SetOfPatterns s = new SetOfPatterns(files);
		
			//3. train	
			//me.train(numTrials, s.getPatterns());	
			int numTrials= me.train(s.getPatterns(),desiredError, maxTrials);		
			System.out.println("took "+numTrials+" to reach error of "+desiredError);			
		
			//4. test data is same as test data
			tr = me.test(s.getPatterns(),s.getAllPossOutputs());
		
			
			System.out.println(tr.toString());
			
			
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
		
	}

	
	public static void main(String[] args) {		
		Xor x = new Xor();
		
	}
	
	
	public TestResults getTr() {
		return tr;
	}
}
