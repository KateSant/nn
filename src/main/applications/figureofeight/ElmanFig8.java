
package applications.figureofeight;


import core.structure.layers.*;
import core.structure.networks.ElmanNetwork;
import core.utils.Util;
import core.utils.plot.*;


/** elman reference implementation!**/
public class ElmanFig8 {

	float desiredError =0.0001f;// go for a lower error?
	int maxTrials = 30000;
	
	/** num to test ***/
	protected int numOut = 20;
	ElmanNetwork me;


	
	/** this one trains with reinitialising, and then starts at 0, (and uses non-prev test) **/
	protected float[][] trainAndTest() throws Exception{
		System.out.println("trainAndTest() in "+this.getClass());
		
		
		//me.setReinitialiseEveryTrialStart(false);
		
		me.setReinitialiseEverySetStart(true);

		//2. read data patterns					
		DataPattern_FigureOfEight sh = new DataPattern_FigureOfEight();
		
		//3. train				
		//me.train(numTrials, sh.getPatterns());
		int numTrials= me.train(sh.getPatterns(),desiredError, maxTrials);		
		System.out.println("took "+numTrials+" to reach error of "+desiredError);
		
		//3. test

		int startAtPoint = 0; 

		float[] startP = sh.getPatterns()[startAtPoint].getInputNumbers();
		System.out.println("start off with:"+Util.join(",",startP));
				
		//float[][] testOutput = me.testWithStartInputPatternAndPrev(prevP,startP, numOut);		
		float[][] testOutput = me.testWithStartInputPattern(startP, numOut);
		
		return testOutput;
	}
	
	
	protected void structure()throws Exception{
		//1. set up structure - first 100 are for keycode, 101 is flight and 102 is dwell
		
			InputLayer input = new InputLayer(2,"input",true);// we want an offsetneuron		
			HiddenLayer hidden = new HiddenLayer(8,"hidden",true);		
			OutputLayer output = new OutputLayer(2,"output");
			me = new ElmanNetwork(input,new HiddenLayer[]{hidden},output);					
			me.setLearningRate(0.5f);
			me.setNearToOne(0.6f);
			me.setNearToZero(0.4f);
			me.setLogevery(1000);
	}
	
	
	public ElmanFig8() {
			
		try{
			structure();
	

/*				
		//me.setReinitialiseEveryTrialStart(false);
		me.setReinitialiseEveryTrialStart(true);

		//2. read data patterns					
		SetOfPatternsFigureOfEight sh = new SetOfPatternsFigureOfEight();
		
		//3. train				
		me.train(numTrials, sh.getPatterns());
		
		//3. test
		int numOut = 60;
		int startAtPoint = 6;  // WHAT IF WE DON'T START AT 0??
		float[] prevP = sh.getPatterns()[startAtPoint-1].getInputNumbers();
		float[] startP = sh.getPatterns()[startAtPoint].getInputNumbers();

				
		//float[][] testOutput = me.testWithStartInputPatternAndPrev(prevP,startP, numOut);		
		float[][] testOutput = me.testWithStartInputPattern(startP, numOut);
							
		
//		OK, not starting at point 0 then... (important because i will need to be able to come in at any point)
//		 
//		- didn't work at first because 0.5 on the context for "previous" not applicable to other points
//		- so had to stop reinitialising to 0.5 at start of every trial 
//				- ie me.setReinitialiseEveryTrialStart(false) (see above)
 * 				- makes huge difference
 
//		- and give first two points..
//				- ie testWithStartInputPatternAndPrev... .. try all options:
 			    - actually, not sure that is necessary!
*/	
		
			float[][] testOutput = trainAndTest();
			plot(testOutput);
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
		
	}
	
	protected void plot(float[][]testOutput){
		
		Point[] points = new Point[testOutput.length];
		for(int n=0; n<testOutput.length; n++){
			float[] outputArray = testOutput[n];
			points[n] = new Point(outputArray[0],outputArray[1]);
			System.out.println("points["+n+"]="+points[n].getX()+" , "+points[n].getY());
			
		}
		Plot plot = new Plot(points);
	}

	
	
	public static void main(String[] args) {		
		
		ElmanFig8 x = new ElmanFig8();

		
	}
}
