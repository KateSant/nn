
package applications.figureofeight;



public class ElmanFig8_noReInitialise_and_startNonZero extends ElmanFig8 {

	
	/** this one trains without reinitialising, but starts at 0! **/
	protected float[][] trainAndTest()throws Exception{
		
		
		me.setReinitialiseEverySetStart(false);

		//2. read data patterns					
		DataPattern_FigureOfEight sh = new DataPattern_FigureOfEight();
		
		//3. train				
		int numTrials= me.train(sh.getPatterns(),desiredError, maxTrials);		
		System.out.println("took "+numTrials+" to reach error of "+desiredError);
		
		//3. test

		int startAtPoint = 4;  
		
		float[] startP = sh.getPatterns()[startAtPoint].getInputNumbers();
		
		float[][] testOutput = me.testWithStartInputPattern(startP, numOut);
		
		return testOutput;
	}
	
	public static void main(String[] args) {		
		try{
			ElmanFig8_noReInitialise_and_startNonZero x = new ElmanFig8_noReInitialise_and_startNonZero();
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
}
