
package applications.figureofeight;



public class ElmanFig8_testStartNotZero extends ElmanFig8 {

	
	/** this one trains with reinitialising, and then starts at point 4! **/
	protected float[][] trainAndTest()throws Exception{
		
		//me.setReinitialiseEveryTrialStart(false);
		me.setReinitialiseEverySetStart(true);

		//2. read data patterns					
		DataPattern_FigureOfEight sh = new DataPattern_FigureOfEight();
		
		//3. train				
		int numTrials= me.train(sh.getPatterns(),desiredError, maxTrials);		
		System.out.println("took "+numTrials+" to reach error of "+desiredError);
		
		//3. test

		int startAtPoint = 4;  // WHAT IF WE DON'T START AT 0??
		
		float[] startP = sh.getPatterns()[startAtPoint].getInputNumbers();
		
		float[][] testOutput = me.testWithStartInputPattern(startP, numOut);
		
		return testOutput;
	}
	
	public static void main(String[] args) {	
		try{
			ElmanFig8_testStartNotZero x = new ElmanFig8_testStartNotZero();
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
}
