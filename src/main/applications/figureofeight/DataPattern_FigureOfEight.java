/*
 * Created on 06-Feb-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package applications.figureofeight;

import java.util.*;
import java.io.*;

import core.datapatterns.Pattern;
import core.datapatterns.SetOfPatterns;
import core.utils.plot.Point;



public class DataPattern_FigureOfEight extends SetOfPatterns{


	
	public DataPattern_FigureOfEight(){

		Point[] points = new Point[13];
		points[0] = new Point (0.5f,0.6f);
		points[1] = new Point (0.4f,0.3f);
		points[2] = new Point (0.2f,0.3f);
		points[3] = new Point (0.1f,0.6f);			
		points[4] = new Point (0.2f,0.9f);		
		points[5] = new Point (0.4f,0.9f);
		points[6] = new Point (0.5f,0.6f);
		points[7] = new Point (0.6f,0.3f);
		points[8] = new Point (0.8f,0.3f);
		points[9] = new Point (0.9f,0.6f);
		points[10] = new Point (0.8f,0.9f);
		points[11] = new Point (0.6f,0.9f);
		points[12] = new Point (0.5f,0.6f);
		
		for(int n=0; n<points.length-1; n++){
			Point po = points[n];
			Point pnext = points[n+1];
			float[] inputBits = new float[]{po.getX(),po.getY()};
			
			// output is next pattern..
			float[] outputBits = new float[]{pnext.getX(),pnext.getY()};
			Pattern p = new Pattern(inputBits, outputBits);
			pat.add(p);
		}           
	
	}
	

}
