package applications.password.dao;

import java.io.*;

import java.util.*;

import core.datapatterns.Pattern;
import core.utils.Util;

import beans.*;


/** reads the samples from their directories, parses them into SetOfKeystrokes lists belonging to each user **/
public abstract class SampleFileReaderDAO {
	

	
	protected String dataArea;
	
	public SampleFileReaderDAO(String dataArea){
		this.dataArea=dataArea;
	}
	
	
	/** get a hashmap which consists of username->userSamples objects, 
	 * by getting a list of userdirs, and passing each to getSetOfSamplesForUser **/
	protected HashMap getDataFromFiles() throws NoDataException{
		
		HashMap h = new HashMap();
		File origdata = new File(dataArea);	
		if(origdata==null){	
			throw new NoDataException("No such directory "+dataArea);
		}
		// for each  user dir
		File[] datadir = origdata.listFiles();	
		if(datadir==null||datadir.length==0){
			throw new NoDataException("No user data in "+dataArea);
		}
		for(int n=0; n<datadir.length; n++){
			String user=datadir[n].getName();
			ArrayList set = getSetOfSamplesForUser(user);
			h.put(user,set);
		}
		return h;	
	}
	
	
	/** opens all the xml files in a userdir, reads them, 
	 * parses them into SetOfKeystrokes objects, checks they are valid, stores them in an arraylist **/
	protected ArrayList getSetOfSamplesForUser(String userName){
		ArrayList al = new ArrayList();
		// go inside for sample files
                System.out.println("dataArea+userName="+dataArea+userName);
                try{
                    File[] samples = new File(dataArea+userName).listFiles();
                        for(int m=0; m<samples.length; m++){
					
			//String fileName = samples[m].toString();
			
                            try{
				SetOfKeystrokes bean = readFileIntoSOK(samples[m],userName);
				al.add(bean);
                            }catch(Exception e){
				System.err.println("skipping:"+e.getMessage());
                            }

                        }   
                }catch(Exception e){
                    System.err.println("skipping:"+e.getMessage());
                }
		return al;
	}
	
	protected SetOfKeystrokes readFileIntoSOK(File file, String userName) throws Exception{
		
		String name = file.toString();
		if(name.lastIndexOf("\\")>0){
			name= name.substring(name.lastIndexOf("\\")+1);
		}
		// read the file
		try {
	        BufferedReader in = new BufferedReader(new FileReader(file));
	        StringBuffer sb = new StringBuffer();
	        String str;
	        while ((str = in.readLine()) != null) {
	        	
	            sb.append(str);
	        }
	        in.close();
		    // now parse it into a SetOfKeystrokes
		    try{
		    	SetOfKeystrokes sok = new SetOfKeystrokes(sb.toString());
		    	// check correct sent
		    	doChecks(sok);
		    	
		    	if(!sok.getUserName().equals(userName)){
		    		//System.out.println("username from sample:"+sok.getUserName());
		    		throw new Exception("Wrong username in "+name);
		    	}		
		    	
		    	//al.add(sok);
		    	//System.out.println(name+" "+sok.getActuallySent());
		    	return sok;
		    	

		    } catch (WrongStringException ws) {
		    	System.err.println("Wrong String in "+file+" "+ws.getMessage());
		    	throw ws;
		    	
		    } catch (Exception en) {
		    	System.err.println("Skipping sample:"+file+" err: "+en.getMessage());
		    	throw en;
		    }			        
	    } catch (IOException e) {
	    	System.err.println("could not read: "+file);
	    	throw e;
	    }
	}

	/** override in freeform one **/
	protected void doChecks(SetOfKeystrokes sok) throws Exception{
    	sok.checkStringSent();
	}

	


}
