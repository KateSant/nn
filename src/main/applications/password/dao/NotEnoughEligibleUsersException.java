/*
 * Created on 03-Aug-2005 by Katie P
 */
package applications.password.dao;


public class NotEnoughEligibleUsersException extends Exception {

	public NotEnoughEligibleUsersException() {
		super();

	}

	public NotEnoughEligibleUsersException(String message) {
		super(message);
		
	}

}
