
package applications.password.dao;
import beans.*;

public class UserSample {

	SetOfKeystrokes[] trainingSamples;
	SetOfKeystrokes[] testSamples;
	String username;
	float[] desiredOutput;
	
	
	

	/**
	 * @param trainingSamples
	 * @param testSamples
	 * @param username
	 * @param desiredOutput
	 */
	public UserSample(SetOfKeystrokes[] trainingSamples,
			SetOfKeystrokes[] testSamples, String username,
			float[] desiredOutput) {
		super();
		this.trainingSamples = trainingSamples;
		this.testSamples = testSamples;
		this.username = username;
		this.desiredOutput = desiredOutput;
	}
	
	
	public UserSample(SetOfKeystrokes[] trainingSamples,
			SetOfKeystrokes[] testSamples, String username) {
		super();
		this.trainingSamples = trainingSamples;
		this.testSamples = testSamples;
		this.username = username;
	}
	
	
	/**
	 * @return Returns the desiredOutput.
	 */
	public float[] getDesiredOutput() {
		return desiredOutput;
	}
	/**
	 * @param desiredOutput The desiredOutput to set.
	 */
	public void setDesiredOutput(float[] desiredOutput) {
		this.desiredOutput = desiredOutput;
	}
	/**
	 * @return Returns the testSamples.
	 */
	public SetOfKeystrokes[] getTestSamples() {
		return testSamples;
	}
	/**
	 * @param testSamples The testSamples to set.
	 */
	public void setTestSamples(SetOfKeystrokes[] testSamples) {
		this.testSamples = testSamples;
	}
	/**
	 * @return Returns the trainingSamples.
	 */
	public SetOfKeystrokes[] getTrainingSamples() {
		return trainingSamples;
	}
	/**
	 * @param trainingSamples The trainingSamples to set.
	 */
	public void setTrainingSamples(SetOfKeystrokes[] trainingSamples) {
		this.trainingSamples = trainingSamples;
	}
	/**
	 * @return Returns the username.
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username The username to set.
	 */
	public void setUsername(String username) {
		this.username = username;
	}
}
