/*
 * Created on 19-Jun-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package applications.password.dao;
import core.datapatterns.*;
import core.utils.Util;

import java.util.*;

import beans.*;


/**
 * from the available data, picks out X users, chops of first Y characters, and uses Z of their patterns... etc
 * 
 */
public class PasswordSampleDataDAO extends SampleFileReaderDAO{

	
	protected float scale = 0.01f;/********* TODO is this scale right??**/	
	
	/** user->data map **/
	protected UserSample[] eligibleUserSamples;
	
	/** outputs... **/
	protected Pattern[] trainingPatterns; // patterns taken from eligibleUserSamples and interleaved and tweaked via activ. function and flight and dwell extracted
	protected Pattern[] testPatterns;
	
	/** inputs **/
	protected int lengthofSample; 
	protected int numTrainingSamples; 
	protected int numTestingSamples; 
	protected int numUsers;
	
	public PasswordSampleDataDAO(String pass){// useful for FreeformSampleData to inherit straight from SampleFileReader
		super(pass);
	}
	
	public PasswordSampleDataDAO(int lengthofSample, int numTrainingSamples, int numTestingSamples, int numUsers, String dataDir) throws NotEnoughEligibleUsersException, BadTimingDataException, NoDataException{
		
		
		super(dataDir);
		
		this.lengthofSample=lengthofSample;
		this.numTrainingSamples=numTrainingSamples;
		this.numTestingSamples=numTestingSamples;
		this.numUsers=numUsers;
		
		// read from files
		System.out.println("READING FILE DATA...  ");
		HashMap usersSamplesFromFile = getDataFromFiles();
		

		
		/* make user data out of raw data.....->
		check users have enough samples, then allocate desiredoutputs, separate training from test samples..
		eliminate any users with too few samples. 	cut down any with too many**/
		//System.out.println("GETTING ELIGIBLE USERS...  ");
		getEligibleUsers(usersSamplesFromFile, numTrainingSamples, numTestingSamples, numUsers);		
		addDesiredOutputs();
		
		// now, cut down patterns to desried length..
		//System.out.print("CUTTING DOWN PATTERN LENGTH...  ");
		cutDownPatternLengths();
		
		// now, turn this into a set of patterns, ie, flight and dwell times please.
		//also interleave them...		
		//System.out.println("MAKING PATTERNS FROM SETOFKEYSTROKES - AND INTERLEAVE...  ");			
		trainingPatterns = getPatternsInterleavedAndFlightDwell(eligibleUserSamples);	
		testPatterns = getPatternsInterleavedAndFlightDwell_TESTPatterns(eligibleUserSamples);
				
	}
	
	/** 
	 * remove any that don't have enough samples.. 
	 * cut down those with too many samples
	 * give them desired outputs**/
	public void getEligibleUsers(HashMap usersSamplesFromFile, int trainingSamplesEachUser, int testSamplesEachUser, int numUsers) throws NotEnoughEligibleUsersException{
			
		int totalSamplesEachUser = trainingSamplesEachUser+testSamplesEachUser;
		System.out.println("totalSamplesEachUser:"+totalSamplesEachUser+" rainingSamplesEachUser:"+trainingSamplesEachUser+" testSamplesEachUser:"+testSamplesEachUser);
		Iterator it = usersSamplesFromFile.keySet().iterator();

		// first, we need to know how many have enough samples.. so we know how many users in this exp
		ArrayList usersf = new ArrayList();
		while(it.hasNext()){
			String user = (String)it.next();
			ArrayList set = (ArrayList)usersSamplesFromFile.get(user);
			if(set.size()<totalSamplesEachUser){
				System.out.println(user+" - "+set.size()+" samples.. NOT ENOUGH SAMPLES - needed "+totalSamplesEachUser);
				
			}else{
				System.out.println(user+" - "+set.size()+" samples");
				
				usersf.add(user);
				
			}
		}	
		
		int elig = usersf.size();
		//System.out.println("** "+ elig+" users are eligible");
		
		if(elig<numUsers){
			throw new NotEnoughEligibleUsersException("not enough eligible users with enough samples.. "+elig+" were eligible, but you wanted "+numUsers);
		}
		
		if(elig>numUsers){
			//System.out.println("** BTW..more eligible users than necessary BTW.. eliminating some");
			elig=numUsers;
		}	
		
		//eligibleUsers = usersf.toArray(new UserSample[]);
			
		eligibleUserSamples = new UserSample[elig];
			
		for(int n=0; n<elig; n++){
			// username
			String username = (String)usersf.get(n);
			ArrayList set = (ArrayList)usersSamplesFromFile.get(username);
			// the training and test sets
			List trainingSet = set.subList(0,trainingSamplesEachUser);
			SetOfKeystrokes[] trs = (SetOfKeystrokes[])trainingSet.toArray(new SetOfKeystrokes[0]);
			List testSet = set.subList(trainingSamplesEachUser,totalSamplesEachUser);
			SetOfKeystrokes[] tes = (SetOfKeystrokes[])testSet.toArray(new SetOfKeystrokes[0]);
			
			// so create em
			/***************** create them alredy??? ****/
			eligibleUserSamples[n]= new UserSample(trs, tes, username);		
		}			
	}
	
	protected void addDesiredOutputs(){
		
		// now, can create output patterns for users (if there are 4 users, user1 is 1000 user2 is 0100.. etc)
		for(int n=0; n<eligibleUserSamples.length; n++){
			UserSample us =eligibleUserSamples[n];
			//System.out.println("eligibleusersample:"+us);
			// desired output
			float[] des = new float[eligibleUserSamples.length];
			for(int z=0; z<eligibleUserSamples.length; z++){
				if (z==n){// n is the position of this user in the set
					des[z]=1;
				}else{
					des[z]=0;
				}
			}
			System.out.print("desired output for "+us.getUsername()+" = "+ Util.join(" ",des));
			// so create em
			us.setDesiredOutput(des);		
		}			
		System.out.println();
	}
	
	protected void cutDownPatternLengths(){
		
		for(int us=0; us<eligibleUserSamples.length; us++){
			UserSample user = eligibleUserSamples[us];
		
			SetOfKeystrokes[] samples = user.getTrainingSamples();
			cutDown(samples);
			// do test patterns too...
			cutDown(user.getTestSamples());
		}
	}
	private void cutDown(SetOfKeystrokes[] samples){
		for (int n=0; n<samples.length; n++){
			LinkedList l = samples[n].getAllKeystrokes();
			// hmm,
			LinkedList firstBit= new LinkedList();
			for(int m=0; m<this.lengthofSample; m++){
				firstBit.add(l.get(m));
			}
			samples[n].setAllKeystrokes(firstBit);				
		}
	}
	
	
	/** now, turn this into a set of patterns, ie, flight and dwell times please. also interleave them...**/
	protected Pattern[] getPatternsInterleavedAndFlightDwell(UserSample[] eligibleUserSamples) throws NotEnoughEligibleUsersException, BadTimingDataException{
		
		if(eligibleUserSamples==null||eligibleUserSamples.length<1){
			throw new NotEnoughEligibleUsersException("no eligible users!");
		}		
		int numUsers = eligibleUserSamples.length;
		int numPatternsEach = eligibleUserSamples[0].getTrainingSamples().length;		
		//System.out.println("numUsers:"+numUsers+" numPatternsEach:"+numPatternsEach);		
		Pattern[]pats = new Pattern[numUsers*numPatternsEach];		
		int z=0;
		for (int pat=0; pat<numPatternsEach; pat++){
			for(int us=0; us<numUsers; us++){
				UserSample user = eligibleUserSamples[us];
				
				// input
				SetOfKeystrokes in = user.getTrainingSamples()[pat];
				float[] input = getFlightDwellAndTransformedPattern(in);				
				//System.out.println("pattern: "+in.getActuallySent());
				
				// output
				float[]output=user.getDesiredOutput();	
				pats[z] = new Pattern(input,output, user.getUsername());
				
				//System.out.println(user.getUsername()+" pattern:"+z+" output:"+Util.join(" ",output)+" input:"+Util.join(",\t",input));
				z++;
			}
		}
		return pats;
		
	}
	
	/** some repetition here.. how to integrate the two **/
	protected Pattern[] getPatternsInterleavedAndFlightDwell_TESTPatterns(UserSample[] eligibleUserSamples) throws BadTimingDataException{
		
		int numUsers = eligibleUserSamples.length;
		int numPatternsEach = eligibleUserSamples[0].getTestSamples().length;	
		//System.out.println("numUsers:"+numUsers+" numPatternsEach_TEST:"+numPatternsEach);	
		Pattern[]pats = new Pattern[numUsers*numPatternsEach];		
		int z=0;
		for (int pat=0; pat<numPatternsEach; pat++){
			for(int us=0; us<numUsers; us++){
				UserSample user = eligibleUserSamples[us];
				// input
				SetOfKeystrokes in = user.getTestSamples()[pat];
				float[] input = getFlightDwellAndTransformedPattern(in);
				
				//System.out.println("input length"+input.length+" pattern: "+in.getActuallySent());
				// output
				float[]output=user.getDesiredOutput();	
				pats[z] = new Pattern(input,output, user.getUsername());
			
				//System.out.println("pattern:"+z+" output:"+Util.join(" ",output)+" input:"+Util.join(",\t",input));
				z++;
			}
		}
		return pats;		
	}
	
	
	/** setofkeystrokes object is neutral as to what you're going to do with keydown and keyup times.. this takes flight and dwell, it is for the static one.  in the dynamic one, you would do something different, which included keychar etc 
	 * this also TRANSFORMS the numbers.. ie, so none are <-1 or >1 **/
	protected float[] getFlightDwellAndTransformedPattern(SetOfKeystrokes in) throws BadTimingDataException{
			
		LinkedList l = in.getAllKeystrokes();
		float[] out = new float[(l.size()*2)-1];	// no flightTo on the first one	
		int whereInOut=0;
		for(int n=0; n<l.size(); n++){
			KeyStroke k = (KeyStroke)l.get(n);
        	if(n>0){
        		out[whereInOut++] = transform(k.getFlightTo());			
        	}
        	out[whereInOut++] = transform(k.getDwell());
		}
		return out;
		
	}
	
	protected float transform(float x){		
		float y = (2 / (1 + (float)Math.exp(-scale*x)))  -1 ;	
		//System.out.println("scaled "+x+" to "+y);
		return y;
		//return x;
	}

	
	/**
	 * @return Returns the testPatterns.
	 */
	public Pattern[] getTestPatterns() {
		return testPatterns;
	}
	/**
	 * @return Returns the trainingPatterns.
	 */
	public Pattern[] getTrainingPatterns() {
		return trainingPatterns;
	}
	
	public HashMap getAllDesiredOutputs() {
		HashMap hp = new HashMap();
		for (int n=0; n<eligibleUserSamples.length; n++){
			UserSample us = eligibleUserSamples[n];
			hp.put(us.getUsername(),us.getDesiredOutput());
		}
		return hp;
	}
	public float[][] getAllPossibleDesiredOutputs(){
		float[][] allPoss = new float[eligibleUserSamples.length][];
		for (int n=0; n<eligibleUserSamples.length; n++){
			UserSample us = eligibleUserSamples[n];	
			allPoss[n]=us.getDesiredOutput();		
		}
		return allPoss;
	}
}
