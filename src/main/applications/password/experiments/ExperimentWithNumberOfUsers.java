
package applications.password.experiments;

import core.utils.plot.Line;
import core.utils.plot.Graph;
import core.utils.plot.Point;
import applications.password.Password;


public class ExperimentWithNumberOfUsers extends PasswordMultiRunner{

	int lengthSample = 25;
	int numSamples=10; //
	int[] numUsers = new int[]{2,3,4,5,6,7,8};/** need to up this! **/
	
	
	protected void test() throws Exception{
		
		tests = new Password[numUsers.length];
		points = new Point[numUsers.length];
		falsePositivesPoints=new Point[numUsers.length];
		falseNegativesPoints=new Point[numUsers.length];
		for(int n=0; n<numUsers.length; n++){
			tests[n]= new Password(lengthSample,numSamples,numUsers[n]);//2 users
			multiRun(tests[n],n,numUsers[n]);

		}
	
	}
	
	
	protected void plot(){		
		super.plot();
		//xmax ymax, xsteps, ysteps
		p = new Graph(new Line[]{correctLine,falsePositivesLine,falseNegativesLine}, 15, 100, 1f, 10f, "Effect of number of users (Password/Backprop)", "num users","accuracy(%)");
		
		p.plot();
	}
	
	
	
	public static void main(String args[]){
		try{
			ExperimentWithNumberOfUsers me = new ExperimentWithNumberOfUsers();
			System.out.println("plotting..");
			me.test();
			me.plot();
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
	

}
