/*
 * Created on 10-Jul-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package applications.password.experiments;

import java.awt.Color;

import core.utils.plot.Line;
import core.utils.plot.Graph;
import core.utils.plot.Point;
import applications.password.Password;
import core.utils.*;

public abstract class PasswordMultiRunner {
	
	protected Password[] tests;
	protected Point[] points;
	protected Point[] falsePositivesPoints;
	protected Point[] falseNegativesPoints;
	
	protected int numTimesPerPoint = 10;
	protected Line correctLine;
	protected Line falseNegativesLine;
	protected Line falsePositivesLine;

	Graph p;
	
	protected void multiRun(Password p, int xAxis, float variable) throws Exception{
		float totalCorrect=0;
		float totalFalsePositives=0;
		float totalFalseNegatives=0;
		
		for (int n=0; n<numTimesPerPoint; n++){
			p.go();
			float result = p.getTestResults().getPercCorrect();
			//System.out.println("** result:"+result+ "p.lengthsample"+p.getLengthofSample());
			totalCorrect+= p.getTestResults().getPercCorrect();
			totalFalsePositives+=p.getTestResults().getPercFalsePositives();
			totalFalseNegatives+=p.getTestResults().getPercFalseNegatives();
		}
		float avCorrect = totalCorrect/numTimesPerPoint;
		float avFalsePositives = totalFalsePositives/numTimesPerPoint;
		float avFalseNegatives = totalFalseNegatives/numTimesPerPoint;
		//System.out.println("totalFalseNegatives:"+totalFalseNegatives+" avFalseNegatives:"+avFalseNegatives);
		
		points[xAxis]=new Point(variable,avCorrect);
		falsePositivesPoints[xAxis]=new Point(variable,avFalsePositives);
		falseNegativesPoints[xAxis]=new Point(variable,avFalseNegatives);
		//System.out.println("putting xAxis:"+xAxis+" variable:"+variable+" avFalseNegatives:"+avFalseNegatives);
	}
	
	protected abstract void test() throws Exception;
	
	protected void plot(){
		// now plot it..
		correctLine = new Line(points,"Correct",Color.BLACK);
		falsePositivesLine = new Line(falsePositivesPoints,"False Positives",Color.RED);
		falseNegativesLine = new Line(falseNegativesPoints,"False Negatives",Color.YELLOW);
	}
}
