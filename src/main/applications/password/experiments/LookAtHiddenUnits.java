/*
 * Created on 31-May-2005 by Katie P
 */
package applications.password.experiments;

import applications.password.Password;


public class LookAtHiddenUnits {

	/** what is proportion of hidden units... to input/output ??**/
	public static void main(String[] args) {
		try{
						
			// 1st param is length of sample, 2nd is number of samples to use,
			Password t = new Password(30, 8, 4);
			t.setHiddenUnits(5);
			t.go();			
				
			System.out.println("--------------------------------------------------------------------------");
			System.out.println(t.getTestResults().toString());				
					
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
}
