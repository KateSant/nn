
package applications.password.experiments;

import core.utils.plot.Line;
import core.utils.plot.Graph;
import core.utils.plot.Point;
import applications.freeformpassword.FreeformPassword;
import applications.password.Password;


public class ExperimentWithOvertraining extends PasswordMultiRunner{



	private int lengthOfSample = 10;
	private int numUsers = 5;
	private int numSamples = 15;
	
	private float[] desiredErrors= new float[]{0.00001f,0.00005f,0.0001f,0.001f,0.005f,0.01f,0.1f};

	
	protected void test() throws Exception{			
		// 1st param is length of sample, 2nd is number of samples to use in training, 3rd is number of users	
		tests = new Password[desiredErrors.length];
		points = new Point[desiredErrors.length];
		falsePositivesPoints=new Point[desiredErrors.length];
		falseNegativesPoints=new Point[desiredErrors.length];
		
		for(int n=0; n<desiredErrors.length; n++){

			// set it up
			tests[n]= new Password(lengthOfSample,numSamples,numUsers);//2 users
			tests[n].setDesiredError(desiredErrors[n]);
			
			// run it.. 10 times		
			multiRun(tests[n],n,desiredErrors[n]);

		}
	}
	

	
	protected  void plot(){		
		super.plot();
		//xmax ymax, xsteps, ysteps
		p = new Graph(new Line[]{correctLine,falsePositivesLine,falseNegativesLine}, 0.011f, 100, 0.001f, 10f, "Effect of changing net error (overtraining/underraining) (Password/Backprop)", "net error","accuracy(%)");
		p.plot();
	}
	
	public static void main(String args[]){
		try{
			ExperimentWithOvertraining me = new ExperimentWithOvertraining();
			me.test();
			me.plot();
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}

}
