/*
 * Created on 01-Jun-2005 by Katie P
 */
package applications.password.experiments;

import applications.password.Password;


public class LookAtHowSimilarArePatterns {

	public static void main(String[] args) {
		try{
						
			/*// 1st param is length of sample, 2nd is number of samples to use, 3rd is num users
			TestLengthOfSample t = new TestLengthOfSample(10, 7, 4);
			t.go();					
			System.out.println("--------------------------------------------------------------------------");
			System.out.println(t.getTestResults().toString());		
			t.plotPatterns("new patterns.  length 10");
			

			
			// 1st param is length of sample, 2nd is number of samples to use, 3rd is num users
			TestLengthOfSample t2 = new TestLengthOfSample(20, 7, 4);
			t2.go();					
			System.out.println("--------------------------------------------------------------------------");
			System.out.println(t2.getTestResults().toString());		
			t2.plotPatterns("new patterns.  length 50");
			
			

			// 1st param is length of sample, 2nd is number of samples to use, 3rd is num users
			TestLengthOfSample t3 = new TestLengthOfSample(50, 7, 4);
			t3.go();					
			System.out.println("--------------------------------------------------------------------------");
			System.out.println(t3.getTestResults().toString());		
			t3.plotPatterns("new patterns.  length 50");*/
			
		
			// 1st param is length of sample, 2nd is number of samples to use, 3rd is num users
			
			Password t2 = new Password(10, 10, 6);
			t2.go();					
			System.out.println("--------------------------------------------------------------------------");
			System.out.println(t2.getTestResults().toString());		
			t2.plotPatterns("Flight-dwell pattern for \"this is my \" for 10 samples from 6 users.");
			
			
					
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
}
