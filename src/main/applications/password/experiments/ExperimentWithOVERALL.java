
package applications.password.experiments;

import core.utils.plot.Line;
import core.utils.plot.Graph;
import core.utils.plot.Point;
import applications.freeformpassword.FreeformPassword;
import applications.password.Password;


public class ExperimentWithOVERALL extends PasswordMultiRunner{



	private int lengthOfSample = 25;
	private int numUsers = 8;
	private int numSamples = 10;
	
	
	protected void test() throws Exception{			
		// 1st param is length of sample, 2nd is number of samples to use in training, 3rd is number of users	
		tests = new Password[1];
		points = new Point[1];
		falsePositivesPoints=new Point[1];
		falseNegativesPoints=new Point[1];
		
	
		// set it up
		tests[0]= new Password(lengthOfSample,numSamples,numUsers);//2 users
		
		
		// run it.. 10 times		
		multiRun(tests[0],1,1);

		
	}
	

	
	protected  void plot(){		
		super.plot();
		//xmax ymax, xsteps, ysteps
		p = new Graph(new Line[]{correctLine,falsePositivesLine,falseNegativesLine}, 0.011f, 100, 0.001f, 10f, "OVERALL", "net error","accuracy(%)");
		p.plot();
	}
	
	public static void main(String args[]){
		try{
			ExperimentWithOVERALL me = new ExperimentWithOVERALL();
			me.test();
			me.plot();
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}

}
