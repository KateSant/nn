
package applications.password.experiments;

import core.utils.plot.Line;
import core.utils.plot.Graph;
import core.utils.plot.Point;
import applications.password.Password;


public class ExperimentWithNumberOfTrainingSamples  extends PasswordMultiRunner{

	int[] numSamples = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
	int numUsers = 5;
	int lengthSample = 10;
	
	
	protected void test() throws Exception{
		
		tests = new Password[numSamples.length];
		points = new Point[numSamples.length];
		falsePositivesPoints=new Point[numSamples.length];
		falseNegativesPoints=new Point[numSamples.length];
		
		for(int n=0; n<numSamples.length; n++){
			tests[n]= new Password(lengthSample,numSamples[n],numUsers);//2 users
			multiRun(tests[n],n,numSamples[n]);
			
		}
	}
	
	
	protected  void plot(){		
		super.plot();
		//xmax ymax, xsteps, ysteps
		p = new Graph(new Line[]{correctLine,falsePositivesLine,falseNegativesLine}, 15, 100, 1f, 10f, "Effect of number of training samples (Password/Backprop)", "num samples","accuracy(%)");
		p.plot();
	}
	
	
	
	public static void main(String args[]){
		try{
			ExperimentWithNumberOfTrainingSamples me = new ExperimentWithNumberOfTrainingSamples();
			me.test();
			me.plot();
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}

}
