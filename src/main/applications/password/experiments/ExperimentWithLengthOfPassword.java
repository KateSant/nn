
package applications.password.experiments;

import core.utils.plot.Line;
import core.utils.plot.Graph;
import core.utils.plot.Point;
import core.utils.TestResults;
import applications.password.Password;
import java.awt.Color;


public class ExperimentWithLengthOfPassword extends PasswordMultiRunner{



	private int[] lengthOfSample = new int[]{2,5,10,15,20,25,30,35,40,45,50};
	//private int[] lengthOfSample = new int[]{2,20,50};
	private int numUsers = 5;
	private int numSamples = 12;	

	
	protected void test() throws Exception{
		// 1st param is length of sample, 2nd is number of samples to use in training, 3rd is number of users	
		tests = new Password[lengthOfSample.length];
		points = new Point[lengthOfSample.length];
		falsePositivesPoints=new Point[lengthOfSample.length];
		falseNegativesPoints=new Point[lengthOfSample.length];
		
		for(int n=0; n<lengthOfSample.length; n++){
			// set it up
			tests[n]= new Password(lengthOfSample[n],numSamples,numUsers);//2 users
			//TestResults res = multiRun(tests[n]);
			multiRun(tests[n],n,lengthOfSample[n]);
			//points[n]=new Point(lengthOfSample[n],res);
			//falsePostivesPoints[n]=new Point(lengthOfSample[n],res.getFalsePositives());
			//System.out.println(lengthOfSample[n]+" "+res);
		}

	}
	

	
	protected void plot(){	
		super.plot();
		//xmax ymax, xsteps, ysteps
		p = new Graph(new Line[]{correctLine,falsePositivesLine,falseNegativesLine}, 50, 100, 5f, 10f, "Effect of Length of Password (Password/Backprop)", "sample length","accuracy(%)");
		p.plot();
	}
	
	public static void main(String args[]){
		try{
			ExperimentWithLengthOfPassword me = new ExperimentWithLengthOfPassword();
			me.test();
			System.out.println("plotting..");
			me.plot();
			
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}

}
