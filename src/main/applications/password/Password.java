package applications.password;


import core.structure.layers.*;
import core.structure.networks.BackPropagationNetwork;
import core.utils.TestResults;

import applications.password.dao.*;

/** **/
public class Password {
			
	
	public static final String dataDir = "data/";
	
	PasswordSampleDataDAO psd;	
	BackPropagationNetwork testNet;	
	float desiredError =0.001f;
	int maxTrials = 10000;
	String label;
	TestResults testResults;
	int hiddenUnits;
	float limitPlusMinus = 0.3f;// default after testing


	/** these are the variables we will vary to test behaviour of network **/
	int lengthofSample;
	int numTrainingSamples;
	int numUsers;	
	int numTestingSamples=5;
		
	
	
	/** constructor.. then call go().  keep them separate so we can call other accessors before go() eg, change no of hidden units..**/
	public Password(int lengthofSample, int numTrainingSamples, int numUsers) throws Exception{	
		this.lengthofSample=lengthofSample;
		this.numTrainingSamples=numTrainingSamples;
		//this.numTestingSamples=numTrainingSamples;
		this.numUsers=numUsers;
				
	}
	
	
	public Password(int lengthofSample, int numTrainingSamples, int numUsers, float limitPlusMinus) throws Exception{	
		this(lengthofSample, numTrainingSamples,numUsers);
		this.limitPlusMinus=limitPlusMinus;
				
	}
	
	public void go() throws Exception{	
		setUpPatterns();	
		createNetwork();
		train();
		test();
		System.out.println("Test results:"+getTestResults().toString());
		//plotPatterns(this.testResults.toString());
	}
	
	private void setUpPatterns() throws Exception{
		psd = new PasswordSampleDataDAO(lengthofSample, numTrainingSamples, numTestingSamples, numUsers, dataDir);
	}
	
	protected void train(){
		System.out.println("TRAINING");
		int numTrials= testNet.train(psd.getTrainingPatterns(),desiredError, maxTrials);		
		System.out.println("took "+numTrials+" to reach error of "+desiredError);
	}
	
	
	protected void test() throws Exception{
		//testResults = testNet.test(psd.getTestPatterns());
		testResults = testNet.test(psd.getTestPatterns(),psd.getAllPossibleDesiredOutputs());		
		testResults.setLabel(getLabel());

	}
	
	
	

	
	private void createNetwork() throws Exception{
		/**  create the network **/
		// the number of inputs, for this static one, is twice the number of chars (flight+dwell for each), but -1 because very first one has no flightTo
		// number of output neurons is number of users
		// hidden.. hmmm, tricky!.. maybe double the number of users..??? dunno! 
		// maybe 1* number of inputs? TODO
		// but need more if more users... hmm, numusers * 1/2 num inputs?
		// ok, let you give it in constructor
		
		int sizeInputString = psd.getTrainingPatterns()[0].getInputNumbers().length;
		//System.out.println("sizeInputString:"+sizeInputString);
		int inputN = sizeInputString;
		int hiddenN=this.hiddenUnits;
		if(hiddenN==0){
			// try to calc it then..
			hiddenN=(int) (sizeInputString*0.5f)*numUsers;//numUsers*2;
		}
		
		int outputN=numUsers;

		// inputs...given.. based on how long string is..		
		InputLayer input = new InputLayer(inputN,"input",true);// we want an offsetneuron		
		HiddenLayer hidden = new HiddenLayer(hiddenN,"hidden",true);		
		OutputLayer output = new OutputLayer(outputN,"output");
		testNet = new BackPropagationNetwork(input,new HiddenLayer[]{hidden},output);					
		testNet.setLearningRate(0.3f);
				
		testNet.setNearToZero(0+limitPlusMinus);
		testNet.setNearToOne(1-limitPlusMinus);	
		
		this.label="Created a "+inputN+":"+hiddenN+":"+outputN +" net, limits: 0 if <"+testNet.getNearToZero()+", 1 if >"+testNet.getNearToOne();
		System.out.println(label);
	}



	public void plotPatterns(String title){
		PlotTrainingPatterns ptp  = new PlotTrainingPatterns();
		ptp.plot(psd.getTrainingPatterns(), title);
	}

	
	/**
	 * @return Returns the testResults.
	 */
	public TestResults getTestResults() {
		return testResults;
	}
	
	public void setHiddenUnits(int hiddenUnits) {
		this.hiddenUnits = hiddenUnits;
	}
	
	
	
	
	/**
	 * @return Returns the desiredError.
	 */
	public float getDesiredError() {
		return desiredError;
	}
	/**
	 * @param desiredError The desiredError to set.
	 */
	public void setDesiredError(float desiredError) {
		this.desiredError = desiredError;
	}
	/**
	 * @return Returns the label.
	 */
	public String getLabel() {
		return label + " \nNetwork with numSamples:"+numTrainingSamples+", lengthOfSample: "+lengthofSample+" numUsers:"+numUsers+" desiredError="+desiredError;
	}
	/**
	 * @param label The label to set.
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * @return Returns the lengthofSample.
	 */
	public int getLengthofSample() {
		return lengthofSample;
	}
	/**
	 * @param lengthofSample The lengthofSample to set.
	 */
	public void setLengthofSample(int lengthofSample) {
		this.lengthofSample = lengthofSample;
	}
	/**
	 * @return Returns the maxTrials.
	 */
	public int getMaxTrials() {
		return maxTrials;
	}
	/**
	 * @param maxTrials The maxTrials to set.
	 */
	public void setMaxTrials(int maxTrials) {
		this.maxTrials = maxTrials;
	}
	/**
	 * @return Returns the numTestingSamples.
	 */
	public int getNumTestingSamples() {
		return numTestingSamples;
	}
	/**
	 * @param numTestingSamples The numTestingSamples to set.
	 */
	public void setNumTestingSamples(int numTestingSamples) {
		this.numTestingSamples = numTestingSamples;
	}
	/**
	 * @return Returns the numTrainingSamples.
	 */
	public int getNumTrainingSamples() {
		return numTrainingSamples;
	}
	/**
	 * @param numTrainingSamples The numTrainingSamples to set.
	 */
	public void setNumTrainingSamples(int numTrainingSamples) {
		this.numTrainingSamples = numTrainingSamples;
	}
	/**
	 * @return Returns the numUsers.
	 */
	public int getNumUsers() {
		return numUsers;
	}
	/**
	 * @param numUsers The numUsers to set.
	 */
	public void setNumUsers(int numUsers) {
		this.numUsers = numUsers;
	}
	/**
	 * @return Returns the psd.
	 */
	public PasswordSampleDataDAO getPsd() {
		return psd;
	}
	/**
	 * @param psd The psd to set.
	 */
	public void setPsd(PasswordSampleDataDAO psd) {
		this.psd = psd;
	}
	/**
	 * @return Returns the testNet.
	 */
	public BackPropagationNetwork getTestNet() {
		return testNet;
	}
	/**
	 * @param testNet The testNet to set.
	 */
	public void setTestNet(BackPropagationNetwork testNet) {
		this.testNet = testNet;
	}
	/**
	 * @return Returns the hiddenUnits.
	 */
	public int getHiddenUnits() {
		return hiddenUnits;
	}
	/**
	 * @param testResults The testResults to set.
	 */
	public void setTestResults(TestResults testResults) {
		this.testResults = testResults;
	}
	public static void main(String[] args){
	
		try{
			// 1st param is length of sample, 2nd is number of samples to use in training, 3rd is number of users
			Password me = new Password(25, 15, 5);
			me.go();
			

		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
	
	

	
}
