/*
 * Created on 01-Jun-2005 by Katie P
 */
package applications.password;

import java.awt.Color;
import java.util.HashMap;

import core.datapatterns.Pattern;
import core.utils.plot.Line;
import core.utils.plot.Graph;
import core.utils.plot.Point;




public class PlotTrainingPatterns {

	public void plot(Pattern[] trainingPatterns, String graphTitle){
		
		
//		 colours...
		HashMap colors = new HashMap();
		colors.put("katie",Color.WHITE);
		colors.put("anna",Color.RED);
		colors.put("dave",Color.CYAN);
		colors.put("tal",Color.GREEN);
		colors.put("james",Color.ORANGE);
		colors.put("jonathan",Color.YELLOW);
		colors.put("peter",Color.BLUE);
		colors.put("charlie",Color.MAGENTA);
		colors.put("davejones",Color.PINK);	
		Line[] lines = new Line[trainingPatterns.length];
		
		HashMap usersAlreadyLabelled = new HashMap();
		
		for(int i=0; i<trainingPatterns.length; i++){
			Pattern pat = trainingPatterns[i];
			
			Point[] points = new Point[pat.getInputNumbers().length];
			for(int xaxis=0; xaxis<pat.getInputNumbers().length; xaxis++){
				float yaxis = pat.getInputNumbers()[xaxis];
				points[xaxis]= new Point(xaxis,yaxis);
			}						
			String user = pat.getWho();
			Color color = (Color)colors.get(user);
			String label = user;
			if(usersAlreadyLabelled.containsKey(label)){
				label=null;// send null for label.. avoid duplicate labels..
			}
			usersAlreadyLabelled.put(user,color);
			Line thisline = new Line(points,label,color);
			
			lines[i]=thisline;
		}
		
		String[] xlabels = new String[]{"-","t","-","h","-","i","-","s","-","\\s","-","i","-","s","-"," ","-","m","-","y","-","\\s","-","p","-","a","-","s","-","s","-","w","-","o"};
		int xaxis =trainingPatterns[0].getInputNumbers().length;
											//xmax ymax, xsteps, ysteps
		Graph p = new Graph(lines, xaxis, 1, 1f, 0.1f, graphTitle, "keystroke","time(ms)",xlabels);
		p.plot();
	}
}
