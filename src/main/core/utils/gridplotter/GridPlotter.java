
package core.utils.gridplotter;

import java.awt.*;

import javax.swing.JComponent;
import javax.swing.JFrame;


import core.utils.plot.Point;



public class GridPlotter {
   	
	
	int scale = 50;
	int xPlus = 100;
	int yPlus = 100;
	
	JFrame frame = new JFrame();

	MyComponent c;
	
	public GridPlotter(){
        // Display the frame
        frame.setSize(300, 300);
        frame.setVisible(true);
	}

	
	public void plot(Point[][]grid, Point trainingPoint, Point winningPoint,Point[] neighbours){
    	// Add a component with a custom paint method
		if(c!=null){
			frame.remove(c);
		}
    	c = new MyComponent(grid,trainingPoint,winningPoint,neighbours);
    	frame.getContentPane().add(c);	            
    	frame.setVisible(true);	
	}
	
	
	   class MyComponent extends JComponent {
	   	
	   			Point[][]grid;
	   			Point trainingPoint;
	   			Point winningPoint;
	   			Point[] neighbours;
	   			
	   			public MyComponent(Point[][]grid, Point trainingPoint, Point winningPoint, Point[] neighbours){
	   				super();
	   				this.grid=grid;
	   				this.trainingPoint = trainingPoint;
	   				this.winningPoint=winningPoint;
	   				this.neighbours=neighbours;
	   			}

	   			// This method is called whenever the contents needs to be painted
	            public void paint(Graphics g) {
	                // Retrieve the graphics context; this object is used to paint shapes
	                Graphics2D g2d = (Graphics2D)g;

	                // training pt
	                int diam=8;
	                int neighbourDiam=5;
	                if(trainingPoint!=null){
	                	g2d.setColor(Color.RED);       
	                	g2d.fillOval( (int)(trainingPoint.getX()*scale + xPlus-diam/2), (int)(trainingPoint.getY()*scale + yPlus-diam/2),diam,diam);
	                }
	                // winning pt
	                if(winningPoint!=null){
	                	g2d.setColor(Color.BLUE);	                	
	                	g2d.fillOval( (int)(winningPoint.getX()*scale + xPlus-diam/2), (int)(winningPoint.getY()*scale + yPlus-diam/2),diam,diam);
	                }
	                if(neighbours!=null){
	                	System.out.println("Neighbours:"+neighbours.length);
	                	for(int wight=0; wight<neighbours.length; wight++){//along	        					
		        			Point nei=neighbours[wight];	        			
		        			g2d.setColor(Color.BLUE);	                	
		                	g2d.fillOval( (int)(nei.getX()*scale + xPlus-neighbourDiam/2), (int)(nei.getY()*scale + yPlus-neighbourDiam/2),neighbourDiam,neighbourDiam);
	                	}
	                }
	                
	                
	                // for each point, do lines down and along	                
	                g2d.setColor(Color.BLACK);
	                int xSize = grid.length;
	        		for(int dogger=0; dogger<xSize; dogger++){//along	        					
	        			int ySize = grid[dogger].length;
	        			for(int fisher=0; fisher<ySize; fisher++){ // down y
	        				// this point
	        				float x = grid[dogger][fisher].getX()*scale + xPlus;
	        				float y = grid[dogger][fisher].getY()*scale + yPlus;
	        				
	        				
	        				// ok, first the down line.. is this the last one?
	        				if(fisher+1<ySize){
		        				float x_down = grid[dogger][fisher+1].getX()*scale  + xPlus;
		        				float y_down = grid[dogger][fisher+1].getY()*scale  + yPlus;	        						        				
	        					g2d.drawLine((int)x,(int)y,(int)x_down,(int)y_down);	
	        				}
	        				
	        				// ok, now the rhs line
	        				if(dogger+1<xSize){
		        				float x_right = grid[dogger+1][fisher].getX()*scale  + xPlus;
		        				float y_right = grid[dogger+1][fisher].getY()*scale  + yPlus;	        						        				
	        					g2d.drawLine((int)x,(int)y,(int)x_right,(int)y_right);		        					
	        				}
        				
	        			}              	
	                }	           
	        		
	            }// close paint     

	   }// close component
	   
	   
	   public static void main (String[] args){
			Point [][] patterns = new Point[3][3];

	   		patterns[0][0]=new Point(1,-1);
	   		patterns[0][1]=new Point(1,0);		
	   		patterns[0][2]=new Point(1,1);	
	   		
	   		patterns[1][0]=new Point(0,-1);	
	   		patterns[1][1]=new Point(0,0);	
	   		patterns[1][2]=new Point(0,1);	

	   		patterns[2][0]=new Point(-1,-1);	
	   		patterns[2][1]=new Point(-1,0);	
	   		patterns[2][2]=new Point(-1,1);	
	   		
	   		Point trainingPoint = new Point(0.8f,0.85f);
	 		Point winningPoint = new Point(1,1);
	   		
	   		GridPlotter me = new GridPlotter();
	   		me.plot(patterns, trainingPoint, winningPoint,null);
	
	   }
	
}
