package core.utils.plot;

/**
 * Simple container for an X/Y point. <BR>
 * Used to create {@link core.utils.plot.Line Line}s.
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public class Point {

	private float x;
	private float y;
	
	/**
	 * @param x
	 * @param y
	 */
	public Point(float x, float y) {
		super();
		this.x = x;
		this.y = y;
	}
	/**
	 * @return Returns the x.
	 */
	public float getX() {
		return x;
	}
	/**
	 * @param x The x to set.
	 */
	public void setX(float x) {
		this.x = x;
	}
	/**
	 * @return Returns the y.
	 */
	public float getY() {
		return y;
	}
	/**
	 * @param y The y to set.
	 */
	public void setY(float y) {
		this.y = y;
	}
	
	public float[] getAsArray(){
		return new float[]{x,y};
	}
	public String toString(){
		return "point:"+x+","+y;
	}
}
