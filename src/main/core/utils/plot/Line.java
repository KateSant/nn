
package core.utils.plot;

import java.awt.*;
import java.util.*;
/**
 * Object which consists of an array of {@link core.utils.plot.Point Point}s, along with colour and label etc.<BR>
 * Used to construct a {@link core.utils.plot.Graph Grap}. 
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public class Line {

	private Point[] points;
	private String label;
	private Color color;

	public Line(Point[] points, String label, Color color) {
	
		this.points = points;
		this.label = label;
		this.color = color;
		log();
	}
	private void log(){
		System.out.println(label);
		for(int n=0; n<points.length;n++){
			System.out.println(points[n].getX()+"="+points[n].getY());
		}
	}
	
	public Line(String label) {	// can start with label and add with addPoints.			
		this.label = label;		
	}
	
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Point[] getPoints() {
		return points;
	}
	public void setPoints(Point[] points) {
		this.points = points;
	}
	
	ArrayList tempPoints = new ArrayList();
	
	public void addPoint(Point p){
		tempPoints.add(p);
		//		
//		 converting an ArrayList to an array
		points = ( Point[] ) tempPoints.toArray ( new Point [tempPoints.size()]);		
	}
}
