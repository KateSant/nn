package core.utils.plot;
import java.awt.*;
import javax.swing.*;

/**
 * This class generates a simple x/y graph.  <BR>
 * Construct it with an array of {@link core.utils.plot.Line Line}s, and then call {@link #plot() plot()}.  See package summary for example use.
 * 
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public class Graph {
	
	
	/** pixel stuff **/
	int frameWidth= 900;  // these are fixed
	int frameHeight = 500;
	
	int xZero = 40;  
		

	int labelHeight = 15;
	
	//int yZero = 40;
	int yZero;
	int yBottom;// now we have +ive and -ive..
	
	
	int xFarEdge = 80;
	int yFarEdge = 40;
	
	
	
	//int totalXaxis=frameWidth/*-xZero;*/-(xZero+xFarEdge); // total size of +ive x axis;    
	//int totalYaxis=frameHeight/*-yZero;*/-(yZero+yFarEdge); // total size of +ive y axis;	
	
	int totalXaxis;    
	int totalYaxis;
	
	
	float xUnit; // * what to get pixel size... total space on axis / total points to get in
	float yUnit;
	
    
	// largest numbers
	float xMax;
	float yMax;
	
	// steps on axis
	float xSteps;
	float ySteps;
	Line[] lines;
	String graphTitle;
	String xAxisTitle;
	String yAxisTitle;

	String family = "SansSerif";
 
    Font nums = new Font(family, Font.PLAIN, 12);
    Font titleF = new Font(family, Font.BOLD, 12);
    Font axes = new Font(family, Font.BOLD, 10);
  
    
    String[] specialLabel;
    
	public Graph (Line[] lines, float xMax, float yMax, float xSteps, float ySteps, String graphTitle, String xAxisTitle, String yAxisTitle){
		
		this.xMax=xMax;
		this.yMax=yMax;
		this.xSteps=xSteps;
		this.ySteps=ySteps;
		this.lines=lines;
		this.graphTitle=graphTitle;	
		this.xAxisTitle=xAxisTitle;
		this.yAxisTitle=yAxisTitle;
	}

	public Graph (Line[] lines, float xMax, float yMax, float xSteps, float ySteps, String graphTitle, String xAxisTitle, String yAxisTitle, String[] specialLabel){
		
		this(lines,xMax,yMax,xSteps,ySteps, graphTitle, xAxisTitle, yAxisTitle);
		this.specialLabel=specialLabel;
	}

	public void plot(){	
		
		yBottom+=(howManyLinesHaveLabels()+1)*labelHeight; // too many labels if not every line has a separate label..
			// now we have -ives, yZero is not really zero, it's the -ive edge..
		
		
			
		totalXaxis=frameWidth-(xZero+xFarEdge); // total size of +ive x axis;    
		totalYaxis=frameHeight-(yZero+yFarEdge); // total size of +ive y axis;	
		yZero=	totalYaxis/2;
		
		//System.out.println("totalYaxis="+totalYaxis+" frameHeight="+frameHeight+" yZero="+yZero);
		
		xUnit = totalXaxis/xMax;
		
		
		
		//yUnit = totalYaxis/yMax;  // +ive and -ive ymax
		yUnit = totalYaxis/(yMax*2);
		
		
	    // Create a frame
	    JFrame frame = new JFrame();
	    
	    // Display the frame
	    frame.setSize(frameWidth, frameHeight);

	    MyComponent c = new MyComponent(lines, graphTitle);
	    frame.getContentPane().add(c);	            
	    frame.setVisible(true);
	            	            	         
	}


    
	   class MyComponent extends JComponent {
	   	
	   			Line[] lines;
	   			String title;
	   			
	   			public MyComponent(Line[] lines, String title){
	   				super();
	   				this.lines=lines;
	   				this.title=title;	   				
	   			}

	   			// This method is called whenever the contents needs to be painted
	            public void paint(Graphics g) {
	                // Retrieve the graphics context; this object is used to paint shapes
	                Graphics2D g2d = (Graphics2D)g;     
	
	                // now plot points, for each line..
	                for(int n=0; n<lines.length; n++){
	                	Line line = lines[n];
	                	drawLine(g2d,line);	                	
	         	                
	                	// now do the label	                
	                	g2d.setColor(line.getColor());
		                String label = line.getLabel();
		                if(label!=null){
		                	//int yPos = (frameHeight-yZero) + ((n+2)*labelHeight);
		                	int yPos = (frameHeight-yBottom) + ((n)*labelHeight);
		                	g2d.drawString(label,xZero+50,yPos);
		                	int midYPos = yPos-5;
		                	g2d.drawLine(xZero+10,midYPos,xZero+45,midYPos);
		                }
	                }	  
	                
	                // draw the axes.
	                g2d.setFont(axes);
	                g2d.setColor(Color.BLACK);
	                drawAxes(g2d);
	                
	                // title	
	                g2d.setFont(titleF);
	                g2d.drawString(title,xZero+30, 20);
	                g2d.setFont(axes);
	                g2d.drawString(yAxisTitle,0, 20);
	                g2d.drawString(xAxisTitle,frameWidth-xFarEdge, frameHeight-(yZero));
	                g2d.setFont(nums);
	            }
	            
	            private void drawLine(Graphics2D g2d,Line line){
	            	Point[] points = line.getPoints();
	            	g2d.setColor(line.getColor());
	                Point prev = points[0];
	               
	                for (int n=0; n<points.length; n++){
	                	Point p = points[n];	                
	                	
	                	int x1=(int)(prev.getX()*xUnit +xZero);
	                	int y1u=(int)(prev.getY()*yUnit +yZero);	                	
	                	int y1 = frameHeight-y1u;
	                	
	                	int x2=(int)(p.getX()*xUnit +xZero);
	                	int y2u=(int)(p.getY()*yUnit +yZero);
	                	int y2 = frameHeight-y2u;
	                	
	                	g2d.drawLine(x1,y1,x2,y2);	  	                	
	                	
	                	prev=p;	                	
	                }	                  
	            }	
	            
	            
	            private void drawAxes( Graphics2D g2d){

	            	

	                
	                // y axis
	                g2d.drawLine(xZero,0, xZero,frameHeight);	         
	        		float yLabel = yMax*(-1);// ymin	                	        		
	                while(yLabel<=yMax+0.01){
	                	// the bit so we can see
	                	float yPix = yLabel*yUnit + yZero;	  
                              
	                    // 2dp please
	                	yLabel = (Math.round(yLabel*100f))/100f;

	                	//System.out.println("yLabel="+yLabel+" *yUnit="+yLabel*yUnit+" +yZero="+yPix +" frameHeight="+frameHeight+" totalYaxis:"+totalYaxis);
	                    
	                	g2d.drawString(yLabel+"",xZero-20,frameHeight-yPix);	         	                	
	                	yLabel+=ySteps;
	                }	               
	 	           
	                
	                
	                // draw xais
	                g2d.drawLine(0,frameHeight-yZero, frameWidth,frameHeight-yZero);	         
	        		float xLabel = 0;	
	        		int specialCount=0;
	                while(xLabel<=xMax){
	                	specialCount++;					// the bit so we can see
	                	float xPix = xLabel*xUnit  + xZero;	 
	                	// make it look nice
	                	String xLabelStr = xLabel+"";
	                	if((int)xLabel==xLabel){
	                		xLabelStr=(int)xLabel+"";
	                		if(specialLabel!=null&&specialCount<specialLabel.length){
	                			g2d.setFont(titleF);
	                			xLabelStr=specialLabel[specialCount];
	                		}
	                	}
	                	g2d.drawString(xLabelStr,xPix,frameHeight-(yZero-15));	         	                	
	                	xLabel+=xSteps;
	                }
	                
	                float xPix = xLabel*xUnit + xZero;
	   
	                g2d.setFont(axes);
	              
	            
	            
	            }
	   }

		public static void main(String[] args) {
			
	
			
			Point[] points = new Point[4];
			points[0] = new Point (0.5f,0.8f);
			points[1] = new Point (3f,0.3f);
			points[2] = new Point (17f,0.7f);
			points[3] = new Point (28f,0.2f);				
			Line katieline = new Line(points,"katie",Color.BLUE);	
			
			Point[] pointsd = new Point[4];
			pointsd[0] = new Point (0.5f,0.1f);
			pointsd[1] = new Point (10f,0.4f);
			pointsd[2] = new Point (17f,0.7f);
			pointsd[3] = new Point (30f,0.9f);				
			Line daveline = new Line(pointsd,"dave",Color.RED);			
			
			Line[] users = new Line[]{katieline, daveline};
			
			
			Graph p = new Graph(users, 30,1, 1f, 0.1f, "Test title", "xaxis", "yaxis");
			p.plot();
		}
		
		
		private int howManyLinesHaveLabels(){
			int ret=0;
	        for(int n=0; n<lines.length; n++){
	        	Line line = lines[n];
	            String label = line.getLabel();
	            if(label!=null){
	            	ret++;
	            }
	        }
	       // System.out.println("ret="+ret);
	        return ret;
		}
}
