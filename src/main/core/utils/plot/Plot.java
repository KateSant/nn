
package core.utils.plot;
import java.awt.*;
import javax.swing.*;

/**
 * 
 * @deprecated  Superceded by {@link core.utils.plot.Graph Graph}
 * 
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public class Plot {
	
    
  
	public Plot (Point[] points){
		this(points, 1, 1);
	}
	
    int sleepFor=150;
	int unitsize = 300;
	int unitTenth = unitsize/10;
	//int xtra = 20;// start 00 somewhere
    int frameWidth = unitTenth*12 *2;//+ xtra ;
    int frameHeight = unitTenth*12 *2;//+ xtra ;
    
    int xZero = frameWidth/2;
    int yZero = frameHeight/2; 
    
	public Plot (Point[] points, int xGra, int yGra){
	
		/*int frameWidth = 500;
		int frameHeight = 500;
	    int xZero = frameWidth/2;
	    int yZero = frameHeight/2; 
	    
	    int unitsizeX = xZero/xGra;
	    int unitsizeY = yZero/yGra;*/
	    

	    
	    

	            // Create a frame
	            JFrame frame = new JFrame();
	    

	    
	            // Display the frame
	            frame.setSize(frameWidth, frameHeight);
	            frame.setVisible(true);
	            
	            for (int n=1; n<points.length; n++){// go up through
	            	//System.out.println("array up to "+n);
	            	Point[] pointsSome= new Point[n+1];
	            	for(int m=0; m<=n; m++){
	            		//System.out.println("pointsSome["+m+"]="+points[m].getX()+" , "+points[m].getY());
	            		pointsSome[m]=points[m];
	            	}
	            
	            	// Add a component with a custom paint method
	            	MyComponent c = new MyComponent(pointsSome);
	            	frame.getContentPane().add(c);	            
	            	frame.setVisible(true);
	            	            
	            	//System.out.println("sleeping");
	            	try{
	            		Thread.sleep(sleepFor);
	            	}catch(Exception e){
	            		System.out.println("thread ex "+e.getMessage());
	            	}
	            }
	            // remove it
	        }


    
	   class MyComponent extends JComponent {
	   	
	   			Point[] points;
	   			public MyComponent(Point[] points){
	   				super();
	   				this.points=points;
	   			}

	   			// This method is called whenever the contents needs to be painted
	            public void paint(Graphics g) {
	                // Retrieve the graphics context; this object is used to paint shapes
	                Graphics2D g2d = (Graphics2D)g;
	                drawAxes(g2d);
	
	                // now plot points:
	                Point prev = points[0];
	                g2d.drawString("0",points[0].getX()*unitsize +xZero,frameHeight-(points[0].getY()*unitsize +yZero));
	                for (int n=1; n<points.length; n++){
	                	Point p = points[n];
	                	if (p==null){
	                		continue;
	                	}
	                	int x1=(int)(prev.getX()*unitsize  +xZero);
	                	int y1u=(int)(prev.getY()*unitsize +yZero);
	                	int y1 = frameHeight-y1u;
	                	int x2=(int)(p.getX()*unitsize +xZero);
	                	int y2u=(int)(p.getY()*unitsize +yZero);
	                	int y2 = frameHeight-y2u;
	                	
	                	g2d.drawLine(x1,y1,x2,y2);	  
	                	g2d.drawString(n+"",p.getX()*unitsize  +xZero,frameHeight-(p.getY()*unitsize +yZero));
	                	
	                	prev=p;	                	
	                }	                
	            }	
	            
	            
	            private void drawAxes( Graphics2D g2d){

	                // draw xais
	                g2d.drawLine(0,yZero, frameWidth,yZero);
	                int xal=0;// start at 0 on the paper
	                float label = -12f; // label it like this
	                
	                while(xal<frameWidth){
	                	float labelfloat = label/10;
	                	String s = String.valueOf(labelfloat);
	                	//System.out.println(s);
	                	g2d.drawString(s,xal,yZero);
	                	label+=1;
	                	xal+=unitTenth;
	                }
	                
	                // y axis
	                g2d.drawLine(xZero,0, xZero,frameHeight);
	                int yal=0;
	                float ylabel = 12f;
	                
	                while(yal<frameHeight){
	                	float labelfloat = ylabel/10;
	                	String s = String.valueOf(labelfloat);
	                	//System.out.println(s);
	                	g2d.drawString(s,xZero,yal);
	                	ylabel-=1;
	                	yal+=unitTenth;
	                }
	                
	            }
	   }

		public static void main(String[] args) {
			
	
			
			Point[] points = new Point[13];
			points[0] = new Point (0.5f,0.6f);
			points[1] = new Point (0.4f,0.3f);
			points[2] = new Point (0.2f,0.3f);
			points[3] = new Point (0.1f,0.6f);			
			points[4] = new Point (0.2f,0.9f);		
			points[5] = new Point (0.4f,0.9f);
			points[6] = new Point (0.5f,0.6f);
			points[7] = new Point (0.6f,0.3f);
			points[8] = new Point (0.8f,0.3f);
			points[9] = new Point (0.9f,0.6f);
			points[10] = new Point (0.8f,0.9f);
			points[11] = new Point (0.6f,0.9f);
			points[12] = new Point (0.5f,0.6f);

			Plot p = new Plot(points);
		}
}
