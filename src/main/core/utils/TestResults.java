
package core.utils;
import java.util.*;

import core.datapatterns.Pattern;


/**
 * Container for the results of testing.<BR>
 * Contains statisitics on numbers of correct positives, false positives, false negatives, and some accessors which calculate percentages.<BR>
 * Returned by {@link core.structure.networks.BackPropagationNetwork#test(Pattern[], float[][]) BackPropagationNetwork#test()}.  
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public class TestResults {

	private float tried;
	private float falseNegatives;
	private float falsePositives;
	private float correct;
	private String label="";
	ArrayList similarities = new ArrayList();
	
	public TestResults(float correct, float falsePositives, float falseNegatives){
		this.correct=correct;
		this.falsePositives=falsePositives;
		this.falseNegatives=falseNegatives;
	}
	
	
	public float  getPercCorrect(){
		return (correct*100/tried);
	}
	
	public float  getPercFalsePositives(){
		return (falsePositives*100/tried);
	}
	
	public float getPercFalseNegatives(){
		return (falseNegatives*100/tried);
	}
	
	public String toString(){
		//String a =  label+"\nTried:"+tried+"\nfalseNegatives:"+falseNegatives+"\ncorrect:"+correct;
		
		String ret = label+"\nTried:"+tried+"\nfalseNegatives:"+falseNegatives+"\ncorrect:"+correct+"\n";
		ret +="correct:"+getPercCorrect()+"%\n";
		ret +="falsePositives:"+getPercFalsePositives()+"%\n";
		ret +="falseNegatives:"+getPercFalseNegatives()+"%\n";
		/*for(int n=0; n<similarities.size(); n++ ){
			ret+= similarities.get(n)+"\n";
		}*/
		return ret;		
	}
	
	public void addSimilarity(Object o){
		similarities.add(o);
	}
	
	/**
	 * @return Returns the label.
	 */
	public String getLabel() {
		return label;
	}
	
	/**
	 * @param label The label to set.
	 */
	public void setLabel(String label) {
		this.label = label;
	}
	public TestResults(int tried) {
		super();
		this.tried = tried;
	}
	public void addFalseNegative(){
		falseNegatives++;
	}
	public void addTried(){
		tried++;
	}
	public void addFalsePostitive(){
		falsePositives++;
	}
	public void addCorrect(){
		correct++;
	}
	
	/**
	 * @return Returns the correct.
	 */
	public float getCorrect() {
		return correct;
	}
	/**
	 * @return Returns the falseNegatives.
	 */
	public float getFalseNegatives() {
		return falseNegatives;
	}
	/**
	 * @return Returns the falsePostitives.
	 */
	public float getFalsePositives() {
		return falsePositives;
	}
	/**
	 * @return Returns the tried.
	 */
	public float getTried() {
		return tried;
	}
}
