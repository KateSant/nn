
package core.utils;

import java.util.ArrayList;

/**
 * Utility package for a few logging and munging methods.
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public class Util {

	 public static String join( String token, float[] strings ) {
        StringBuffer sb = new StringBuffer();	       
        for( int x = 0; x < strings.length; x++ ){
        	
        	if(x>500){
        		break;// bail or else can't print!
        	}
        	String s = String.valueOf(strings[x]);
        	if(s.equals("0.0")){// printing convenience!
        		s="0";
        	}
        	if(s.equals("1.0")){
        		s="1";
        	}
        	sb.append( s );
        	
        	if(x<strings.length-1){// not on last one
        		sb.append( token );
        	}
        }
        //sb.append( strings[ strings.length - 1 ] );	 
        
        
        return( sb.toString() );
    }
	 
	 
	 public static float[] concat(float[]a, float[]b){
	 	float[] output = new float[a.length+b.length];
	 	for(int dogger=0; dogger<a.length; dogger++){
	 		output[dogger]=a[dogger];
	 	}
	 	for(int fisher=0; fisher<b.length; fisher++){
	 		output[fisher+a.length]=b[fisher];
	 	}
	 	
	 	return output;
	 	
	 }
	 
	 public static boolean floatArrayEqual(float[]a, float[]b){
	 	if(a==null||b==null){
	 		return false;
	 	}
	 	if(a.length!=b.length){
	 		return false;
	 	}
	 	for(int n=0; n<a.length; n++){
	 		if(a[n]!=b[n]){
	 			return false;
	 		}
	 	}
	 	return true;
	 }
	 
	 public static float floatArraySimilar(float[]a, float[]b) throws Exception{
	 	
	 	/** do some sort of statistical measure here?? chi squared??**/
	 	/** just subtract each one, and divide by length of float array ?**/
	 	
	 	if(a==null||b==null){
	 		throw new Exception("one is null.. can't call floatArraySimilar");
	 	}
	 	if(a.length!=b.length){
	 		throw new Exception("float arrays not equal length.. can't call floatArraySimilar");
	 	}
	 	
	 	double total=0;
	 	
	 	for(int n=0; n<a.length; n++){
	 		float difference = (a[n]-b[n]);
	 		
	 		float diffAbs = Math.abs (difference);   	
	 		
	 		//System.out.println("difference:"+difference+" difAbs"+diffAbs);
	 		total+=diffAbs;
	 	
	 		
	 	}
	 	
	 	float av = (float)total/a.length;
	
	 	
	 	float confidence = 1-av;
	 	//System.out.println("total:"+total+" average:"+av+"");
	 	
	 	//return av;  // inverse because we want how similar, not how different
	 	return confidence;
	 }
	 
	 
	 public static ArrayList cutArrayList(ArrayList orig, int des){
		ArrayList desiredSize = new ArrayList(des);
		for(int n=0; n<des; n++){
			desiredSize.add(orig.get(n));
		}
		return desiredSize;
	}
	 
	 
	 public static ArrayList getSliceFromArrayList(ArrayList orig, int start, int end){
	 	ArrayList desired = new ArrayList();
	 	for(int m=start; m<end; m++){
	 		desired.add(orig.get(m));
	 	}
	 	return desired;
	 }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
}
