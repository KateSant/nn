/*
 * Created on 25-Apr-2005 by Katie P
 */
package core.utils;

import core.utils.plot.*;
/**
 * Logarithmic activation function which returns results in range 0 - +1.<BR>
 * Used in {@link core.structure.neurons.ActivatableNeuron#beActivated() ActivatableNeuron#beActivated()}.
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public class ActivationFunctionZeroToOne implements ActivationFunction{



	
	/**
	 * performs calculation:<BR>
	 * float activation = (1 / (1 + (float)Math.exp(-scale*input))) ;	
	 */
	public float getact(float input, int scale){
			
		float activation = (1 / (1 + (float)Math.exp(-scale*input))) ;		
		return activation;		
	}
	
	//activation = 1 / (1 + (float)Math.exp(0-total));


}
