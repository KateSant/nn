/*
 * Created on 25-Apr-2005 by Katie P
 */
package core.utils;

import core.utils.plot.*;

/**
 * Logarithmic activation function which returns results in range -1 - +1.<BR>
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public class ActivationFunctionMinusOneToOne implements ActivationFunction{

	//protected float scale=5f;
	
	/** 
	 * performs thecalculation:<BR>
	 * float activation = (2 / (1 + (float)Math.exp(-scale*total)))  -1 ;
	 */
	public float getact(float input, int scale){
		//float activation = 1 / (1 + (float)Math.exp(0-total));
		
		//float activation = (float)Math.exp(total);
		//float activation = (float)Math.pow(Math.E,total);

	
		
		
		//float ex = (1 / (1 + (float)Math.exp(-scale*total))) ;		
		//System.out.println(total +" - "+ex);
		
		// from 0 to 1, crosses y at 0.5
		// so scale it * 1
		// and take away 1
		// now crosses y at 0, and is from -1 to 1
		
		
		float activation = (2 / (1 + (float)Math.exp(-scale*input)))  -1 ;		
		//System.out.println(total +" - "+activation);
		//return activation;
		return activation;
		
	}
	
	protected void plotDemo(){
		
		//Point[] points = new Point[21];
		Point[] points = new Point[42];
		int pa=0;
		
		//for(float x=-1; x<=1.01; x+=0.1){			
		for(float x=-1; x<=1; x+=0.05){
			float y = getact(x, 5);			
			System.out.println(pa+" x="+x+" y="+y);
			points[pa++]=new Point(x,y);
		}
		Plot plot = new Plot(points);
		//Graph g = new Graph(line);
	}
	
	public static void main(String[] args) {
		ActivationFunctionMinusOneToOne a = new ActivationFunctionMinusOneToOne();
		a.plotDemo();
	}
}
