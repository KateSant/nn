/*
 * Created on 25-Apr-2005 by Katie P
 */
package core.utils;

/**
 * Interface defining an (activation) function. 
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public interface ActivationFunction {

	/**
	 * 
	 * @param input = Input number to be transformed.
	 * @param scale = Scale to use in transformation.
	 * @return
	 */
	public float getact(float input, int scale);
}
