
package core.datapatterns;


public class Pattern {

	private float[] inputNumbers;
	private float[] outputNumbers;
	String who;
	
	public Pattern(float[]inputNumbers,float[]outputNumbers){
		this.inputNumbers=inputNumbers;
		this.outputNumbers=outputNumbers;		
	}
	
	public Pattern(float[]inputNumbers,float[]outputNumbers, String who){
		this(inputNumbers,outputNumbers);
		this.who=who;
	}
	
	
	public String getWho() {
		if(who==null){
			return "anon";
		}else{
			return who;
		}
	}
	public void setWho(String who) {
		this.who = who;
	}
	/**
	 * @return Returns the inputNumbers.
	 */
	public float[] getInputNumbers() {
		return inputNumbers;
	}
	/**
	 * @param inputNumbers The inputNumbers to set.
	 */
	public void setInputNumbers(float[] inputNumbers) {
		this.inputNumbers = inputNumbers;
	}
	/**
	 * @return Returns the outputNumbers.
	 */
	public float[] getOutputNumbers() {
		return outputNumbers;
	}
	/**
	 * @param outputNumbers The outputNumbers to set.
	 */
	public void setOutputNumbers(float[] outputNumbers) {
		this.outputNumbers = outputNumbers;
	}
}
