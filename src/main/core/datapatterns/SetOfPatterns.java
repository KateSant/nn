/*
 * Created on 06-Feb-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package core.datapatterns;

import java.util.*;
import java.io.*;

public class SetOfPatterns {

	protected ArrayList pat = new ArrayList();
	protected String label;
	
	public void addPattern(Pattern p){
		pat.add(p);
	}
	
	public Pattern[] getPatterns(){
		return (Pattern[])pat.toArray(new Pattern[0]);
	}
	/** use this constructor when you are going to follow it with calls to addPattern explicitly**/
	public SetOfPatterns(){
	}
	public SetOfPatterns(ArrayList pat, String label){
		this.pat=pat;
		this.label=label;
	}
	
	/** use this constructor when you are going to pass in a (set of) inputstreams **/
	public SetOfPatterns(String[] filenames){
	    
		for(int n=0; n<filenames.length; n++){
			
			System.out.println("reading "+filenames[n]);
			
			try {
		        BufferedReader in = new BufferedReader(new FileReader(filenames[n]));
		        String str = "";
		        int num=0;
		        while ((str= in.readLine()) != null) {
		            //str = in.readLine();
		            System.out.println("line:"+str);
		            try {
		            	addPattern(str);
				    } catch (Exception pp) {
				    	System.err.println("Exception reading line '"+num+" "+str+"' "+pp.getMessage());
				    	continue;
				    }	
				    num++;
		        }
		    } catch (Exception e) {
		    	System.err.println("Error reading from file: "+filenames[n]+" "+e.getMessage());
		    }			
		}

	}
	
	/** parses a string which goes [input],[input],[input]::[output],[output]**/
	public void addPattern(String s) throws Exception{
		String[] inout = s.split("::");
		if(inout.length!=2){
			throw new Exception ("Can't parse "+s);
		}
		float[] preInputBits = stringArrayToFloatArray(inout[0].split(","));
		float[] inputBits = makeInputs(preInputBits);
		float[] outputBits = stringArrayToFloatArray(inout[1].split(","));
		
		Pattern p = new Pattern(inputBits, outputBits);
		pat.add(p);
	}	
	
	/** override this in some child classes to tranform the input.. **/
	protected float[] makeInputs(float[] sa) throws Exception{		
		return sa;
	}
	
	protected float[] stringArrayToFloatArray(String[] sa) throws Exception{
		float[] fa = new float[sa.length];
		for(int a=0; a<sa.length; a++){
			try{
				fa[a]=Float.parseFloat(sa[a]);
			}catch(Exception e){
				throw new Exception (sa[a]+" is NOT a FLOAT");
			}
		}
		return fa;
	}
	
	public float[][] getAllPossOutputs(){
		float[][] all = new float[pat.size()][];
		for (int n=0; n<pat.size();n++){
			Pattern p = (Pattern)pat.get(n);
			all[n]=p.getOutputNumbers();
		}
		return all;
	}
	
	/**
	 * @return Returns the label.
	 */
	public String getLabel() {
		return label;
	}
}
