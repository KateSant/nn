
package core.structure;

import java.util.*;

import core.structure.networks.Network;
import core.structure.neurons.*;



public class Synapse {

	static Random rand = new Random();
	float plusMinus=0.3f;// init random values..
	private Neuron leftN;
	private Neuron rightN;
	protected float weight;
	
	private float delta;
	
	public void calcDelta(){
		// the error on postsynaptic one * activation of presynaptic one (proportional to contribution)
		float postSError = ((ActivatableNeuron)rightN).getTempError();
		float preSAct= leftN.getActivation();		
		float adjust = postSError * preSAct * Network.learningRate;
		//System.out.println("adjust synapse by "+adjust);
		delta = adjust;
	}
	
	public void updateWeight(){
		//weight = weight - delta;
		weight = weight + delta;
		//System.out.println("weight="+weight);
	}
	
	public Synapse (Neuron leftN, Neuron rightN){
		this.rightN=rightN;
		this.leftN=leftN;	
		// start with random weight
		
		float pos = rand.nextFloat(); // from 0 to +1
		//weight = 0.5f - pos; // now from 0.5 to -0.5
		
		weight = (pos*plusMinus*2) - plusMinus; // range -0.3 to +0.3
		//System.out.println("weight on synapse: "+weight);
		//weight = pos/10; // make it small and positive..??
	}

	public Neuron getLeftN() {
		return leftN;
	}

	public Neuron getRightN() {
		return rightN;
	}

	public float getWeight() {
		return weight;
	}
	
	public float getDelta() {
		return delta;
	}
	
	public void setWeight(float weight) {
		this.weight = weight;
	}
}
