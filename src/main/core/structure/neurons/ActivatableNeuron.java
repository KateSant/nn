
package core.structure.neurons;

import java.util.ArrayList;

import core.structure.Synapse;
import core.structure.layers.Layer;
import core.structure.networks.Network;
import core.utils.*;

/**
 * An Activatable Neuron has a concrete {@link #beActivated() #beActivated()} method, and an abstract definition of {@link #calcError() calcError()}<BR>
 * HiddenNeuron and OutputNeuron are Activatable.  
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public abstract class ActivatableNeuron extends Neuron {


	protected float tempError;	

	public abstract void calcError();
	
	
	/** 
	 * Runs through the list of {@link core.structure.neurons.Neuron#synapsesInputs synapsesInputs}, and for each, multiplies
	 * the weight ({@link core.structure.Synapse#getWeight() Synapse.getWeight()}) by the 
	 * activation of the upstream neuron (({@link core.structure.Synapse#getLeftN() Synapse.getLeftN().getActivation()})<BR>
	 * Next, it passes the sum of these through {@link core.utils.ActivationFunctionZeroToOne ActivationFunctionZeroToOne}, and then finally sets its own {@link core.structure.neurons.Neuron#activation activation}.
	 */
	public void beActivated(){
		
		float total=0;
		
		for (int n=0;n<synapsesInputs.size();n++){
			Synapse sy = (Synapse)synapsesInputs.get(n);
			
			// input from this synapse = weight X activation of other unit
			float input = sy.getWeight() * sy.getLeftN().getActivation();
			//System.out.println("input from synapse "+n+" from layer '"+sy.getLeftN().getLayer().getName()+"' = "+input+" (weight="+sy.getWeight()+" activation from lhs="+sy.getLeftN().getActivation()+")");
			
			total=total+input;
		}
		
		
		// now have to put through activation function....
		//activation = 1 / (1 + (float)Math.exp(0-total));	
		
		ActivationFunctionZeroToOne actFuncClass = new ActivationFunctionZeroToOne();
		int activationFunctionScaleBy = 1;// always 1 inside network..
		activation = actFuncClass.getact(total,activationFunctionScaleBy);
		
	}
	
	
	
	

	public void calcDeltaOnPreSynapses(){
	// calc weight adjustment for each synapse onto this hidden unit
		ArrayList inputSy = this.getSynapsesInputs();
		for (int s =0; s<inputSy.size(); s++){
			
			((Synapse)inputSy.get(s)).calcDelta();
		}
	}
	

	public void updatePreSynapses(){
		// make weight adjustment for each synapse onto this hidden unit
			ArrayList inputSy = this.getSynapsesInputs();
			for (int s =0; s<inputSy.size(); s++){
				
				((Synapse)inputSy.get(s)).updateWeight();
			}
		}
	

	


	public void setTempError(float tempError) {
		this.tempError = tempError;
	}
	public float getTempError() {
		return tempError;
	}
	
	public ActivatableNeuron(Layer layer){
		super(layer);
	}
	public ActivatableNeuron(){
		super();
	}	

}
