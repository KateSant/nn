package core.structure.neurons;

import core.structure.Synapse;
import core.structure.layers.Layer;

/** Concrete subclass of ActivatableNeuron, providing a backpropagation {@link #calcError() #calcError()} method. 
 * 
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public class HiddenNeuron extends ActivatableNeuron{

	/** 
	 * 
	 * 
	 * Runs through the list of {@link core.structure.neurons.Neuron#synapsesOutputs synapsesOutputs}, and for each, multiplies
	 * the weight ({@link core.structure.Synapse#getWeight() Synapse.getWeight()}) by the 
	 * error on the downstream neuron (({@link core.structure.Synapse#getRightN() Synapse.getRightN().getTempError()})<BR>
	 * Next, multiplies this by this.activation * (1-this.activation).  Sets tempError.
	 * **/
	public void calcError(){
		//System.out.println("calling calcerror on a hidden neuron");
		/*The hidden neuron error signal ?pj is given by:
		 Opj * (1-Opj) *  Eum (dpk * Wkj)
		where 
		?pk is the error signal of a post-synaptic neuron k and 
		Wkj is the weight of the connection from hidden neuron j to the post-synaptic neuron k */

		// ie O.. is size of this neuron's contribution, and Eum  errors on next ones along * weights with them..		
		float thisNeuronContrib =this.activation * (1-this.activation);

		// now errors from rhs
		float totalERHS =0;
		for (int p=0;p<synapsesOutputs.size();p++){
			Synapse sy = (Synapse)synapsesOutputs.get(p);
			// input from this synapse = weight X error on unit on rhs
			float errFromRhs = sy.getWeight() * ((ActivatableNeuron)sy.getRightN()).getTempError();
			totalERHS+=errFromRhs;
		}
		
		float completeError = thisNeuronContrib*totalERHS;
		//System.out.println("thisNeuronContrib="+thisNeuronContrib+" totalERHS="+totalERHS+" completeError="+completeError);

		tempError = completeError;
	}
	
	public HiddenNeuron(Layer layer){
		super(layer);
	}
	public HiddenNeuron(){
		super();
	}
}
