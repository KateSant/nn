package core.structure.neurons;

import java.util.*;

import core.structure.layers.Layer;


/** Concrete subclass of ActivatableNeuron, which has a {@link #calcError() #calcError()} method appropriate to an output neuron, and also a {@link #desiredOutput #desiredOutput}. 
 * 
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public class OutputNeuron extends ActivatableNeuron {
	

	private float desiredOutput;

	
	public void setDesiredOutput(float desired){
		this.desiredOutput=desired;
	}

	/**
	 * 
	 * this.activation * (1-this.activation) * (desiredOutput - actual)
	 * 
	 */
	public void calcError(){
		//System.out.println("calling calcerror on an output neuron");
		float actual = this.getActivation();					
		float error =  actual * (1-actual) * (desiredOutput - actual);
		//System.out.println("->error term on neuron - actual="+actual+" desired="+desiredOutput+" error="+error);						
		// now store it for a minute..
		tempError = error;
	}
	
	/** override this... not really appropri**/
	public ArrayList getSynapsesOutputs() {
		System.err.println("Warning: Not really appropriate to cal getSynapsesOutputs() on an Output Neuron ...???");
		return super.getSynapsesOutputs();
	}
	
	public OutputNeuron(Layer layer){
		super(layer);
	}
	public OutputNeuron(){
		super();
	}
}
