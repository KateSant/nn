
package core.structure.neurons;

import java.util.ArrayList;

import core.structure.layers.Layer;


/** No extra features on top of a simple {@link core.structure.neurons.Neuron Neuron} - just a concrete subclass, named for clarity.
 * 
 * @author Katie Portwin
 * @version 1.0  August 2005
 */ 
public class InputNeuron extends Neuron {

	
	public InputNeuron(){
		super();
	}
	
	public InputNeuron(Layer layer){
		super(layer);
	}
}
