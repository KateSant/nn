
package core.structure.neurons;

import java.util.ArrayList;

import core.structure.layers.Layer;
import core.structure.Synapse;

 
public class SOMNeuron extends ActivatableNeuron {

	private ArrayList neighbours= new ArrayList();
	private String label;
	
	public SOMNeuron(String label){
		super();
		this.label=label;
	}
	
	public SOMNeuron(Layer layer){
		super(layer);
	}
	
	public String toString(){
		return (label!=null) ? label : super.toString(); 
		
	}
	
	
	public void addNeighbour(SOMNeuron n){
		neighbours.add(n);
	}
	
	public ArrayList getNeighbours() {
		return neighbours;
	}
	

	public void updateWeightsOnSelfAndNeighbours(){
		this.updateWeights();
		for(int n=0; n<neighbours.size(); n++){
			SOMNeuron nei =(SOMNeuron)neighbours.get(n);
			nei.updateWeights();
		}
	}
	
	float k=0.05f;// this needs to decrease over time..
	/** TODO, move this into SYNAPSE -> new synapse needed REFACTOR **/
	private void updateWeights(){
		for(int l=0; l<synapsesInputs.size(); l++){
			Synapse s = (Synapse)this.synapsesInputs.get(l);
//			k(x-w)y;
			
			float y = s.getLeftN().getActivation();
			float w = s.getWeight();
			float x = this.getActivation();
			float diff =  (x-w) * y * k;
			System.out.println(this+" synapse"+l+"  y="+y+" w="+w+" x="+x+"  diff="+diff);
			// update..
			System.out.println("oldweight:"+s.getWeight());
			s.setWeight(s.getWeight()+diff);
			System.out.println("newweight:"+s.getWeight());
		}		
	}
	public void calcError(){
		// TODO
	}
	
	
	
	
	
	
	
	
}
