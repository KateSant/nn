package core.structure.neurons;

import java.util.*;

import core.structure.*;
import core.structure.layers.*;
import core.utils.plot.Point;

/**
 * Represents a unit in the neural network.  Has an activation level - a float -  and an array of {@link core.structure.Synapse Synapse}s as inputs, and another array of synapses as outputs.<BR>  
 * This class is abstract, since real neurons are Input, Hidden, or Output.
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public abstract class Neuron {
	
	protected float activation;
	
	protected ArrayList synapsesInputs=new ArrayList();
	protected ArrayList synapsesOutputs=new ArrayList();	
	

	public float getActivation() {
		return activation;
	}
	
	
	public void setActivation(float activation) {
		this.activation = activation;
	}

	public void addInputSynapse(Synapse s){
		synapsesInputs.add(s);
	}
	public void addOutputSynapse(Synapse s){
		synapsesOutputs.add(s);
	}

	
	public ArrayList getSynapsesInputs() {
		return synapsesInputs;
	}
	
	public ArrayList getSynapsesOutputs() {
		return synapsesOutputs;
	}
	private Layer layer;	
	/** 
	 * Extra constructor with pointer to what layer this one is in.
	- this  this is just useful for knowing it's name, for logging + debugging
	**/
	public Neuron(Layer layer){
		this.layer=layer;
	}
	public Neuron(){
	}	
	public Layer getLayer() {
		return layer;
	}
	
	public float[] getInputWeightsAsArray(){
		float[] arr = new float[synapsesInputs.size()];
		for(int cromarty=0; cromarty<synapsesInputs.size(); cromarty++){
			arr[cromarty] = ((Synapse)synapsesInputs.get(cromarty)).getWeight();
		}
		return arr;
	}
	/** makes sense only when 2 weights **/
	public Point getInputWeightsAsPoint(){
		float xCoord = ((Synapse)getSynapsesInputs().get(0)).getWeight();
		float yCoord = ((Synapse)getSynapsesInputs().get(1)).getWeight();
		
		Point p = new Point(xCoord, yCoord);
		return p;
	}
}
