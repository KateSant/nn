
package core.structure.networks;


import java.awt.Color;
import java.util.*;

import core.datapatterns.Pattern;
import core.structure.*;
import core.structure.layers.*;
import core.structure.neurons.Neuron;
import core.utils.Util;
import core.utils.plot.*;



/** 
 * Inherits almost everything from backpropagation network - including {@link core.structure.networks.BackPropagationNetwork#feedForward(float[]) feedForward(float[])},
 * {@link core.structure.networks.BackPropagationNetwork#feedBack(float[]) feedBack(float[])},
 * {@link core.structure.networks.BackPropagationNetwork#train(Pattern[], float, int) train(Pattern[], float, int)}.
 * <BR>
 * Except: constructor differs -it sets up the context layer - , and 
 *  {@link core.structure.networks.ElmanNetwork#trainWithPatternSet(Pattern[], boolean) trainWithPatternSet} is overridden. 

 */
public class ElmanNetwork extends BackPropagationNetwork {


	protected InputLayer contextLayer;
	HiddenLayer recHLayer;
	
	boolean reinitialiseEverySetStart=false;

	public byte betweenChunk=ElmanNetwork.BC_DO_NOTHING;

	public static final byte BC_REINITIALISE =0;
	public static final byte BC_DO_NOTHING =1;
	public static final byte BC_REVERT_TO_SAVED=2;	
	
	
	/** 
	 * super() hooks up the 3 normal layers, 
	 * then this contstructor creates and hooks in a context layer too **/
	public ElmanNetwork(InputLayer inputLayer, HiddenLayer[] hiddenLayers, OutputLayer outputLayer) throws EmptyLayerException{
		super(inputLayer,hiddenLayers,outputLayer);
		
		recHLayer = (HiddenLayer)hiddenLayers[0];
		int len = recHLayer.getNeurons().length;
		// this is the one that gets its activations copied.
		
		// now add the context units
		contextLayer = new InputLayer(len,"context layer");
		// they start off with activations of 0.5
		initialiseContextLayer(); 		
		
		// they are hooked up to that hidden layer so output of context is input to that hiddenl
		//glueLayers(contextLayer,recHLayer);
		new LayerGluer().glueLayers(contextLayer,recHLayer);
		//glueLayersWithNonUpdatableSynapses(contextLayer,recHLayer);
	}
	

	/** resets all activations on the context layer  to 0.5
	 * **/	
	private void initialiseContextLayer(){
		try{
			int len = contextLayer.getNeurons().length;
			float[]f = new float[len];
			for(int n=0;n<len;n++){
				f[n]=0.5f; /*********************** should it be 0.5 ?????(we have negatives)...**/
			}
			contextLayer.presentPattern(f);
			
		}catch(DataArrayWrongSizeForLayerException da){
			System.err.println("DataArrayWrongSize - extremely odd");
			System.exit(1);
		}
	
	}
	

	

	
	
	/** datastructure for storing context layer activations in between leaves - in order to implement the revert-to-stored-context behaviour**/
	private HashMap users = new HashMap();
	
	/** implements revert-to-stored-context behaviour**/
	private float[] getStoredContextForUser(float[] userfloats){/** recode for chunks labelled by user..**/
		
		String userS = Util.join(" ",userfloats);
		if(users.containsKey(userS)){
			//System.out.println("found user:"+userS+" in stored context layer..");
			return (float[])users.get(userS);
			
		}else{
			//System.out.println("user:"+userS+" not in stored context yet..");
			return null;
		}
		
	}
	/** implements revert-to-stored-context behaviour**/
	private void saveStoredContextForUser(float[]userfloats, float[] contextActivations){
		String userS = Util.join(" ",userfloats);
		users.put(userS,contextActivations);
	}
	
	float[] previousDesiredOutput=null;
	
	
	
	/**
	 * For each pattern, calls {@link #feedForward(float[]) #feedForward(float[])} with the input array from the pattern, 
	 * then {@link #copyActivationFromHiddenToContext() copyActivationFromHiddenToContext()}, and then 
	 * and then {@link #feedBack(float[]) #feedBack(float[])}.<BR>  
	 * Also, when a new (different to last time) desired output is detected, assumes that this is a new leaf.  Behaviour now 
	 * depends on value of {@link #betweenChunk betweenChunk}.. if BC_REINITIALISE, calls {@link #initialiseContextLayer() initialiseContextLayer()}, if 
	 * C_REVERT_TO_SAVED, then uses {@link #getStoredContextForUser(float[]) getStoredContextForUser(float[])}, if BC_DONOTHING, does nothing. 
	 *  
	 * Method is repeatedly called by {@link #trainWithPatternSet(Pattern[], boolean) trainWithPatternSet(Pattern[], boolean)}.<BR>
	
	 */
	protected float trainWithPatternSet(Pattern[] set, boolean logthis){
		
		if(reinitialiseEverySetStart){
			initialiseContextLayer();
		}
		
		float errorThisSet=0;// error is across all patterns in set
		
		
		for(int ps =0; ps<set.length; ps++){			// for each pattern in the list	
		    try{
		    	// reset context back to neutral if it is a new user?? or to whatever it was last??
		    	float[] desiredOutput = set[ps].getOutputNumbers();	
		    	if(!Util.floatArrayEqual(desiredOutput,previousDesiredOutput)){
		    		if(logthis){
		    			System.out.println("New chunk");/** maybe recode this so chunks inside patterns?**/
		    		}			
		    		switch(betweenChunk){
			 			case BC_DO_NOTHING: 
			 				break;
			 			case BC_REINITIALISE:
							initialiseContextLayer();
							break;
			 			case BC_REVERT_TO_SAVED:					
			 				float[] savedContextActivations = getStoredContextForUser(desiredOutput);
			 				if(savedContextActivations!=null){
			 					contextLayer.presentPattern(savedContextActivations);
				 				}
			 				break;														
					}				
				}
					
				/** get input pattern **/
				float[] firstPattern = set[ps].getInputNumbers();		
				// check that pattern size matches input layer size
				if(firstPattern.length>inputLayer.getNeurons().length){
				System.err.println("Not enough input neurons for that pattern..(input neurons:"+inputLayer.getNeurons().length+" but pattern has "+firstPattern.length+" pattern:"+firstPattern.toString());
				System.exit(1);
				}		
			
				/** forward training **/
				feedForward(firstPattern);

			
				/** get output pattern**/
				previousDesiredOutput=desiredOutput;
			
				/** a bit of logging **/
				logSoFar(logthis, desiredOutput, firstPattern);
			
			
				/** now do the elman bit - copy hidden to context **/
				copyActivationFromHiddenToContext();
				/** should this be befpre or after backward training?
			 	* ok, defo before training **/
						
			
				/** backward training **/		
				float avErrorThisPattern= feedBack(desiredOutput);
			
				//if(logthis){
				
				//}
				errorThisSet+=avErrorThisPattern;			
			
			
				// save this for chunk stuff later
				this.saveStoredContextForUser(desiredOutput,contextLayer.getActualOutputPattern());
			
		    }catch(DataArrayWrongSizeForLayerException da){
		    	System.err.println("DataArrayWrongSize:"+da.getMessage());
		    	System.exit(1);
		    }	
	    }
		
		
		float avErrorThisSet = errorThisSet/set.length;		
		System.out.println("Av error:"+avErrorThisSet);
		return avErrorThisSet;
		
    }

	/** overloaded **/
	protected void logSoFar(boolean logthis, float[] desiredOutput, float input[]){
		if(logthis){
			float[] actualOutPattern = outputLayer.getActualOutputPattern();
			float[] contextPattern = contextLayer.getActualOutputPattern();
			
			//System.out.print("IN="+ Util.join(" ",input)+ ", ");
			System.out.print("DesiredOUT="+ Util.join(" ",desiredOutput)+ ", ");
			System.out.print("ActualOUT="+ Util.join(" ",actualOutPattern)+ ", ");
			System.out.println("context="+ Util.join(" ",contextPattern)+ ", ");
	
		}
	}
	

	/** mediator **/
	private void copyActivationFromHiddenToContext() throws DataArrayWrongSizeForLayerException{
		/** now copy from hidden to context units..**/
		float[] hiddenActivations = recHLayer.getActualOutputPattern();
		contextLayer.presentPattern(hiddenActivations);
	}	
	

	
	
	/** test for predict next one.. (the ref impl) **/
	public float[][] testWithStartInputPattern(float[] firstPattern, int wantOutNum) throws Exception{
		
		System.out.println("\nTESTING.. elman stylee with just start-me-off at :"+Util.join(",",firstPattern)+"\n\n");
		
		//output patterns
		float[][] outputPatterns = new float[wantOutNum+1][];
		outputPatterns[0]=firstPattern;		


		//float[] far = firstPattern;
		float[] far = firstPattern;
		for(int n=1; n<wantOutNum+1; n++){
			
			/** forward propagation **/
			feedForward(far);
			
			/** what was the output..**/
			float[] actualOutPattern = outputLayer.getActualOutputPattern();		
			//System.out.println("Out="+ Util.join(" ",actualOutPattern));
			outputPatterns[n]=actualOutPattern;

			copyActivationFromHiddenToContext();// do we do this?
			
			// now output goes to input
			far = actualOutPattern;
		}
		return outputPatterns;
    }
	
	
	/** if needs first two to start it off.. so have "previous" thing to copy?  (test for predict next one.. (the ref impl))**/
	public float[][] testWithStartInputPatternAndPrev(float[] firstPattern, float[] secPattern, int wantOutNum) throws Exception{
		
		System.out.println("\nTESTING.. elman stylee with just start-me-off:\n\n");
		
		//output patterns
		float[][] outputPatterns = new float[wantOutNum+2][];
		outputPatterns[0]=firstPattern;		
		outputPatterns[1]=secPattern;
		
		// this is just setup... the "previous" for the secPattern
		initialiseContextLayer();
		// put first pattern through
		feedForward(firstPattern);
		// we don't save this output..
		copyActivationFromHiddenToContext();
		
		

		//float[] far = firstPattern;
		float[] far = secPattern;
		for(int n=2; n<wantOutNum+2; n++){
			
			/** forward propagation **/
			feedForward(far);
			
			/** what was the output..**/
			float[] actualOutPattern = outputLayer.getActualOutputPattern();		
			System.out.println("Out="+ Util.join(" ",actualOutPattern));
			outputPatterns[n]=actualOutPattern;

			copyActivationFromHiddenToContext();// do we do this?
			
			// now output goes to input
			far = actualOutPattern;
		}
		return outputPatterns;
    }
	
	
	
	
	
	
	
	
	
	
	
	/** another kind of testing method. this time tell it all outputs, 
	 * let it guess with x confidence etc, and do a graph of how it settles.. also return a true/false for match totals**/
	public byte guess(Pattern[] set, HashMap users, String title, boolean drawGraph) throws Exception{
		
		float[][]allPossibleOutputs=new float[users.keySet().size()][];
		// the output set it up....
		HashMap usersLines= new HashMap();
		Iterator ite = users.keySet().iterator();
		int n=0;
		while (ite.hasNext()){
			String userName = (String)ite.next();
			Line l = new Line(userName);
			usersLines.put(userName,l);
			allPossibleOutputs[n++]=(float[])users.get(userName);
		}
		
		
		//System.out.println("\nGUESSING.. elman .. do the copyActivationFromHiddenToContext thing...:\n\n");
		initialiseContextLayer();
		
		float[] desiredOutput=null;// outside loop so can use final value for final comparison
		float[] actualOutPattern=null; 
		float[] limitedOutPattern=null;
		for(int ps =0; ps<set.length; ps++){		// for each pattern in the list	
		
			/** get input pattern **/
			float[] firstPattern = set[ps].getInputNumbers();		
	
			
			/** forward propagation **/
			feedForward(firstPattern);

			/** get desired output pattern**/
			desiredOutput = set[ps].getOutputNumbers();	
			
			/** what was the output..**/ // only want to know at the end..
			
			actualOutPattern = outputLayer.getActualOutputPattern();
			
			//System.out.print("Desired="+ Util.join(" ",desiredOutput)+ ", ");
			//System.out.print("Actual="+ Util.join(" ",actualOutPattern)+ ", ");
			limitedOutPattern = limit(actualOutPattern);
			//matches = matches(limitedOutPattern,desiredOutput);	// update every time, will return last one				
			//System.out.print("matches="+matches+" ..");			
			
			
			
			/** compare it to various ones to do graph.. **/
			Iterator it = users.keySet().iterator(); 
			while (it.hasNext()){
				String userName = (String)it.next();
				float[] userFloat = (float[])users.get(userName);
				float sim = Util.floatArraySimilar(actualOutPattern,userFloat);
				//System.out.print(userName+" "+sim+"%  ");			
				// put it in the lines..
				Line l = (Line)usersLines.get(userName);
				l.addPoint(new Point(ps,sim));				
			}
	
					
			copyActivationFromHiddenToContext();
	    }		
		
		if(drawGraph){
			// plot it
			plot(usersLines,title);
		}
		
		// finally matches or not?
	
		byte result = this.matchesAny(limitedOutPattern,desiredOutput,allPossibleOutputs);
		//return usersLines;
		//return matches;
		return result;
    }
	
	

	/** use {@link core.utils.plot.Graph Graph} for graphical representation of settling of network **/
	private void plot(HashMap userLinesHash, String title){
		
		// add some colours... and make an array of them
		Line[] lines = new Line[userLinesHash.size()];
		int n=0;
		int xaxis=0;
		Iterator ite = userLinesHash.keySet().iterator(); 
		while (ite.hasNext()){
			String userName = (String)ite.next();
			Line line = (Line)userLinesHash.get(userName);
			xaxis = line.getPoints().length;
			// add colour
			line.setColor(colours[n]);
			lines[n]=line;
		    n++;
		}
		
		//MultiPlot p = new MultiPlot(lines, 30,1, 1f, 0.1f, title);
		Graph p = new Graph(lines, xaxis,1, 5f, 0.1f, title, "keystrokes", "confidence");
		p.plot();

	}
	
	
	// colours...
	Color[] colours = new Color[]{Color.YELLOW,Color.BLUE,Color.RED,Color.GREEN};

	public void setReinitialiseEverySetStart(boolean reinitialiseEverySetStart) {
		this.reinitialiseEverySetStart = reinitialiseEverySetStart;
	}
	
	
	


	public InputLayer getContextLayer() {
		return contextLayer;
	}
	public void setBetweenChunk(byte betweenChunk) {
		this.betweenChunk = betweenChunk;
	}
}
