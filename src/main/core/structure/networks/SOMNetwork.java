
package core.structure.networks;

import core.structure.neurons.*;
import core.structure.layers.*;
import core.utils.plot.Point;
import core.utils.gridplotter.*;
import java.util.*;

/**
 * 
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public class SOMNetwork  {

	int sleepFor=100;
	private InputLayer inputLayer;
	private SOMMapLayer mapLayer;
	
	GridPlotter gp = new GridPlotter();
	int trials=10;
	
	/**we need to know 1. how many dimensions (how many d array, and 2. what granularity (ie how many in each array)
	 * ... for now, just do granularity - assume 2 d.  TODO
	 */
	public SOMNetwork(int granularityX, int granularityY) throws EmptyLayerException{
		// 2d for now..
		inputLayer = new InputLayer(2, "inputlayer");
		
		// it creates its own multi d array
		mapLayer = new SOMMapLayer(granularityX, granularityY);
		
		// now glue them
		new LayerGluer().glueLayers(inputLayer, mapLayer);
	}

	/** chooses point, then delegates to train(point)**/
	public void train(Point[] trainingData) throws Exception{
		
		/** TODO choose random ones **/
		//
	
		while(trials-- > 0){
			
			int ran = (int)Math.random()*trainingData.length;
			System.out.println("ran:"+ran);
			Point tp = trainingData[ran];
			train(tp);				
			
		}
	}
	

	
	private void train(Point tp) throws Exception{

		/** 1. put inputs on input layer **/
		System.out.println("Presenting pattern: "+tp);
		inputLayer.presentPattern(tp.getAsArray());
		
    	/** 2. propagate **/
		this.getMapLayer().beActivated();
    	
    	/** 3. train  **/
    	
		
		//Choose closest match **/
		SOMNeuron chosen = mapLayer.chooseNeuronNearestTo(tp);
		
		chosen.updateWeightsOnSelfAndNeighbours();
		
		
		/** plot it.. **/
		System.out.println("Nearest neuron is:"+chosen+" neighbours:"+chosen.getNeighbours().size());			
		// all patterns
		Point[][] plottable = getMapLayer().getPatterns();
		// winning point
		Point chosenLocation = chosen.getInputWeightsAsPoint();
		// winning point's neighbours
		ArrayList neighbours = chosen.getNeighbours();
		Point[] neighbourLocs = new Point[neighbours.size()];
		for(int n=0; n<neighbours.size(); n++){
			SOMNeuron nei =(SOMNeuron)neighbours.get(n);
			neighbourLocs[n]= nei.getInputWeightsAsPoint();
		}
		
		gp.plot(plottable, tp, chosenLocation, neighbourLocs);
		/*artificial wait..*/

		try{
    		Thread.sleep(sleepFor);
    	}catch(Exception e){
    		System.out.println("thread ex "+e.getMessage());
    	}
    	

	}
	
	
	/**
	 * @return Returns the inputLayer.
	 */
	public InputLayer getInputLayer() {
		return inputLayer;
	}
	/**
	 * @return Returns the mapLayer.
	 */
	public SOMMapLayer getMapLayer() {
		return mapLayer;
	}
	public static void main(String[] args){
		try{
			// structure
			SOMNetwork me = new SOMNetwork(3,3);
			// display - print out
			me.mapLayer.printMe();
			// display - in graphical form
			GridPlotter gp = new GridPlotter();
			Point[][] points = me.mapLayer.getPatterns();
			gp.plot(points, null, null, null);		
			
			
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
	
}
