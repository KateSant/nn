
package core.structure.networks;
import java.util.*;
import core.datapatterns.Pattern;
import core.datapatterns.SetOfPatterns;
import core.structure.layers.*;
import core.utils.TestResults;
import core.utils.Util;



/** A backpropagation network.<BR>  
 * Adds {@link #train(Pattern[], float, int) train()} and {@link #test(Pattern[], float[][]) test()} methods to the structural functionality of {@link core.structure.networks.Network Network}.
 * **/
public class BackPropagationNetwork extends Network {

	public int logevery=1000;

	
	/** possible returns from {@link #matchesAny(float[], float[], float[][]) #matchesAny(float[], float[], float[][])}**/ 
	public static final byte MATCH = 0;
	/** possible returns from {@link #matchesAny(float[], float[], float[][]) #matchesAny}**/ 
	public static final byte FALSEPOSITIVE = 1;
	/** possible returns from {@link #matchesAny(float[], float[], float[][]) #matchesAny}**/ 
	public static final byte FALSENEGATIVE = 2;
	
	

	public BackPropagationNetwork(InputLayer inputLayer, HiddenLayer[] hiddenLayers, OutputLayer outputLayer)throws EmptyLayerException{
		super(inputLayer,hiddenLayers,outputLayer);
	}
	
	public BackPropagationNetwork(InputLayer inputLayer, HiddenLayer[] hiddenLayers, OutputLayer outputLayer, float learningRate) throws EmptyLayerException{
		super(inputLayer,hiddenLayers,outputLayer);
		BackPropagationNetwork.learningRate = learningRate;
	}	

	

	/** Calls {@link #trainWithPatternSet(Pattern[], boolean) trainWithPatternSet(Pattern[], boolean)} repeatedly, 
	 * compare the net error returned with desiredError, and exit.  

	 * @param s = The set of patterns which is the training data
	 * @param desiredError =  net error at which to stop training
	 * @param maxTrials = when to loop panic - even if desiredError not reached
	 * @return = the number of training cycles in fact taken to reach desired error
	 */
	public int train(Pattern[] s, float desiredError, int maxTrials){
	
		int trials = 0;
		float errorThisTrial;
		
		do{
		//for (int n=0; n<trials; n++){	
			trials++;

			boolean log=(trials%logevery==0);

			if(log){
				System.out.println("\n\nTrial: "+trials);
			}
			
			errorThisTrial = trainWithPatternSet(s, log);
			
			//System.out.println("Error this pattern set:"+errorThisTrial+"\n");
			
			if(log){
				System.out.println("Av Error this set of patterns (trial): "+errorThisTrial);
			}
			
			if(trials==maxTrials){
				System.out.println("Loop panic at "+maxTrials+" bailing.  local minima thing?");
				return maxTrials;
			}
			
		}while(errorThisTrial>desiredError);
			
		
		return trials;
	}
	
	
	/**
	 * For each pattern, calls {@link #feedForward(float[]) #feedForward(float[])} with the input array from the pattern, and then {@link #feedBack(float[]) #feedBack(float[])} with the output array from the pattern.<BR>  
	 * Method is repeatedly called by {@link #trainWithPatternSet(Pattern[], boolean) trainWithPatternSet(Pattern[], boolean)}.<BR>
	 * @param logthis = whether to log this trial (eg, may only want to log every 100)
	 * @param set = the pattern set training data
	 * @return = net error this time
	 */
	protected float trainWithPatternSet(Pattern[] set, boolean logthis ) {

		
		float errorThisSet=0;
		
		for(int ps =0; ps<set.length; ps++){			// for each pattern in the list	
		
			try{
				/** get input pattern **/
				float[] firstPattern = set[ps].getInputNumbers();		

				/** forward training **/
				feedForward(firstPattern);

				/** get output pattern**/
				float[] desiredOutput = set[ps].getOutputNumbers();	
			
				/** a bit of logging **/
				logSoFar(logthis, desiredOutput);
			
				/** backward training **/		
				float avErrorThisPattern = feedBack(desiredOutput);
			
				/*if(logthis){
					System.out.println("avErrorThisPattern:"+avErrorThisPattern);
				}*/
				errorThisSet+=avErrorThisPattern;
				
			}catch(DataArrayWrongSizeForLayerException da){
				System.err.println("DataArrayWrongSize:"+da.getMessage());
				System.exit(1);
			}
			
	    }
		
		
		float avErrorThisSet = errorThisSet/set.length;
		return avErrorThisSet;
		

    }
	
	
	protected void logSoFar(boolean logthis, float[] desiredOutput){
		if(logthis){
			float[] actualOutPattern = outputLayer.getActualOutputPattern();
			System.out.println("Desired="+ Util.join(" ",desiredOutput)+ ", ");
			System.out.println("Actual="+ Util.join(" ",actualOutPattern)+ ", ");	
		}
	}
	



	
	/**
	 * Helper for {@link #trainWithPatternSet(Pattern[], boolean) rainWithPatternSet(Pattern[], boolean)}<BR>
	 * Presents pattern to the input layer.  
	 * Then for every layer, except the input layer, 
	 * casts it as {@link core.structure.layers.ActivatableLayer Activatable} and
	 *  calls  {@link core.structure.layers.ActivatableLayer#beActivated() ActivatableLayer#beActivated()} 
	 * @param firstPattern = the float[] which is the input pattern
	 * @throws DataArrayWrongSizeForLayerException
	 */
	protected void feedForward(float[] firstPattern) throws DataArrayWrongSizeForLayerException{
	
		
		// put it on the input layer 
		inputLayer.presentPattern(firstPattern);
		
		// now propagate through hidden layers to ouptput
		for(int la=1;la<layers.size();la++){
			ActivatableLayer l = (ActivatableLayer)layers.get(la);
			//System.out.println("\n..propagating to "+l.getName());
			l.beActivated();
		}
	}
	
	/**
	 * /**
	 * Helper for {@link #trainWithPatternSet(Pattern[], boolean) rainWithPatternSet(Pattern[], boolean)}<BR>
	 * Presents pattern to the output layer, then iterates backwards through layers, calling 
	 *  {@link core.structure.layers.ActivatableLayer#calcError() (except very first layer, which is the input layer).
	 * @param desiredOutput = float[] representing pattern which is the desired output
	 * @return
	 * @throws DataArrayWrongSizeForLayerException
	 */
	protected float feedBack(float[] desiredOutput) throws DataArrayWrongSizeForLayerException{
		
		/** put desired output on output layer **/
		outputLayer.presentDesiredOutput(desiredOutput);
		
		/** calculate errors and delta  on output layer**/
		outputLayer.calcError();		
		outputLayer.calcDeltaOnPreSynapses();
		
		/** calculate errors and delta on hidden layers - count down through them **/
		for (int m=hiddenLayers.length-1;m>=0;m--){
			HiddenLayer h = hiddenLayers[m];
			h.calcError();			
			h.calcDeltaOnPreSynapses();
		}

					
		/** now adjust all weights by delta**/
		outputLayer.updatePreSynapses();
		
		for (int m=hiddenLayers.length-1;m>=0;m--){
			HiddenLayer h = hiddenLayers[m];
			h.updatePreSynapses();
		}
		
		
		/** how near are we to end training?**/
		return outputLayer.getModulusError();
		
	}

	
	
	/** Public method indended to be called after train().  <BR>
	 * For each test pattern, presents input vector to network, then 
	 * calls {@link #feedForward(float[]) feedForward(float[])}.
	 *  Next compares actual output (first passed through {@link core.structure.networks.Network#limit(float[]) limit(float[])},) 
	 * with desired output, using helper method {@link #matchesAny(float[], float[], float[][]) matchesAny(float[], float[], float[][])}.   **/
	public TestResults test(Pattern[] set, float[][]allPossibleOutputs) throws Exception{	
		
		TestResults tr = new TestResults(set.length);
		
		System.out.println("\nTESTING:\n\n");	
		for(int ps =0; ps<set.length; ps++){		// for each pattern in the list	
		
			/** get input pattern **/
			float[] firstPattern = set[ps].getInputNumbers();		
			// check that pattern size matches input layer size
			if(firstPattern.length>inputLayer.getNeurons().length){
				System.err.println("Not enough input neurons for that pattern..(input neurons:"+inputLayer.getNeurons().length+" but pattern has "+firstPattern.length+" pattern:"+firstPattern.toString());
				System.exit(1);
			}		
			
			
			/** forward propagation **/
			feedForward(firstPattern);

			/** get desired output pattern**/
			float[] desiredOutput = set[ps].getOutputNumbers();	
			
			/** what was the output..**/
			float[] actualOutPattern = outputLayer.getActualOutputPattern();
			//System.out.print("Input pattern ="+ join(" ",firstPattern)+ ", ");
			System.out.println("Desired      ="+ Util.join(" ",desiredOutput)+ ", ");
			System.out.println("Actual       ="+ Util.join(" ",actualOutPattern)+ ", ");
			
			/** hard limit the output.. **/
			float[] limitedOutPattern = limit(actualOutPattern);
			System.out.println("ActualLimited="+ Util.join(" ",limitedOutPattern)+ ", ");
			
			/** compare it to the desired output - does it "match" true/false **/
			/*boolean matches = matches(limitedOutPattern,desiredOutput);		
			System.out.println("matches="+matches);
			if(matches){
				tr.addCorrect();			
				
			}else{
				tr.addFalseNegative();
				//TODO: false positive..  fn=thinks it's nobody.. fp=thinks its somebody else			
			}*/
			byte result = this.matchesAny(limitedOutPattern,desiredOutput,allPossibleOutputs);
			switch (result){
				case MATCH : 	tr.addCorrect();
								System.out.println("MATCH\n");
								break;
				case FALSEPOSITIVE : tr.addFalsePostitive();
								System.out.println("FALSE +ive *****\n");
								break;
				case FALSENEGATIVE : tr.addFalseNegative();
								System.out.println("FALSE -ive\n");
				 			 break;
			}
//			 how convinced?			
			/*try{
				double sim = Util.floatArraySimilar(actualOutPattern,desiredOutput);
				//System.out.println(sim+"%  ");
				String info=set[ps].getWho()+ " Matches:"+matches+" Similarity:"+sim;
				info+="Desired="+ Util.join(" ",desiredOutput)+ ", ";
				info+=" Actual="+ Util.join(" ",actualOutPattern)+ ", ";
				info+=" ActualLimited="+ Util.join(" ",limitedOutPattern)+ ", ";
								
				tr.addSimilarity(info);
				
			}catch(Exception e){
				System.err.println("exception calculating float[] similarity");
			}*/
	    }
		return tr;		
    }
	
	/**  
	 * demo.. 

	 */
	public static void main(String[] args){
		
		float desiredError =0.001f;
		int maxTrials = 10000;

		try{		
			//1. set up structure
			InputLayer input = new InputLayer(2,"input",true);
			HiddenLayer hidden = new HiddenLayer(2,"hidden",true);		
			OutputLayer output = new OutputLayer(1,"output");
			BackPropagationNetwork me = new BackPropagationNetwork(input,new HiddenLayer[]{hidden},output);					
				
			//2. set up some data patterns..
			SetOfPatterns pats = new SetOfPatterns();
			pats.addPattern(new Pattern (new float[]{0,0}, new float[]{0}));
			pats.addPattern(new Pattern (new float[]{0,1}, new float[]{1}));
			pats.addPattern(new Pattern (new float[]{1,0}, new float[]{1}));
			pats.addPattern(new Pattern (new float[]{1,1}, new float[]{0}));
			
			//3. train	
			int numTrials= me.train(pats.getPatterns(),desiredError, maxTrials);		
			System.out.println("took "+numTrials+" to reach error of "+desiredError);			
		
			//4. test data is same as test data
			TestResults tr = me.test(pats.getPatterns(),pats.getAllPossOutputs());
		
			
			System.out.println(tr.toString());
			
			
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}

	/** 
	 * utility method for comparing arrays of floats, 
	 * called by {@link #matches(float[], float[]) matches(float[], float[])}.
	 * **/
	protected boolean matches(float[]limitedOutPattern, float[]desiredOutput){
		for(int m=0; m<limitedOutPattern.length;m++){			
			if(limitedOutPattern[m]!=desiredOutput[m]){// a match failure
				return false;
			}
		}
		return true;
	}

	
	/** 
	 * Compares final output pattern with desired output, and also other possible outputs (false positives)
	 */
	protected byte matchesAny(float[]limitedOutPattern, float[]desiredOutput, float[][]possibleOutputs){
		if(matches(limitedOutPattern,desiredOutput)){
			return MATCH;
		}
		
		for(int n=0; n<possibleOutputs.length; n++){
			float[] alternativeOutput = possibleOutputs[n];
			//System.out.println("looking for false positive: actual:"+Util.join(" ",limitedOutPattern)+" wanted:"+Util.join(" ",desiredOutput)+" trying ALTERNATIVE:"+Util.join(" ",alternativeOutput));
			if(matches(limitedOutPattern,alternativeOutput)){
				//System.err.println("false positive: actual:"+Util.join(" ",limitedOutPattern)+" wanted:"+Util.join(" ",desiredOutput)+" got:"+Util.join(" ",alternativeOutput));
				return FALSEPOSITIVE;
			}
		}
		return FALSENEGATIVE;
	}
	public void setLogevery(int logevery) {
		this.logevery = logevery;
	}
	
}
