package core.structure.networks;

import java.util.LinkedList;

import core.structure.Synapse;
import core.structure.layers.*;
import core.structure.neurons.Neuron;
import core.utils.Util;


/**
 * An abstract neural network.<BR>
 * Has the structural code for creating and containing layers of neurons. 
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public abstract class Network {


	public static float learningRate = 0.3f;
	
	/** tolerance ... ie, output of < this is treated as 0**/
	private float nearToZero=0.1f; // ie, less than this and we call it 0.. eg 0.1
	/** tolerance**/
	private float nearToOne=0.9f;// as above, for 1 .. eg 0.9
	
	
	
	protected InputLayer inputLayer;
	protected OutputLayer outputLayer;
	protected HiddenLayer[] hiddenLayers;
	protected LinkedList layers = new LinkedList();


	/** 
	 * Construct a network - set the layers, and then loop through them calling {@link #glueLayers(Layer, Layer) #glueLayers(Layer, Layer)}
	 * **/
	public Network(InputLayer inputLayer, HiddenLayer[] hiddenLayers, OutputLayer outputLayer) throws EmptyLayerException{
		this.inputLayer=inputLayer;
		this.outputLayer=outputLayer;
		this.hiddenLayers=hiddenLayers;
				
		layers.add(inputLayer);
		
		for(int l=0;l<hiddenLayers.length;l++){
			Layer la= hiddenLayers[l];
			//glueLayers((Layer)(layers.get(layers.size()-1)),la);
			new LayerGluer().glueLayers((Layer)(layers.get(layers.size()-1)),la);
			layers.add(la);			
		}
		// glues all layers together
		//glueLayers((Layer)(layers.get(layers.size()-1)),outputLayer);
		new LayerGluer().glueLayers((Layer)(layers.get(layers.size()-1)),outputLayer);
		layers.add(outputLayer);
	}


	
	public static float getLearningRate() {
		return learningRate;
	}

	public static void setLearningRate(float learningRate) {
		Network.learningRate = learningRate;
	}
	
	/** For each neuron on the first layer, for each neuron on the second, 
	 * creates a synapse with references to both of them, and then
	 * adds an appropriate reference to this synapse to both, by calling addOutputSynapse(synapse) on the lhs neuron, and addInputSynapse(synapse) on the rhs neuron. 
	 *   **/
	/* Extract Class.. now composition rather than inheritance so can reuse in som**/
	/*protected void glueLayers(Layer lhs, Layer rhs) throws EmptyLayerException{

		
		if(lhs.getNeurons().length<1){
			throw new EmptyLayerException("Layer "+lhs.getName()+" is empty");
		}
		if(rhs.getNeurons().length<1){
			throw new EmptyLayerException("Layer "+rhs.getName()+" is empty");
		}
		Neuron[] nlhs = lhs.getNeurons();
		Neuron[] nrhs = rhs.getNeurons();
				
		//System.out.println("lhs="+lhs.getName()+" rhs="+rhs.getName());
		for(int n=0;n<nlhs.length; n++){ // for each on lhs layer			
			Neuron leftN = (Neuron)nlhs[n];
			
			for(int m=0;m<nrhs.length; m++){// for each on rhs layer
				Neuron rightN = (Neuron)nrhs[m];				
				// create synapse between them
				Synapse s=new Synapse(leftN,rightN);				
				// add synapse to output of LHS
				leftN.addOutputSynapse(s);				
				// and to output of RHS
				rightN.addInputSynapse(s);				
				//System.out.prfloatln("connected "+lhs.getName()+" neuron "+n +" to "+rhs.getName()+" neuron "+m);				
			}
		}
		// now for any offsetneuron on lhs - attach it to rhs
		Neuron offsetNeuron = lhs.getOffsetNeuron();
		
		if(offsetNeuron!=null){
			for(int z=0;z<nrhs.length; z++){// for each on rhs layer
				Neuron rightN = (Neuron)nrhs[z];				
				// create synapse between them
				Synapse s=new Synapse(offsetNeuron,rightN);				
				// add synapse to output of LHS
				offsetNeuron.addOutputSynapse(s);				
				// and to output of RHS
				rightN.addInputSynapse(s);				
				//System.out.prfloatln("connected "+lhs.getName()+" neuron "+n +" to "+rhs.getName()+" neuron "+m);				
			}
		}
	}	**/
	

	/** tweaks output by comparing with limits nearToZero and nearToOne.. to give output rounded **/
	protected float[] limit(float[]actual){
		
		float[] hltd = new float[actual.length];

		boolean ambig=false;
		for(int m=0; m<actual.length;m++){	
			
			if(actual[m]>nearToOne){
				hltd[m]=1;
			}else if(actual[m]<nearToZero){
				hltd[m]=0;
				
			}else{
				hltd[m]=actual[m];//leave it
			}			
		}
		return hltd;
	}
	

	
	
	public LinkedList getLayers() {
		return layers;
	}
	
	
	
	public float getNearToOne() {
		return nearToOne;
	}
	public float getNearToZero() {
		return nearToZero;
	}
	
	

	public void setNearToOne(float nearToOne) {
		this.nearToOne = nearToOne;
	}
	public void setNearToZero(float nearToZero) {
		this.nearToZero = nearToZero;
	}
}
