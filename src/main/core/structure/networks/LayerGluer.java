
package core.structure.networks;

import core.structure.Synapse;
import core.structure.layers.EmptyLayerException;
import core.structure.layers.Layer;
import core.structure.neurons.Neuron;

/*
 * 
 * 
 * @author Katie Portwin
 * @version 1.0
 * 
 * utility for Network.  extracted so can reuse  by composition in SOMNEtwork which may not inherit.
 */
public class LayerGluer {

	/** For each neuron on the first layer, for each neuron on the second, 
	 * creates a synapse with references to both of them, and then
	 * adds an appropriate reference to this synapse to both, by calling addOutputSynapse(synapse) on the lhs neuron, and addInputSynapse(synapse) on the rhs neuron. 
	 *   **/
	protected void glueLayers(Layer lhs, Layer rhs) throws EmptyLayerException{

		
		if(lhs.getNeurons().length<1){
			throw new EmptyLayerException("Layer "+lhs.getName()+" is empty");
		}
		if(rhs.getNeurons().length<1){
			throw new EmptyLayerException("Layer "+rhs.getName()+" is empty");
		}
		Neuron[] nlhs = lhs.getNeurons();
		Neuron[] nrhs = rhs.getNeurons();
				
		//System.out.println("lhs="+lhs.getName()+" rhs="+rhs.getName());
		for(int n=0;n<nlhs.length; n++){ // for each on lhs layer			
			Neuron leftN = (Neuron)nlhs[n];
			
			for(int m=0;m<nrhs.length; m++){// for each on rhs layer
				Neuron rightN = (Neuron)nrhs[m];				
				// create synapse between them
				Synapse s=new Synapse(leftN,rightN);				
				// add synapse to output of LHS
				leftN.addOutputSynapse(s);				
				// and to output of RHS
				rightN.addInputSynapse(s);				
				//System.out.prfloatln("connected "+lhs.getName()+" neuron "+n +" to "+rhs.getName()+" neuron "+m);				
			}
		}
		// now for any offsetneuron on lhs - attach it to rhs
		Neuron offsetNeuron = lhs.getOffsetNeuron();
		
		if(offsetNeuron!=null){
			for(int z=0;z<nrhs.length; z++){// for each on rhs layer
				Neuron rightN = (Neuron)nrhs[z];				
				// create synapse between them
				Synapse s=new Synapse(offsetNeuron,rightN);				
				// add synapse to output of LHS
				offsetNeuron.addOutputSynapse(s);				
				// and to output of RHS
				rightN.addInputSynapse(s);				
				//System.out.prfloatln("connected "+lhs.getName()+" neuron "+n +" to "+rhs.getName()+" neuron "+m);				
			}
		}
	}	

}
