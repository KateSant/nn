/*
 * Created on 03-Aug-2005 by Katie P
 */
package core.structure.layers;


public class EmptyLayerException extends Exception {

	public EmptyLayerException() {
		super();
		
	}

	public EmptyLayerException(String message) {
		super(message);
	}

}
