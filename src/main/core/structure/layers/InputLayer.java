
package core.structure.layers;


import core.structure.neurons.*;

public class InputLayer extends Layer{


	/** fill neuron array with InputNeurons **/
	/*public InputLayer (int numNeurons, String name){		
		this.name=name;
		//neurons = new InputNeuron[numNeurons];
		neurons = new Neuron[numNeurons];
		for(int n=0; n<numNeurons; n++){
			//System.out.println("adding neuron "+n+" in layer "+name);
			InputNeuron en = new InputNeuron(this);
			neurons[n]=en;
		}	
	}*/
	
	/** use factory method to create them instead **/
	public InputLayer (int numNeurons, String name){
		super(numNeurons, name);
	}
	protected Neuron createNeuron(Layer layer){
		return new InputNeuron(layer);
	}
	
	
	/** send offsetNeuron=true to add a Bias unit to this layer**/
	public InputLayer (int numNeurons, String name, boolean offsetNeuron){
		this(numNeurons,name);
		if(offsetNeuron){
			setupOffsetNeuron();
		}
	}
	
	
	/** just sets activations on input layer **/
	public void presentPattern(float[] firstPattern) throws DataArrayWrongSizeForLayerException{
		
		if(firstPattern.length!=this.getNeurons().length){
			throw new DataArrayWrongSizeForLayerException("DataArrayWrongSizeForLayer: InputLayer has "+getNeurons().length+" neurosn but this input pattern has "+firstPattern.length+" numbers");
		}
		//put it on the input neurons..
		for(int n=0;n<firstPattern.length;n++){			
			float number = firstPattern[n];
			Neuron neuron = this.getNeuron(n);
			//System.out.println("putting activation value "+number+" on neuron "+n+" in the input layer");
			//just set activation?
			neuron.setActivation(number);			
		}
	}
	

}
