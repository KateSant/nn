
package core.structure.layers;

import java.util.*;

import core.structure.neurons.*;



public class OutputLayer extends ActivatableLayer{
	
	
	/** fill neuron array with OutputNeurons **/
	/*public OutputLayer (int numNeurons, String name){		
		this.name=name;
		neurons = new OutputNeuron[numNeurons];//= new ArrayList();
		for(int n=0; n<numNeurons; n++){
			//System.out.println("adding neuron "+n+" in layer "+name);
			OutputNeuron en = new OutputNeuron(this);
			neurons[n]=en;
		}	
	}*/
	
	/** use factory method to create them instead **/
	public OutputLayer (int numNeurons, String name){
		super(numNeurons, name);
	}
	protected Neuron createNeuron(Layer layer){
		return new OutputNeuron(layer);
	}


	
	/** like calcError except use modulus of error.. this is for deciding when to stop training, rather than for updating weights **/
	public float getModulusError(){
		float totalError=0;
		for(int n=0;n<neurons.length;n++){									
			 ((OutputNeuron)neurons[n]).calcError();
			 float errorThisOne = ((OutputNeuron)neurons[n]).getTempError();
			 
			 totalError+=Math.abs(errorThisOne);
			 //System.out.println("errorThisOn:"+errorThisOne+" abs:"+Math.abs(errorThisOne));
		}
		return totalError/neurons.length;	
	}
	
	
	
	public void presentDesiredOutput(float[] oup) throws DataArrayWrongSizeForLayerException{
			
		if(oup.length!=this.getNeurons().length){
			throw new DataArrayWrongSizeForLayerException("DataArrayWrongSizeForLayer: OutputLayer has "+getNeurons().length+" neuronsn but this input pattern has "+oup.length+" numbers");
		}
		
		//System.out.println("\n.presenting desired on output layer");	
		for(int n=0;n<oup.length;n++){				
			float desired = oup[n];			
			((OutputNeuron)this.neurons[n]).setDesiredOutput(desired);
		}
	}
	


	
	
}
