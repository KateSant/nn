package  core.structure.layers;

import core.structure.neurons.*;

/**
 * Container for a set of neurons.
 * @author Katie Portwin
 * @version 1.0  August 2005
 */
public abstract class Layer {


	protected Neuron[] neurons;



	protected String name;


	private Neuron offsetNeuron;// may be null



	public Layer (int numNeurons, String name) {

		this.name=name;
		neurons = new Neuron[numNeurons];
		for(int n=0; n<numNeurons; n++){
			//InputNeuron en = new InputNeuron(this);
			Neuron factoryMadeNeuron = createNeuron(this);
			neurons[n]=factoryMadeNeuron;
		}
	}
	/** 
	 * FACTORY METHOD 
	 * **/
	protected abstract Neuron createNeuron(Layer layer);
	
	
	/** 
	 * called by constructors 
	 * **/
	protected void setupOffsetNeuron(){
		offsetNeuron = new InputNeuron(this);
		offsetNeuron.setActivation(1);	// activation is set to 1 always
	}

	public Layer () {
	}
	
	public Neuron getOffsetNeuron() {
		return offsetNeuron;
	}



	

	public Neuron getNeuron(int n){
		return (Neuron)neurons[n];
	}


	public String getName() {
		return name;
	}


	public Neuron[] getNeurons() {
		return neurons;
	}

	
	public float[]getActualOutputPattern(){
		float[] oup = new float[neurons.length];
		for(int n=0;n<neurons.length;n++){				
			float act = ((Neuron)this.neurons[n]).getActivation();
			oup[n]=act;
		}
		return oup;
	}

}
