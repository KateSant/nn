/*
 * Created on 03-Aug-2005 by Katie P
 */
package core.structure.layers;


public class DataArrayWrongSizeForLayerException extends Exception {

	public DataArrayWrongSizeForLayerException() {
		super();
		
	}

	public DataArrayWrongSizeForLayerException(String message) {
		super(message);
	}

}
