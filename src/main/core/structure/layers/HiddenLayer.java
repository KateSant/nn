
package  core.structure.layers;

import java.util.*;

import core.structure.neurons.*;



public class HiddenLayer extends ActivatableLayer{
	
	
	/** fill neuron array with HiddenNeurons **/
	/*public HiddenLayer (int numNeurons, String name){		
		this.name=name;
		neurons = new HiddenNeuron[numNeurons];//= new ArrayList();
		for(int n=0; n<numNeurons; n++){
			//System.out.println("adding neuron "+n+" in layer "+name);
			HiddenNeuron en = new HiddenNeuron(this);
			neurons[n]=en;
		}	
	}*/
	
	/** use factory method to create them instead **/
	public HiddenLayer (int numNeurons, String name){
		super(numNeurons, name);
	}
	protected Neuron createNeuron(Layer layer){
		return new HiddenNeuron(layer);
	}

	/** send offsetNeuron=true to add a Bias unit to this layer**/
	public HiddenLayer (int numNeurons, String name, boolean offsetNeuron){
		this(numNeurons,name);
		if(offsetNeuron){
			setupOffsetNeuron();
		}
	}
	
	
}
