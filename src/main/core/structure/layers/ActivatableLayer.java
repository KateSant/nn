package core.structure.layers;

import core.structure.neurons.*;

public abstract class ActivatableLayer extends Layer {


	/** passes on the calcError call to ActivatableNeurons in its set.. nb this will
	 * be a different implementation in HiddenNeuron and OutputNeuron.<BR>
	 * basically just passes on the command to calc error to each of its neurons
	 */
	public void calcError(){		
		//System.out.println("\n..calculating error on layer: "+this.name);		

		for(int n=0;n<neurons.length;n++){									
			 ((ActivatableNeuron)neurons[n]).calcError();
	
		}	
	}

	public ActivatableLayer (int numNeurons, String name) {
		super(numNeurons, name);
	}
	public ActivatableLayer (){
		
	}
	
	public void calcDeltaOnPreSynapses(){
		//System.out.println("calling calcDeltaOnPreSynapses on layer "+name);
		for(int n=0;n<neurons.length;n++){									
			 ((ActivatableNeuron)neurons[n]).calcDeltaOnPreSynapses();		
		}
	}
	public void updatePreSynapses(){
		//System.out.println("calling updatePreSynapses on layer "+name);
		for(int n=0;n<neurons.length;n++){									
			 ((ActivatableNeuron)neurons[n]).updatePreSynapses();		
		}
	}
	
	
	public void beActivated(){
		for(int n=0; n<neurons.length; n++){
			 //System.out.print("activating neuron "+n+": ");
			 ((ActivatableNeuron)neurons[n]).beActivated();
		}
	}
}
