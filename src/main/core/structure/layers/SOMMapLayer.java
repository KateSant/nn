
package core.structure.layers;

import java.util.*;

import core.structure.neurons.*;
import core.structure.Synapse;
import core.utils.plot.Point;


public class SOMMapLayer extends ActivatableLayer{
	
	// 2d for now
	private SOMNeuron[][]grid;
	//int numX; 
	//int numY;

	
	/** use factory method to create them instead **/
	public SOMMapLayer (int numX, int numY){
		//this.numX=numX;
		//this.numY=numY;
		
		grid = new SOMNeuron[numX][];// the grid
		neurons = new Neuron[numX*numY];// another list of all of them
		
		int tyne=0;// counter for total no neurons
		for(int dogger=0; dogger<numX; dogger++){
			// go along x, create array down each y
			SOMNeuron[] upDown = new SOMNeuron[numY];
			grid[dogger]=upDown;
			
			// now fill it with neurons..
			for(int fisher=0; fisher<numY; fisher++){
				SOMNeuron neuron = new SOMNeuron(dogger+","+fisher);
				upDown[fisher]=neuron;
				
				// also, put pointer to it in the general list..
				this.neurons[tyne++]=neuron;
			}			
		}
		
		addNeighbourhood();		
	}
	
	private void addNeighbourhood(){
		for(int dogger=0; dogger<grid.length; dogger++){//along x			
			for(int fisher=0; fisher<grid[dogger].length; fisher++){ // down y
				SOMNeuron neuron = grid[dogger][fisher];
				
				boolean rhsOk =(dogger+1<grid.length) ? true : false;
				boolean lhsOk =(dogger-1>=0) ? true : false;
				boolean bottomOk =(fisher+1<grid[dogger].length) ? true : false;
				boolean topOk =(fisher-1>=0) ? true : false;
				
				System.out.println(" "+dogger+","+fisher+" rhsOK:"+rhsOk+" lhsOK:"+lhsOk+" topOK:"+topOk+" botOK:"+bottomOk);
				if(rhsOk){
					neuron.addNeighbour(grid[dogger+1][fisher]);
				}				
				if(lhsOk){
					neuron.addNeighbour(grid[dogger-1][fisher]);
				}
				if(topOk){
					neuron.addNeighbour(grid[dogger][fisher-1]);
				}
				if(bottomOk){
					neuron.addNeighbour(grid[dogger][fisher+1]);
				}	
				if(rhsOk&&topOk){
					neuron.addNeighbour(grid[dogger+1][fisher-1]);
				}
				if(rhsOk&&bottomOk){
					neuron.addNeighbour(grid[dogger+1][fisher+1]);
				}
				if(lhsOk&&topOk){
					neuron.addNeighbour(grid[dogger-1][fisher-1]);
				}
				if(lhsOk&&bottomOk){
					neuron.addNeighbour(grid[dogger-1][fisher+1]);
				}			
			}
		}
	}

	
	/** first stage of training - pick the one nearest to the input coordinate **/
	public SOMNeuron chooseNeuronNearestTo(Point p){
		
		float minEDist=1000;// hmm
		SOMNeuron minEDistNeuron = null;
		
		for(int lundy=0; lundy<neurons.length; lundy++){
			float eDist = calculateEuclidianDifferenceBetween(p.getAsArray(), neurons[lundy].getInputWeightsAsArray());
			if(eDist<minEDist){// new candidate
				minEDist=eDist;
				minEDistNeuron=(SOMNeuron)neurons[lundy];
			}
		}
		return minEDistNeuron;
	}
	
	protected float calculateEuclidianDifferenceBetween(float[]a, float[]b){
		
		float sumSquares=0;
		for(int fastnet=0; fastnet<a.length; fastnet++){
			float sum= (a[fastnet]-b[fastnet]);
			float sumSquare = (float)Math.pow(sum,2);
			sumSquares+= sumSquare;
		}
		return (float)Math.sqrt(sumSquares);
		
	}
	
	
	/** TODO.. this is based on a 2D thing - ie 2 weights.. **/
	public void printMe(){
		for(int dogger=0; dogger<grid.length; dogger++){//along x
		
			for(int fisher=0; fisher<grid[dogger].length; fisher++){ // down y
				SOMNeuron neuron = grid[dogger][fisher];
				float xCoord = ((Synapse)neuron.getSynapsesInputs().get(0)).getWeight();
				float yCoord = ((Synapse)neuron.getSynapsesInputs().get(1)).getWeight();
				System.out.print(xCoord+","+yCoord+"   ");
			}			
			System.out.println("");
		}
	}
	
	/** get 2d array of Point objects.  Each Point has an x and a y coordinate, from the two weights**/
	public Point[][] getPatterns(){
		Point[][] points= new Point[grid.length][grid[0].length];
		
		for(int dogger=0; dogger<grid.length; dogger++){//along x		
			for(int fisher=0; fisher<grid[dogger].length; fisher++){ // down y
				SOMNeuron neuron = grid[dogger][fisher];
				float xCoord = ((Synapse)neuron.getSynapsesInputs().get(0)).getWeight();
				float yCoord = ((Synapse)neuron.getSynapsesInputs().get(1)).getWeight();
				Point p = new Point(xCoord, yCoord);
				points[dogger][fisher]=p;
			}			
		}
		return points;
	}
	
	// we don't really want this - it's just for factorying..
	protected Neuron createNeuron(Layer layer){
		return new SOMNeuron(layer);
	}
	
	
	
	/**
	 * @return Returns the grid.
	 */
	public SOMNeuron[][] getGrid() {
		return grid;
	}
}
